<?php 
  $titlepag="FAC3 - Novo Laboratório";
  include("includes/inserir.header.php");
  header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php echo $titlepag ?></title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
</head>
<body>
  <div class="w3-card-4 w3-display-middle" style="width: 40%; top: 60%">
    	<header class="w3-container w3-blue w3-center">
    		<h2>Novo Laboratório</h2>
    	</header>
	 <div class="message" style="<?php echo $display;?>"><?php echo $msg;?></div>
	<div id="disable" class="w3-container w3-center">
          <form action="?acao=inserir" method="post">
          	<br/>
             <label for="nome_lab" class="w3-left-align"><p><b>Nome do Laboratório:</b></p></label><input autofocus id="nome_lab" type="text" name="cNomelab" class="w3-input"/>
             <br/>
		     <label for="tipo_lab" class="w3-left-align"><p><b>Tipo de Laboratório:</b></p></label><input autofocus id="tipo_lab" name="cType" type="text" class="w3-input"/>
		     <br/>
		     <label for="maquinas_lab" class="w3-left-align"><p><b>Máquinas (PC):</b></p></label><input id="maquinas_lab" type="text" name="cMaquina" class="w3-input"/>
		     <br/>
		     <label for="quanti_lab" class="w3-left-align"><p><b>Quantidade de Máquinas:</b></p></label><input id="quanti_lab" name="cQuantidademaquina" type="text" class="w3-input"/>
		     <br/>
		     <div class="w3-left-align">
		     <p><b>Status</b></p>
		     <input type="radio" id="enable" name="status" class="w3-radio" value="ATIVO"/><label for="status"> Ativado</label>
		     <input type="radio" checked id="disable"  name="status" class="w3-radio" value="DESATIVADO"/><label for="status"> Desativado</label><br>
		     </div>
		     <br/>
		     <div class="w3-left-align">
		     <p><b>Acesso</b></p>
		     <input type="radio" id="open" name="acess" class="w3-radio" value="ABERTO"/><label for="acess">Aberto</label>
		     <input type="radio" checked id="close"  name="acess" class="w3-radio" value="FECHADO"/><label for="acess">Fechado</label><br>
		     </div>
		     <input type="submit" class="w3-btn w3-blue w3-margin w3-right" value="Adicionar"/>
		     <a href="config_lab.php" class="w3-btn w3-red w3-margin w3-left" >Voltar</a>
		  </form>
        </div>
	
	</div>
</body>
</html>





