<?php
include("includes/header_config.php");
//SELECIONANDO TODOS OS CURSOS CADASTRADOS NO BANCO DE DADOS
$sql=mysql_query("SELECT * FROM coordenadores WHERE Nome!='' AND status='ATIVO' ORDER BY sigla_curso");
$contar=0;


//SELECIONANDO TODAS AS DISCIPLINAS CADASTRADAS QUE TENHA O CURSO, PERIODO E SERIE ESCOLHIDA PELO USUARIO
if ($curso=='EAD') {
  $query_cursos=mysql_query("SELECT * FROM disciplina WHERE modalidade_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo' ORDER BY curso_disc");
}else{
  $query_cursos=mysql_query("SELECT * FROM disciplina WHERE curso_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo' AND modalidade_disc='Presencial' ORDER BY curso_disc");
}
$dataInicial="01/01/2019";
$dataFinal="31/12/2019";
$cont_curso=0;

$totalag=mysql_query("SELECT * FROM agendadata WHERE data BETWEEN '$dataInicial' AND '$dataFinal'");    
$total_do_total=mysql_num_rows($totalag);

$totalag_saude=mysql_query("SELECT * FROM agendadata_saude WHERE data BETWEEN '$dataInicial' AND '$dataFinal'");
$total_da_saude=mysql_num_rows($totalag_saude);

$totalag_eng=mysql_query("SELECT * FROM agendadata_eng WHERE data BETWEEN '$dataInicial' AND '$dataFinal'");
$total_da_eng=mysql_num_rows($totalag_eng);





?>



<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
  <link href="/open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/open-iconic-master/font/css/open-iconic-bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="css/styleEng.css"


  <!-- Hight Charts Scripts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>


</head>
<?php

while($row = mysql_fetch_array($query_cursos, MYSQL_NUM)) {
 $cur_n[$cont_curso][6] = $row[6];
 $cur_n[$cont_curso][1] = $row[1];
 $cur_n[$cont_curso][4] = $row[4];
 $cur_n[$cont_curso][5] = $row[5];
 $cur_n[$cont_curso][7] = $row[7];
 $cur_n[$cont_curso][8] = $row[8];
 $cont_curso++;
}

  while($linha = mysql_fetch_array($sql, MYSQL_NUM)) {
  $coordenador[$contar][0]  = $linha[0];
  $coordenador[$contar][1]  = $linha[1];
  $coordenador[$contar][3]  = $linha[3];
  $coordenador[$contar][4]  = $linha[4];
  $coordenador[$contar][5]  = $linha[5];
  $contar++;
} 
?>

<body>
 <div class="container">
   <div>
    <h2>Dashboard</h2>
    <p>Estatisticas de agendamentos..</p>
    <hr>
  </div>
<!-- FORMULARIO DE CONSULTA PARA CRIAÇÃO DO CHART -->
  <form action="?acao=visualizar_chart" method="post">
    <div class="form-row">

      <div class="form-group col-md-3">
        <label>Selecione um curso<small style="color: red">*</small></label>
        <select class="form-control" name="curso_chart" name="curso_chart" required>
         <option value="" disabled selected>Selecione uma Curso..</option>
         <?php foreach($coordenador as $show) { ?>
          <option value="<?php echo $show[5];?>"><?php echo $show[4]?></option>
        <?php } ?>
        </select>
      </div>

      <div class="form-group col-md-3">
        <label>Turno<small style="color: red">*</small></label>
        <select class="form-control" name="periodo_chart" id="periodo_chart" required>
         <option value="" disabled selected>Selecione uma turno..</option>
          <option value="diurno">Diurno</option>
          <option value="noturno">Noturno</option>
        </select>
      </div>

      <div class="form-group col-md-3"> 
        <label>Semestre<small style="color: red">*</small></label>
        <select class="form-control" name="serie_chart" id="serie_chart" required>
         <option value="" disabled selected>Selecione uma semestre..</option>
          <option value="1">1ª Semestre</option>
          <option value="2">2ª Semestre</option>
          <option value="3">3ª Semestre</option>
          <option value="4">4ª Semestre</option>
          <option value="5">5ª Semestre</option>
          <option value="6">6ª Semestre</option>
          <option value="7">7ª Semestre</option>
          <option value="8">8ª Semestre</option>
          <option value="9">9ª Semestre</option>
          <option value="0">10ª Semestre</option>
        </select>
      </div>

     </div>

        <button class="btn btn-primary float-middle" onclick="gerar()">Visualizar</button>
  </form>
  <br/>
<hr/>
<?php
if ($curso=="" || $turno=="" || $serie=="") {
  $display_all="block";
  $display_filtro="none";
  
}else{
  $display_all="none";
  $display_filtro="block";
  if(isset($query_cursos) AND mysql_num_rows($query_cursos)==0){
    echo "<div class=\"message\"> Não há disciplinas para esse filtro..por favor, tente outro filtro. </div>";
  }
}
?>

<!-- DIV COM RESULTADO DE CONSULTA DO FORM ACIMA -->


<div id="container-chart" style="min-width: 310px; height: 400px; margin: 0 auto; display:<?php echo $display_filtro;?>;"></div>

<div id="container-all" style="min-width: 310px; height: 400px; margin: 0 auto; display:<?php echo $display_all;?>"></div>
<br/>
 </div>

<script type="text/javascript">
  Highcharts.setOptions({
    colors: ['#cccccc', '#058DC7','#50B432' ,'#ff0000' ]
});
  Highcharts.chart('container-chart', {
    chart: {
      type: 'column'
      
    },
    title: {
      text: 'Relação de agendamentos do curso de <?php echo $curso;?> - <?php echo $turno;?> - <?php echo $serie;?>ª Série'
    },
    xAxis: {
      categories: [
      <?php
      if ($curso=='EAD') {
         $sql="SELECT * FROM disciplina WHERE modalidade_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo' ORDER BY curso_disc";
      }else{
        $sql="SELECT * FROM disciplina WHERE curso_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo' AND modalidade_disc='Presencial' ORDER BY curso_disc";
      }
        
        $result=mysql_query($sql);
        
        while ($registro=mysql_fetch_array($result))
        {
          ?>
           '<?php echo $registro['nome_disc'];?>',
          <?php
            }
           ?>
         ],
       },
    yAxis: {
      min: 0,
      title: {
        text: 'Total de horas reservadas ( Horas / % )'
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
        }
      }
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y} Horas ({point.percentage:.0f}%)<br/>Total: {point.stackTotal} Horas'
    },
    plotOptions: {
      column: {
        stacking: 'percent',
        dataLabels: {
          enabled: true,
          color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
        }
      }
          },
    series: [{

      name: 'Carga Horária',
      
      data: [ 
        <?php foreach($cur_n as &$cur) {
          
          if ($curso=='EAD') {
            $query_carga=mysql_query("SELECT * FROM disciplina WHERE curso_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo'");
          }else{
            $query_carga=mysql_query("SELECT * FROM disciplina WHERE curso_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo'");
          }
          
          $array=mysql_fetch_array($query_carga);
          $referencia=$cur[1];

          $query=mysql_query("SELECT SUM(hora_aula) as total FROM agendadata WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada IN ('','presente') AND (data BETWEEN '$dataInicial' AND '$dataFinal') ");
          $query_saude=mysql_query("SELECT SUM(hora_aula) as total FROM agendadata_saude WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada IN ('','presente') AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
          $query_eng=mysql_query("SELECT SUM(hora_aula) as total FROM agendadata_eng WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada IN ('','presente') AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
          while ($arraySum=mysql_fetch_array($query) AND $arraySum_saude=mysql_fetch_array($query_saude) AND $arraySum_eng=mysql_fetch_array($query_eng))
        {
          if ($arraySum["total"]== null) {
            $valor=0;
          }else{
            $valor=$arraySum["total"];
          }
          if ($arraySum_saude["total"]== null) {
            $valor_saude=0;
          }else{
            $valor_saude=$arraySum_saude["total"];
          }
          if ($arraySum_eng["total"]== null) {
            $valor_eng=0;
          }else{
            $valor_eng=$arraySum_eng["total"];
          }
          $minutos_carga=$cur[6] * 60;
          $subTotal=$valor + $valor_saude + $valor_eng;
          $total=$minutos_carga - $subTotal;
          $valorTotalCarga=$total / 60;
          }
          ?>

          <?php echo round($valorTotalCarga); ?>,

          <?php 
        } ?>


      ]},{

        name: 'Reservados',
       
        data: [ 

      <?php foreach($cur_n as &$cur) {
       
        if ($curso=='EAD') {
            $query_carga=mysql_query("SELECT * FROM disciplina WHERE curso_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo'");
          }else{
            $query_carga=mysql_query("SELECT * FROM disciplina WHERE curso_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo'");
          }
          $array=mysql_fetch_array($query_carga);
          $referencia=$cur[1];

        $query=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada='' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_saude=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata_saude WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada='' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_eng=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata_eng WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada='' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        while ($arraySum=mysql_fetch_array($query) AND $arraySum_saude=mysql_fetch_array($query_saude) AND $arraySum_eng=mysql_fetch_array($query_eng))
        {
          if ($arraySum_saude["total"]== null) {
            $valor_saude=0;
          }else{
            $valor_saude=$arraySum_saude["total"];
          }
          $valorHoras_saude=$valor_saude/60;

          if ($arraySum_eng["total"]== null) {
            $valor_eng=0;
          }else{
            $valor_eng=$arraySum_eng["total"];
          }
          $valorHoras_eng=$valor_eng/60;

          if ($arraySum["total"]== null) {
            $valor=0;
          }else{
            $valor=$arraySum["total"];
          }
        $valorHoras=$valor/60;


        $valor_total_horas= $valorHoras + $valorHoras_saude + $valorHoras_eng;

          echo round($valor_total_horas);?>,
          <?php  } 
        } ?>

      ]},{

      name: 'Concluidos',
      
      data: [ 

      <?php foreach($cur_n as &$cur) {
        
       if ($curso=='EAD') {
            $query_carga=mysql_query("SELECT * FROM disciplina WHERE curso_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo'");
          }else{
            $query_carga=mysql_query("SELECT * FROM disciplina WHERE curso_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo'");
          }
          $array=mysql_fetch_array($query_carga);
          $referencia=$cur[1];

        $query=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada='presente' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_saude=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata_saude WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada='presente' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_eng=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata_eng WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada='presente' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        while ($arraySum=mysql_fetch_array($query) AND $arraySum_saude=mysql_fetch_array($query_saude) AND $arraySum_eng=mysql_fetch_array($query_eng))
        {
          if ($arraySum["total"]== null) {
            $valor=0;
          }else{
            $valor=$arraySum["total"];
          }
          if ($arraySum_saude["total"]== null) {
            $valor_saude=0;
          }else{
            $valor_saude=$arraySum_saude["total"];
          }
          if ($arraySum_eng["total"]== null) {
            $valor_eng=0;
          }else{
            $valor_eng=$arraySum_eng["total"];
          }
          $totalHora=$valor + $valor_saude + $valor_eng;
          $valorHoras=$totalHora/60;

          echo round($valorHoras);?>,
          <?php  } 
        } ?>

      ]},{

      name: 'Faltas',
     
      data: [ 

      <?php foreach($cur_n as &$cur) {
        
        if ($curso=='EAD') {
            $query_carga=mysql_query("SELECT * FROM disciplina WHERE curso_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo'");
          }else{
            $query_carga=mysql_query("SELECT * FROM disciplina WHERE curso_disc='$curso' AND turno_disc='$turno' AND semestre_disc='$serie' AND status_disc='Ativo'");
          }
          $array=mysql_fetch_array($query_carga);
          $referencia=$cur[1];

        $query=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada='faltou' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_saude=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata_saude WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada='faltou' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_eng=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata_eng WHERE referencia='$referencia' AND situation='RESERVADO' AND chamada='faltou' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        while ($arraySum=mysql_fetch_array($query) AND $array_saude=mysql_fetch_array($query_saude) AND $array_eng=mysql_fetch_array($query_eng))
        {
          if ($arraySum["total"]== null) {
            $valor=0;
          }else{
            $valor=$arraySum["total"];
          }
           if ($array_saude["total"]== null) {
            $valor_saude=0;
          }else{
            $valor_saude=$array_saude["total"];
          }
          if ($array_eng["total"]== null) {
            $valor_eng=0;
          }else{
            $valor_eng=$array_eng["total"];
          }
          $totalHora=$valor + $valor_saude + $valor_eng;
          $valorHoras=$totalHora/60;

          echo round($valorHoras);?>,
          <?php  } 
        } ?>

      ]},

        ]});

</script>





<script type="text/javascript">
  Highcharts.setOptions({
    colors: ['#cccccc', '#058DC7','#50B432' ,'#ff0000' ]
});
  Highcharts.chart('container-all', {
    chart: {
      type: 'column'
      
    },
    title: {
      text: 'Relação de agendamentos de todos os cursos'
    },
    xAxis: {
      categories: [
      <?php
        $sql="SELECT * FROM coordenadores WHERE status='ATIVO' ORDER BY sigla_curso";
        $result=mysql_query($sql);
        while ($registro=mysql_fetch_array($result))
        {
          ?>
           '<?php echo $registro['sigla_curso'];?>',
          <?php
            }
           ?>
         ],
       },
    yAxis: {
      min: 0,
      title: {
        text: 'Total de horas reservadas (Horas)'
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
        }
      }
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y} Horas ({point.percentage:.0f}%)<br/>Total: {point.stackTotal} Horas'
    },
    plotOptions: {
      column: {
        stacking: 'percent',
        dataLabels: {
          enabled: true,
          color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
        }
      }
          },
    series: [{

      name: 'Carga Horária',
      
      data: [ 
        <?php foreach($coordenador as $show) {
          $name_curso=$show[5];
          if ($name_curso=='EAD') {
            $query_carga=mysql_query("SELECT SUM(carghora_disc) as total FROM disciplina WHERE modalidade_disc='$show[5]' AND status_disc='Ativo' ");
          }else{
            $query_carga=mysql_query("SELECT SUM(carghora_disc) as total FROM disciplina WHERE curso_disc='$show[5]' AND status_disc='Ativo' AND modalidade_disc='Presencial' ");
          }
          
          while ($array=mysql_fetch_array($query_carga))
        {
          if ($array["total"]== null) {
            $valor_hora=0;
          }else{
            $valor_hora=$array["total"];
          }


          
           $query=mysql_query("SELECT SUM(hora_aula) as total FROM agendadata WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada IN ('','presente') AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
           $query_saude=mysql_query("SELECT SUM(hora_aula) as total FROM agendadata_saude WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada IN ('','presente') AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
           $query_eng=mysql_query("SELECT SUM(hora_aula) as total FROM agendadata_eng WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada IN ('','presente') AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        while ($arraySum=mysql_fetch_array($query) AND $arraySum_saude=mysql_fetch_array($query_saude) AND $arraySum_eng=mysql_fetch_array($query_eng))
        {
          if ($arraySum["total"]== null) {
            $valor=0;
          }else{
            $valor=$arraySum["total"];
          }
        
           if ($arraySum_saude["total"]== null) {
            $valor_saude=0;
          }else{
            $valor_saude=$arraySum_saude["total"];
          }
          if ($arraySum_eng["total"]== null) {
            $valor_eng=0;
          }else{
            $valor_eng=$arraySum_eng["total"];
          }
          $minutos_carga=$valor_hora * 60;
          $subTotal=$valor_saude + $valor + $valor_eng;
          $total=$minutos_carga - $subTotal;
          $valorTotalCarga=$total / 60;
        }

          ?>

          <?php echo round($valorTotalCarga); ?>,

          <?php }  } ?>


      ]},{

        name: 'Reservados',
       
        data: [ 

      <?php foreach($coordenador as $show) {
        $name_curso=$show[5];
        $query=mysql_query("SELECT SUM(hora_aula) as total FROM agendadata WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada='' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_saude=mysql_query("SELECT SUM(hora_aula) as total FROM agendadata_saude WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada='' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_eng=mysql_query("SELECT SUM(hora_aula) as total FROM agendadata_eng WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada='' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        while ($array=mysql_fetch_array($query) AND $array_saude=mysql_fetch_array($query_saude) AND $array_eng=mysql_fetch_array($query_eng))
        {
          if ($array["total"]== null) {
            $valor=0;
          }else{
            $valor=$array["total"];
          }
          if ($array_saude["total"]== null) {
            $valor_saude=0;
          }else{
            $valor_saude=$array_saude["total"];
          }
          if ($array_eng["total"]== null) {
            $valor_eng=0;
          }else{
            $valor_eng=$array_eng["total"];
          }
          $totalhoras=$valor_saude + $valor + $valor_eng;
        $valorHoras=$totalhoras/60;

          echo round($valorHoras);?>,
          <?php  } 
        } ?>

      ]},{

      name: 'Concluidos',
      
      data: [ 

      <?php foreach($coordenador as $show) {
        $name_curso=$show[5];
        $query=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada='presente' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_saude=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata_saude WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada='presente' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_eng=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata_eng WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada='presente' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        while ($array=mysql_fetch_array($query) AND $array_saude=mysql_fetch_array($query_saude) AND $array_eng=mysql_fetch_array($query_eng))
        {
          if ($array["total"]== null) {
            $valor=0;
          }else{
            $valor=$array["total"];
          }
          if ($array_saude["total"]== null) {
            $valor_saude=0;
          }else{
            $valor_saude=$array_saude["total"];
          }
          if ($array_eng["total"]== null) {
            $valor_eng=0;
          }else{
            $valor_eng=$array_eng["total"];
          }
          $totalHoras=$valor + $valor_saude + $valor_eng;
          $valorHoras=$totalHoras/60;

          echo round($valorHoras);?>,
          <?php  } 
        } ?>

      ]},{

      name: 'Faltas',
     
      data: [ 

      <?php foreach($coordenador as $show) {
        $name_curso=$show[5];
        $query=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada='faltou' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_saude=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata_saude WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada='faltou' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        $query_eng=mysql_query("SELECT *,SUM(hora_aula) as total FROM agendadata_eng WHERE Disciplina='$name_curso' AND situation='RESERVADO' AND chamada='faltou' AND (data BETWEEN '$dataInicial' AND '$dataFinal')");
        while ($array=mysql_fetch_array($query) AND $array_saude=mysql_fetch_array($query_saude) AND $array_eng=mysql_fetch_array($query_eng))
        {
          if ($array["total"]== null) {
            $valor=0;
          }else{
            $valor=$array["total"];
          }
          if ($array_saude["total"]== null) {
            $valor_saude=0;
          }else{
            $valor_saude=$array_saude["total"];
          }
          if ($array_eng["total"]== null) {
            $valor_eng=0;
          }else{
            $valor_eng=$array_eng["total"];
          }
          $totalHoras=$valor + $valor_saude + $valor_eng;
          $valorHoras=$totalHoras/60;

          echo round($valorHoras);?>,
          <?php  } 
        } ?>

      ]},

        ]});

</script>



</body>
</html>