<?php  
  $titlepag="AgendaLAB - FAC 3";
  header('Content-Type: text/html; charset=utf-8');
  include("includes/headerlog.php");
  
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="css/images/favicon2.png" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php echo $titlepag ?></title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<style type="text/css">
	body{
		background-image: url("css/images/fac3_op.jpg");
		background-size: 100% 100% ;

	}
</style>
</head>
<body>

<div class="w3-container w3-third w3-card-4 w3-border-blue w3-topbar w3-bottombar w3-display-middle w3-white" >	 
	<div class="w3-container w3-pale-red w3-display-container">
          <p class="w3-large">Senha resetada!</p> <p>Para continuar mude sua senha:</p>
    </div>
	    <form action="?acao=Asen_reset" method="post" class="w3-animate-zoom" >
	        <p>
		       <label for="Logon"><h4>Nova Senha</h4></label>
		       <input id="Logon" type="password" class="w3-input w3-pale-blue" name="senha1" placeholder="Digite aqui.." />
		    </p>
		    <p>
		       <label for="Senha"><h4>Repita a Senha:</h4></label>
		       <input id="Senha" type="password" class="w3-input w3-pale-blue" name="Senha2" placeholder="Digite aqui.." />
		    </p>
		  <div class="w3-center w3-margin w3-large">
		  <input type="submit" class="w3-btn w3-blue" value="Alterar"/>
		  </div>
		</form>

</div>	


<div id="footer" class="w3-container w3-display-bottommiddle">
<p class="w3-text-black">© Copyright</p>
</div>

</body>
</html>