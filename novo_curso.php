<?php 
  $titlepag="FAC3 - Novo Laboratório";
  include("includes/inserir.header.php");
  header('Content-Type: text/html; charset=utf-8');

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php echo $titlepag ?></title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<style type="text/css">
	body{
		background-color: white;
	}
</style>
</head>

<body>
  <div class="w3-card-4 w3-display-middle" style="width: 40%">
    	<header class="w3-container w3-blue w3-center">
    		<h2>Novo Laboratório</h2>
    	</header>
	 <div class="message" style="<?php echo $display;?>"><?php echo $msg;?></div>
	<div id="disable" class="w3-container w3-center">
          <form action="?acao=inserir_c" method="post">
          	<br/>
             <label for="nome_curso" class="w3-left-align"><p><b>Nome do Curso:</b></p></label><input autofocus id="nome_curso" placeholder="Recursos Humanos" type="text" name="nome_curso" class="w3-input"/>
             <br/>
		     <label for="sigla_curso" class="w3-left-align"><p><b>Abreviação do Curso:</b></p></label><input autofocus id="sigla_curso" placeholder="RH" name="sigla_curso" type="text" class="w3-input"/>
		     <br/>
		     <div class="w3-left-align">
		     <p><b>Status do curso</b></p>
		     <input type="radio" id="enable" name="status" class="w3-radio" value="ATIVO"/><label for="status"> Ativo</label>
		     <input type="radio" checked id="disable"  name="status" class="w3-radio" value="DESATIVADO"/><label for="status"> Desativado</label><br>
		     </div>
		     <br/>
		     <input type="submit" class="w3-btn w3-blue w3-margin w3-right" value="Adicionar"/>
		     <a href="config_cursos.php" class="w3-btn w3-red w3-margin w3-left" >Voltar</a>
		  </form>
        </div>
	 <!--login-->
	</div>
</body>
</html>