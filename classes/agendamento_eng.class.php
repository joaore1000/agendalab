<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


require_once 'vendor/autoload.php';
date_default_timezone_set('America/Sao_Paulo');
header('Content-Type: text/html; charset=utf-8');

  class agendaEngenharia{
	  public function consultarData($dataBD,$periodo){
		  // Metodo de consultar Lab disponivel
		  $pesquisar=mysql_query("SELECT * FROM lab_eng WHERE lab_eng . status_eng = 'ATIVO' AND lab_eng . id_eng NOT IN (SELECT lab_eng . id_eng FROM agendadata_eng WHERE agendadata_eng . data ='$dataBD' AND lab_eng . nome_lab = agendadata_eng . Lab AND agendadata_eng . periodo = '$periodo' AND NOT situation='CANCELADO');") or die(mysql_error());
		  $_SESSION["periodo"]=$periodo;
		  $conta=mysql_num_rows($pesquisar);
		  
		  $contador = 0;
		  while ($row = mysql_fetch_array($pesquisar, MYSQL_NUM)) {
    			$dado[$contador][0]  = $row[0];
    			$dado[$contador][1]  = $row[1];
          $dado[$contador][2]  = $row[2];
          $dado[$contador][3]  = $row[3];
    			$contador++;
		  }		
		    if(isset($dado)){
		    	$_SESSION["dado_eng"]=$dado;
		    	echo "<br/>
				<div class=\"container text-center alert alert-success alert-dismissible fade show\" role=\"alert\">
		    		<h3>Oba!..</h3>
		    		<p><b>".$conta."</b> laboratório(s) disponivei(s) na data e periodo selecionado...</p>
		    	</div>";
		    } else {

				echo "<br/>
				<div class=\"container text-center alert alert-danger alert-dismissible fade show\" role=\"alert\">
		    		<h3>Ops!..</h3>
		    		<p>Não há laboratórios disponiveis na data e periodo selecionado. Por favor tente outra data ou periodo...</p>
				</div>";
			
		    	unset($_POST['dataAg']);

		    }
		    
		      

  }

    public function agendarEngenharia ($nome,$sbname,$cood,$userProf,$turma,$curso,$mail,$dataBD,$periodo,$alunos,$disci,$aulas,$lab,$soft,$horaum){

      $turmas="";
      $n=count($turma);
      for ($i = 0; $i < $n; $i++) {
            $turmas.=$turma[$i].";";
      }


      $tempo = $aulas[0]." Até ".$aulas[1];
      //Calcula o tempo de aula
      $entrada = $aulas[0];
      $saida = $aulas[1];
      $hora1 = explode(":",$entrada);
      $hora2 = explode(":",$saida);
      $acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60);
      $acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60);
      $resultado = $acumulador2 - $acumulador1;
      $hora_ponto = floor($resultado / 3600);
      $resultado = $resultado - ($hora_ponto * 3600);
      $min_ponto = floor($resultado / 60);
      $resultado = $resultado - ($min_ponto * 60);
      $secs_ponto = $resultado;
      //Grava na variável resultado final
      $hora_aula = $hora_ponto.":".$min_ponto.":".$secs_ponto;

      $strStart=date("Y-m-d H:i");
      $strEnd=$dataBD." ".$horaum;
      $dteAtual = new DateTime($strStart);
      $dteAgendada = new DateTime($strEnd);

      $dteDif = $dteAtual->diff($dteAgendada);
      $horas=$dteDif->h + ($dteDif->days * 24);
if ($horas < 24){
            $add=mysql_query("SELECT bloqueio,bloqueio_total FROM usuarios WHERE email='$mail'");
             $addexe=mysql_fetch_array($add);
             $block=$addexe[0];
             $block_total=$addexe[1];
            $block++;
            $block_total++;
             
	  		$bloqueio="UPDATE usuarios SET Estato='BLOQUEADO', bloqueio='$block', bloqueio_total='$block_total' WHERE email='$mail' AND nivel<=3";
		    $bloqueioexe=mysql_query($bloqueio) or die("Erro no query". mysql_error());
}


        //Inserção no banco de dados 
         $insert=mysql_query("INSERT INTO agendadata_eng 
        (agendado_por,sobrenome,Coordenador,agendado_para,Disciplina,email,data,periodo,Quantidade,Aula,turmas,Softwares,Lab,situation,adicionado,hora_aula,aula_total) 
        VALUES 
        ('$nome','$sbname','$cood','$userProf','$curso','$mail','$dataBD','$periodo','$alunos','$disci','$turmas','$soft','$lab','AGUARDANDO','$strStart','$hora_aula','$tempo')");
		  if(!isset($bloqueio)){
         
                
            //Seleciona o Nome do Coordenador
            $query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$cood'");
            $array=mysql_fetch_array($query_coord);
            //Seleciona o Nome da Disciplina
           
            $disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$disci'") or die(mysql_error());
            $nome_disciplina=mysql_fetch_assoc($disciplina);
            //Seleciona o Nome do curso
            $cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
            $nome_curso=mysql_fetch_assoc($cursoMq);
            //Usuário agendado
            
            $userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userProf'") or die(mysql_error());
            $agendado_para=mysql_fetch_assoc($userMq);
            $emailUserTo=$agendado_para["email"];
            $emailUserFor=$mail;
         
         
	$situation = "AGUARDANDO APROVAÇÃO";
                
			          $dataemail = $dataBD;
                      $dataDiv = explode('-', $dataemail);
                      $dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];
			          
   $assunto="Solicitação de Agendamento";
             
   $mensagem="<html>            
              <head>
              <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
              </head>
              <body>
              <h1>AgendaLAB - Leste Sul</h1>
              <div id='mensage' class='bradius' style='top:80px'>
              <div class='mail'>
              <h4>".ucfirst($nome).",<p> Sua solicitação foi realizada com sucesso, aguardando aprovação do setor da Engenharia</p></h4>
              <fieldset><legend>
              <h2>Informações</h2></legend><br/>
              <b>Agendado Para: </b>".$agendado_para["Nome"]." (em cópia).<br/>
              <b>Laboratório: </b>".$lab."<br/>
              <b>Data: </b>".$dataBR."<br/>
              <b>Periodo: </b>".$periodo."<br/>
              <b>Curso: </b>".$nome_curso["curso"]."<br/>
              <b>Disciplina: </b>".$nome_disciplina["disciplina"]."<br/>
              <b>Tempo de aulas: </b>".$tempo."<br/>
              <b>Status: </b>".$situation."<br/>
              <b>Observações: </b>".$soft."<br/>
              </fieldset>
              <h4>Você receberá um e-mail com a Aprovação do agendamento.</h4>
              <h4 style='color:#003399'><b>ATENÇÃO</b> - Sempre realize seu agendamento ou cancelamento com no mínimo 24 horas de antecedência.</h4>
              <p>_____________________________________</p>
              <h4 style='color:#666666'>Por favor não responda a esta mensagem. Este é um e-mail automático.</h4><br/>
              <img src='http://tifac3.com.br/css/images/Assinatura.png'>
              </div>
              </div>
              </body>
              </html>"; 

             $mail = new PHPMailer();
             $mail->isSMTP();
             $mail->SMTPDebug = 0;
             $mail->CharSet = 'UTF-8';
             $mail->isHTML();
             $mail->Host = 'smtp.gmail.com';
             $mail->Port = 465;
             $mail->SMTPAuth = true;
             $mail->SMTPSecure = "ssl";  
             $mail->Username = "fac3.agendamento@gmail.com";
             $mail->Password = "infn@@1414";
             $mail->setFrom('fac3.agendamento@gmail.com', 'AgendaLAB - Engenharia');
             $mail->addAddress($emailUserFor, $nome);
             $mail->AddCC($emailUserTo);
             $mail->Subject = $assunto;
             $mail->Body = $mensagem;
             $mail->Send();
             echo "<br/><div class=\"container text-center alert alert-success alert-dismissible fade show\" role=\"alert\">
		    	<h3>Sucesso!..</h3>
		    	<p>Sua solicitação foi realizado com sucesso. Você será receberá um e-mail com a aprovação.</p>
		    	</div>
                ";
			  }else{

        //Seleciona o Nome do Coordenador  
        $query_coord=mysql_query("SELECT * FROM coordenadores WHERE id_cood='$cood'") or die(mysql_error());
        $array=mysql_fetch_array($query_coord);
        $emailUserCoord=$array["email"];

        //Seleciona o Nome da Disciplina
        $disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$disci'") or die(mysql_error());
        $nome_disciplina=mysql_fetch_assoc($disciplina);

        //Seleciona o Nome do curso
        $cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
        $nome_curso=mysql_fetch_assoc($cursoMq);

        //Usuário agendado
        $userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userProf'") or die(mysql_error());
        $agendado_para=mysql_fetch_assoc($userMq);
        $emailUserTo=$agendado_para["email"];
        $emailUserFor=$mail;

          $situation = "AGUARDANDO APROVAÇÃO";
                
          $dataemail = $dataBD;
                $dataDiv = explode('-', $dataemail);
                $dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];

          $assunto="Solicitação de Agendamento e bloqueio";
             
   $mensagem="<html>            
              <head>
              <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
              </head>
              <body>
              <h1>AgendaLAB - FAC 3 Taquaral</h1>
              <div id='mensage' class='bradius' style='top:80px'>
              <div class='mail'>
              <h4>".ucfirst($nome).",<p> Sua solicitação foi realizada com sucesso, aguardando aprovação do setor de Engenharia</p></h4>
              <h4><p> Você foi bloqueado por solicitar agendamento com menos de 24 horas de antecedência, contate seu Coordenador para efetuar o desbloqueio!</p></h4>
              <fieldset><legend>
              <h2>Informações</h2></legend><br/>
              <b>Agendado Para: </b>".$agendado_para["Nome"]." (em cópia).<br/>
              <b>Laboratório: </b>".$lab."<br/>
              <b>Data: </b>".$dataBR."<br/>
              <b>Periodo: </b>".$periodo."<br/>
              <b>Curso: </b>".$nome_curso["curso"]."<br/>
              <b>Disciplina: </b>".$nome_disciplina["disciplina"]."<br/>
              <b>Tempo de aulas: </b>".$tempo."<br/>
              <b>Status: </b>".$situation."<br/>
              <b>Observações: </b>".$soft."<br/>
              </fieldset>
              <h4>Você receberá um e-mail com a Aprovação do agendamento.</h4>
              <h4 style='color:#003399'><b>ATENÇÃO</b> - Sempre realize seu agendamento ou cancelamento com no mínimo 24 horas de antecedência.</h4>
              <p>_____________________________________</p>
              <h4 style='color:#666666'>Por favor não responda a esta mensagem. Este é um e-mail automático.</h4><br/>
              <img src='http://tifac3/agendalab/css/msgs/Assinatura.png'>
              </div>
              </div>
              </body>
              </html>"; 

             $mail = new PHPMailer();
             $mail->isSMTP();
             $mail->SMTPDebug = 0;
             $mail->CharSet = 'UTF-8';
             $mail->isHTML();
             $mail->Host = 'smtp.gmail.com';
             $mail->Port = 465;
             $mail->SMTPAuth = true;
             $mail->SMTPSecure = "ssl";  
             $mail->Username = "fac3.agendamento@gmail.com";
             $mail->Password = "infn@@1414";
             $mail->setFrom('fac3.agendamento@gmail.com', 'AgendaLAB - Engenharia');
             $mail->addAddress($emailUserFor, $nome);
             $mail->AddCC($emailUserTo);
             $mail->AddCC($emailUserCoord);
             $mail->Subject = $assunto;
             $mail->Body = $mensagem;
             $mail->Send();
             echo "<br/><div class=\"container text-center alert alert-warning alert-dismissible fade show\" role=\"alert\">
             <h3>Sucesso!..</h3>
             <p>Sua solicitação foi realizado com sucesso, mas você foi bloqueado por agendar com menos de 24 Horas de Antecedência, procure seu coordenador.!</p>
             </div>
                 ";
        }

    }
    

    
      public function aprovarAgendamentoEng(){
        if(isset($_GET["number"])){
                 $id=$_GET["number"];
                 $query=mysql_query("UPDATE agendadata_eng SET situation='RESERVADO' WHERE ID='$id'")or die("Erro no query ". mysql_error());
                $flash="<p><h4>AGENDAMENTO AUTORIZADO</h4></p>";
                   $select="SELECT * FROM agendadata_eng WHERE ID='$id'";
                   $execute=mysql_query($select)or die("ERRO".mysql_error());
                   $mfa=mysql_fetch_assoc($execute);
                 
  
                  $situation = "RESERVADO";
                  $dataemail = $mfa["data"];
                  $dataDiv = explode('-', $dataemail);
                  $dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];
                  $agendado_por=$mfa["agendado_por"];
                
            //Seleciona o Nome do Coordenador
            $id_cood=$mfa["Coordenador"];
            $query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$id_cood'");
            $array=mysql_fetch_array($query_coord);
            //Seleciona o Nome da Disciplina
            $aula=$mfa["Aula"];
            $disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$aula'") or die(mysql_error());
            $nome_disciplina=mysql_fetch_assoc($disciplina);
            //Seleciona o Nome do curso
            $curso=$mfa["Disciplina"];
            $cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
            $nome_curso=mysql_fetch_assoc($cursoMq);
            //Usuário agendado
            $userRequest=$mfa["agendado_para"];
            $userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userRequest'") or die(mysql_error());
            $agendado_para=mysql_fetch_assoc($userMq);
            $emailUserTo=$agendado_para["email"];
            $emailUserFor=$mfa["email"];

     $assunto="Agendamento APROVADO";
               
     $mensagem="<html>            
                <head>
                <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                </head>
                <body>
                <h1>AgendaLAB - FAC 3 Taquaral</h1>
                <div style='top:80px'>
                <div>
                <h4>".ucfirst($agendado_por).",<p>Seu agendamento foi APROVADO e está reservado com sucesso!</p></h4>
                <fieldset><legend>
                <h2>Informações</h2></legend><br/>
                <b>Agendado para: </b>".$agendado_para["Nome"]." (Adicionado em Cópia)<br/>
                <b>Laboratório: </b>".$mfa["Lab"]."<br/>
                <b>Data: </b>".$dataBR."<br/>
                <b>Periodo: </b>".$mfa["periodo"]."<br/>
                <b>Tempo de Aula: </b>".$mfa["aula_total"]."<br/>
                <b>Curso: </b>".$nome_curso["curso"]."<br/>
                <b>Disciplina: </b>".$nome_disciplina["disciplina"]."<br/>
                <b>Status: </b>".$situation."<br/>
                <b>Observações: </b>".$mfa["Softwares"]."<br/>
                </fieldset>
                <h4 style='color:#003399'><b>ATENÇÃO</b> - Sempre realize seu agendamento ou cancelamento com no mínimo 24 horas de antecedência.</h4>
                <p>_____________________________________</p>
                <h4 style='color:#666666'>Por favor não responda a esta mensagem. Este é um e-mail automático.</h4><br/>
                <img src='http://tifac3/agendalab/css/msgs/Assinatura.png'>
                </div>
                </div>
                </body>
                </html>"; 
  
  
               $mail = new PHPMailer();
               $mail->isSMTP();
               $mail->SMTPDebug = 0;
               $mail->CharSet = 'UTF-8';
               $mail->isHTML();
               $mail->Host = 'smtp.gmail.com';
               $mail->Port = 465;
               $mail->SMTPAuth = true;
               $mail->SMTPSecure = "ssl";  
               $mail->Username = "fac3.agendamento@gmail.com";
               $mail->Password = "infn@@1414";
               $mail->setFrom('fac3.agendamento@gmail.com', 'AgendaLAB - Engenharia');
               $mail->addAddress($emailUserFor, $agendado_por);
               $mail->AddCC($emailUserTo);
               $mail->Subject = $assunto;
               $mail->Body = $mensagem;
               $mail->Send();
              
  
      }elseif(empty($flash)){
        $flash="Erro de Aprovação";
  }
  
  echo "<div class=\"w3-panel w3-border-blue w3-round-xxlarge w3-blue w3-center w3-display-top\">";
  echo $flash;
  echo "</div>";
  
  }

  public function cancelarAgendamento(){
    if(isset($_GET["number"])){
             $id=$_GET["number"];
       $query=mysql_query("UPDATE agendadata_eng SET situation='CANCELADO' WHERE id='$id'")or die("Erro no query ". mysql_error());
       $flash="<p><h4>Agendamento ".$_GET["number"]." Cancelado!</h4></p>";
               $select="SELECT * FROM agendadata_eng WHERE id='$id'";
               $execute=mysql_query($select)or die("ERRO".mysql_error());
               $mfa=mysql_fetch_assoc($execute);
             


              $situation = "CANCELADO";
              $dataemail = $mfa["data"];
              $dataDiv = explode('-', $dataemail);
              $dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];

              $agendado_por=$mfa["agendado_por"];
              //Seleciona o Nome do Coordenador
              $id_cood=$mfa["Coordenador"];
              $query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$id_cood'");
              $array=mysql_fetch_array($query_coord);
              //Seleciona o Nome da Disciplina
              $aula=$mfa["Aula"];
              $disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$aula'") or die(mysql_error());
              $nome_disciplina=mysql_fetch_assoc($disciplina);
              //Seleciona o Nome do curso
              $curso=$mfa["Disciplina"];
              $cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
              $nome_curso=mysql_fetch_assoc($cursoMq);
              //Usuário agendado
              $userRequest=$mfa["agendado_para"];
              $userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userRequest'") or die(mysql_error());
              $agendado_para=mysql_fetch_assoc($userMq);
              $emailUserTo=$agendado_para["email"];
              $emailUserFor=$mfa["email"];

        
 $assunto="Cancelamento de Agendamento";
           
 $mensagem="<html>            
            <head>
            <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
            </head>
            <body>
            <h1>AgendaLAB - Leste Sul</h1>
            <div style='top:80px'>
            <div>
            <h4>".ucfirst($agendado_por).",<p>Seu agendamento foi cancelado com sucesso, caso não reconheça o cancelamento, contate o setor de Engenharia da unidade!</p></h4>
            <fieldset><legend>
            <h2>Informações</h2></legend><br/>
            <b>Agendado para: </b>".$agendado_para["Nome"]." (Adicionado em Cópia)<br/>
            <b>Laboratório: </b>".$mfa["Lab"]."<br/>
            <b>Data: </b>".$dataBR."<br/>
            <b>Periodo: </b>".$mfa["periodo"]."<br/>
            <b>Tempo de Aula: </b>".$mfa["aula_total"]."<br/>
            <b>Curso: </b>".$nome_curso["curso"]."<br/>
            <b>Disciplina: </b>".$nome_disciplina["disciplina"]."<br/>
            <b>Status: </b>".$situation."<br/>
            <b>Observações: </b>".$mfa["Softwares"]."<br/>
            </fieldset>
            <h4 style='color:#003399'><b>ATENÇÃO</b> - Sempre realize seu agendamento ou cancelamento com no mínimo 24 horas de antecedência.</h4>
            <p>_____________________________________</p>
            <h4 style='color:#666666'>Por favor não responda a esta mensagem. Este é um e-mail automático.</h4><br/>
            <img src='http://tifac3/agendalab/css/msgs/Assinatura.png'>
            </div>
            </div>
            </body>
            </html>"; 


           $mail = new PHPMailer();
           $mail->isSMTP();
           $mail->SMTPDebug = 0;
           $mail->CharSet = 'UTF-8';
           $mail->isHTML();
           $mail->Host = 'smtp.gmail.com';
           $mail->Port = 465;
           $mail->SMTPAuth = true;
           $mail->SMTPSecure = "ssl";  
           $mail->Username = "fac3.agendamento@gmail.com";
           $mail->Password = "infn@@1414";
           $mail->setFrom('fac3.agendamento@gmail.com', 'AgendaLAB - Engenharia');
           $mail->addAddress($emailUserFor, $agendado_por);
           $mail->AddCC($emailUserTo);
           $mail->Subject = $assunto;
           $mail->Body = $mensagem;
           $mail->Send();
          

  }elseif(empty($flash)){
    $flash="Erro de sistema!";
}
echo "<div class=\"w3-panel w3-border-blue w3-round-xxlarge w3-blue w3-center w3-display-top\">";
echo $flash;
echo "</div>";
}


  }

?>