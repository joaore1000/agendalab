<?php
class Paginacao {

	public $total; 			// armazena o total de registros
	public $pg_atual;		// armazena  pagina atual
	public $mostrar;		// armazena a quantidade de resultados a mostrar 
	public $fim;			// armazena os valores "fim,inicio" da da função limit do mysql
	public $menos;			// armazena a pagina "anterior"
	public $mais;			// armazena a pagina "proximo"

function __construct($qtd , $pag){	
	if($pag < 0 || !isset($pag)){
		$pag = 1;
	}

	$this->mostrar = 5;
	$this->total = $qtd;
	$this->pg_atual = $pag;		
	$this->paginas = ceil($this->total/$this->mostrar);	

	if($this->pg_atual > $this->paginas){
		$this->pg_atual = $this->paginas;
	}		

	$this->menos = $this->pg_atual-1;
	$this->mais = $this->pg_atual+1;
}	

// calcula a pagina "anterior"
function menos($paginar){
	if($this->menos >= 1){
		return "<li class=\"page-item\"><a class=\"page-link\" href=\"?pagina".$paginar."=". $this->menos . "\">" . $this->menos . "</a></li> ";
	}
}

// retorna a pagina atual	
function atual(){
	return "<li class=\"page-item active\" aria-current=\"page\"> <span class=\"page-link\">".$this->pg_atual . "<span class=\"sr-only\">(current)</span></span></li>";
}

// calcula a pagina "proximo"
function mais($paginar){
	if($this->mais <= $this->paginas){
		return "<li class=\"page-item\"> <a class=\"page-link\" href=\"?pagina".$paginar."=". $this->mais . "\">" . $this->mais . "</a></li>";
	}
}

// exibi a paginação
function paginar($paginar){
	?>
	<nav aria-label="Paginacao">
  		<ul class="pagination justify-content-center pagination-sm">
			<?php
				print $this->menos($paginar) . $this->atual() . $this->mais($paginar);
			?>
		</ul>
	</nav>
	<?php
}

// retorna os valores da função limite do mysql
function limite(){
	$this->fim = ($this->pg_atual*$this->mostrar)-$this->mostrar . "," . $this->mostrar;
	return $this->fim;
}
}

?>