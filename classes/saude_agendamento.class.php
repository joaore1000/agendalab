<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


require_once 'vendor/autoload.php';
date_default_timezone_set('America/Sao_Paulo');
header('Content-Type: text/html; charset=utf-8');

  class saudeAgendamento{
	  public function verificarData($dataBD,$periodo){

	  	switch ($periodo) {
	  			case 'M12':
	  			$periodo_query="'M1','M2','M12'";
	  			break;

	  			case 'M1':
	  			$periodo_query="'M1','M12'";
	  			break;

	  			case 'M2':
	  			$periodo_query="'M2','M12'";
	  			break;

	  			case 'N12':
	  			$periodo_query="'N1','N2','N12'";
	  			break;

	  			case 'N1':
	  			$periodo_query="'N1','N12'";
	  			break;

	  			case 'N2':
	  			$periodo_query="'N2','N12'";
	  			break;

	  			default:
	  			$periodo_query="";
	  			break;
	  	
	  	}
		  // Metodo de consultar Lab disponivel
		  $pesquisar=mysql_query("SELECT * FROM lab_saude WHERE lab_saude . status_saude = 1 AND lab_saude . id_saude NOT IN (SELECT lab_saude . id_saude FROM agendadata_saude WHERE agendadata_saude . data ='$dataBD' AND lab_saude . nome_lab = agendadata_saude . Lab AND agendadata_saude . periodo IN ($periodo_query) AND NOT situation='CANCELADO');") or die(mysql_error());
		  $_SESSION["periodoSelect"]=$periodo;
		  $conta=mysql_num_rows($pesquisar);
		  
		  $contador = 0;
		  while ($row = mysql_fetch_array($pesquisar, MYSQL_NUM)) {
    			$dado[$contador][0]  = $row[0];
                  $dado[$contador][1]  = $row[1];
                  $dado[$contador][2]  = $row[2];
                  $dado[$contador][3]  = $row[3];
    			$contador++;
		  }		
		    if(isset($dado)){
		    	$_SESSION["dado_saude"]=$dado;
		    	
		    } else {

                echo "<br/><div class=\"container text-center alert alert-danger alert-dismissible fade show\" role=\"alert\">
		    	<h3>Ops!..</h3>
		    	<p>Não há laboratorios disponiveis na data e periodo selecionado. Por favor tente outra data ou periodo...</p>
		    	</div>
                ";
		    	
		    	unset($_POST['dataAg']);

		    }
		    
		      

  }

  		public function agendarSaude($nome,$sbname,$cood,$userProf,$turma,$curso,$mail,$dataBD,$periodo,$alunos,$disci,$aulas,$lab,$soft,$horaum){

                  $turmas="";
            $n=count($turma);
            for ($i = 0; $i < $n; $i++) {
                  $turmas.=$turma[$i].";";
            }
            $tempo = $aulas[0]." Até ".$aulas[1];
      //Calcula o tempo de aula
      $entrada = $aulas[0];
      $saida = $aulas[1];
      $hora1 = explode(":",$entrada);
      $hora2 = explode(":",$saida);
      $acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60);
      $acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60);
      $resultado = $acumulador2 - $acumulador1;
      $hora_ponto = floor($resultado / 3600);
      $resultado = $resultado - ($hora_ponto * 3600);
      $min_ponto = floor($resultado / 60);
      $resultado = $resultado - ($min_ponto * 60);
      $secs_ponto = $resultado;
      //Grava na variável resultado final
      $hora_aula = $hora_ponto.":".$min_ponto.":".$secs_ponto;
      

      $strStart=date("Y-m-d H:i");
      $strEnd=$dataBD." ".$horaum;
      $dteAtual = new DateTime($strStart);
      $dteAgendada = new DateTime($strEnd);

      $dteDif = $dteAtual->diff($dteAgendada);
      $horas=$dteDif->h + ($dteDif->days * 24);

      if ($horas < 168){
            $add=mysql_query("SELECT bloqueio,bloqueio_total FROM usuarios WHERE email='$mail'");
             $addexe=mysql_fetch_array($add);
             $block=$addexe[0];
             $block_total=$addexe[1];
            $block++;
            $block_total++;
             
	  		$bloqueio="UPDATE usuarios SET Estato='BLOQUEADO', bloqueio='$block', bloqueio_total='$block_total' WHERE email='$mail' AND nivel<=3";
		    $bloqueioexe=mysql_query($bloqueio) or die("Erro no query". mysql_error());
}

        //Inserção no banco de dados 
         $insert=mysql_query("INSERT INTO agendadata_saude 
        (agendado_por,sobrenome,Coordenador,agendado_para,Disciplina,email,data,periodo,quantidade,Aula,turmas,Lab,Softwares,situation,adicionado,hora_aula,aula_total) 
        VALUES 
        ('$nome','$sbname','$cood','$userProf','$curso','$mail','$dataBD','$periodo','$alunos','$disci','$turmas','$lab','$soft','AGUARDANDO','$strStart','$hora_aula','$tempo')");

if(!isset($bloqueio)){

      //Seleciona o Nome do Coordenador
      $query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$cood'");
      $array=mysql_fetch_array($query_coord);
      //Seleciona o Nome da Disciplina
      $disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$disci'") or die(mysql_error());
      $nome_disciplina=mysql_fetch_assoc($disciplina);
      //Seleciona o Nome do curso
      $cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
      $nome_curso=mysql_fetch_assoc($cursoMq);
      //Usuário agendado
      $userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userProf'") or die(mysql_error());
      $agendado_para=mysql_fetch_assoc($userMq);
      $emailUserTo=$agendado_para["email"];
      $emailUserFor=$mail;
         
    switch ($periodo) {
          case 'M12':
          
          $emailPerdiodo="Matutino - 1º e 2º Periodo - ".$tempo;
          break;

          case 'M1':
                $emailPerdiodo="Matutino - 1º Periodo - ".$tempo;
          break;

          case 'M2':
                $emailPerdiodo="Matutino - 2º Periodo - ".$tempo;
          break;

          case 'N12':
          
          $emailPerdiodo="Noturno - 1º e 2º Periodo - ".$tempo;
          break;

          case 'N1':
                $emailPerdiodo="Noturno - 1º Periodo - ".$tempo;
          break;

          case 'N2':
                $emailPerdiodo="Noturno - 2º Periodo - ".$tempo;
          break;

      
      }
         
         
	$situation = "Aguardando Aprovação do Setor de Saude";
                
			          $dataemail = $dataBD;
                $dataDiv = explode('-', $dataemail);
                $dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];
			          $email=$mail;
   $assunto="AgendaLAB - Saude";
             
   $mensagem="<html>            
              <head>
              <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
              </head>
              <body>
              <h1>Sistema AgendaLAB - Leste Sul</h1>
              <div id='mensage' style='top:80px'>
              <div>
              <h4>".ucfirst($agendado_para["Nome"]).",<p> Um Laboratório foi reservado para você pelo ".$nome.".</p></h4>
              <fieldset><legend>
              <h2>Informações</h2></legend><br/>
              <b>Agendado Para: </b>".$agendado_para["Nome"]." (em cópia).<br/>
              <b>Laboratório: </b>".$lab."<br/>
              <b>Data: </b>".$dataBR."<br/>
              <b>Periodo: </b>".$emailPerdiodo."<br/>
              <b>Curso: </b>".$nome_curso["curso"]."<br/>
              <b>Disciplina: </b>".$nome_disciplina["disciplina"]."<br/>
              <b>Tempo de aulas: </b>".$tempo."<br/>
              <b>Status: </b>".$situation."<br/>
              <b>Materiais: </b>".$soft."<br/>
              </fieldset>
              <h4 style='color:#003399'>ATENÇÃO - Caso necessite de materiais, acesse o portal <a href='http://www.tifac3.com.br/'> AgendaLAB </a> e preencha no campo observações. Fique atento ao prazo de 1 semana de antecedência.</h4>
              <p>_____________________________________</p>
              <h4 style='color:#666666'>Por favor não responda a esta mensagem. Este é um e-mail automático.</h4><br/>
              <table cellspacing='2' cellpadding='1'>
<tbody><tr>

<td style='border-right:thin #999 solid; width:=92px; height:=50px; padding-left:2px;'><img src='css/images/anhanguera_assinatura.jpg'></td>

<td style='padding:10px 7px;'>

<font style='color:#4d4d4f; font-family: Calibri; font-size:18px;'><b>AgendaLAB - Saúde</b></font><br><font style='color:#4d4d4f; font-family: Calibri; font-size:12px;text-transform:uppercase;'>Núcleo de Informática</font><br><font style='color:#4d4d4f; font-family: Calibri ; font-size:10px;'>(19) 3512-3109</font><br><font font=' style='color:#4d4d4f; font-family: Calibri ; font-size:10px;font-weight: bold;'>www.tifac3.com.br</font>

</td>
</tr>

</tbody></table>
              </div>
              </div>
              </body>
              </html>"; 

             $mail = new PHPMailer();
             $mail->isSMTP();
             $mail->SMTPDebug = 0;
             $mail->CharSet = 'UTF-8';
             $mail->isHTML();
             $mail->Host = 'smtp.gmail.com';
             $mail->Port = 465;
             $mail->SMTPAuth = true;
             $mail->SMTPSecure = "ssl";  
             $mail->Username = "fac3.agendamento@gmail.com";
             $mail->Password = "infn@@1414";
             $mail->setFrom('fac3.agendamento@gmail.com', 'Saude - Agendamentos');
             $mail->addAddress($emailUserFor, $nome);
             $mail->AddCC($emailUserTo, $agendado_para["Nome"]);
             $mail->Subject = $assunto;
             $mail->Body = $mensagem;
             $mail->Send();
			  
            
             echo "<br/><div class=\"container text-center alert alert-success alert-dismissible fade show\" role=\"alert\">
          <h3>Sucesso!...</h3>
          <p>Enviamos um e-mail para o professor informando a reserva concluida com sucesso!</p>
          </div>
                ";
            }else{
                  //Seleciona o Nome do Coordenador  
        $query_coord=mysql_query("SELECT * FROM coordenadores WHERE id_cood='$cood'") or die(mysql_error());
        $array=mysql_fetch_array($query_coord);
        $emailUserCoord=$array["email"];

        //Seleciona o Nome da Disciplina
        $disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$disci'") or die(mysql_error());
        $nome_disciplina=mysql_fetch_assoc($disciplina);

        //Seleciona o Nome do curso
        $cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
        $nome_curso=mysql_fetch_assoc($cursoMq);

        //Usuário agendado
        $userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userProf'") or die(mysql_error());
        $agendado_para=mysql_fetch_assoc($userMq);
        $emailUserTo=$agendado_para["email"];
        $emailUserFor=$mail;


        switch ($periodo) {
            case 'M12':
            
                  $emailPerdiodo="Matutino - 1º e 2º Periodo - ".$tempo;
            break;
  
            case 'M1':
                  $emailPerdiodo="Matutino - 1º Periodo - ".$tempo;
            break;
  
            case 'M2':
                  $emailPerdiodo="Matutino - 2º Periodo - ".$tempo;
            break;
  
            case 'N12':
            
                  $emailPerdiodo="Noturno - 1º e 2º Periodo - ".$tempo;
            break;
  
            case 'N1':
                  $emailPerdiodo="Noturno - 1º Periodo - ".$tempo;
            break;
  
            case 'N2':
                  $emailPerdiodo="Noturno - 2º Periodo - ".$tempo;
            break;
  
        
        }
           
           
        $situation = "Aguardando Aprovação do Setor de Saude";
                  
                              $dataemail = $dataBD;
                  $dataDiv = explode('-', $dataemail);
                  $dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];
                              $email=$mail;
     $assunto="AgendaLAB - Saude";
               
     $mensagem="<html>            
                <head>
                <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                </head>
                <body>
                <h1>Sistema AgendaLAB - Leste Sul</h1>
                <div id='mensage' style='top:80px'>
                <div>
                <h4>".ucfirst($agendado_para["Nome"]).",<p> Um Laboratório foi reservado para você pelo ".$nome.".</p></h4>
                <h4><p> ".$nome." você realizou o agendamento com menos de 1 semana de antecedência e por isso foi BLOQUEADO, entrar em contato com o seu Coordenador (em cópia neste e-mail).</p></h4>
                <h4 style='color:#d93025'>ATENÇÃO - Em caso de agendamentos realizados fora do prazo de 1 semana, os materiais solicitado não são garantidos para a aula.</p><p> Em caso de duvidas, entre em contato com seu coordenador.</p></h4>
                <fieldset><legend>
                <h2>Informações</h2></legend><br/>
                <b>Agendado Para: </b>".$agendado_para["Nome"]." (em cópia).<br/>
                <b>Laboratório: </b>".$lab."<br/>
                <b>Data: </b>".$dataBR."<br/>
                <b>Periodo: </b>".$emailPerdiodo."<br/>
                <b>Curso: </b>".$nome_curso["curso"]."<br/>
                <b>Disciplina: </b>".$nome_disciplina["disciplina"]."<br/>
                <b>Tempo de aulas: </b>".$tempo."<br/>
                <b>Status: </b>".$situation."<br/>
                <b>Materiais: </b>".$soft."<br/>
                </fieldset>
                <h4 style='color:#003399'>ATENÇÃO - Caso necessite de materiais, acesse o portal <a href='http://www.tifac3.com.br/'> AgendaLAB </a> e preencha no campo observações. Fique atento ao prazo de 1 semana de antecedência.</h4>
                <p>_____________________________________</p>
                <h4 style='color:#666666'>Por favor não responda a esta mensagem. Este é um e-mail automático.</h4><br/>
                <table cellspacing='2' cellpadding='1'>
  <tbody><tr>
  
  <td style='border-right:thin #999 solid; width:=92px; height:=50px; padding-left:2px;'><img src='css/images/anhanguera_assinatura.jpg'></td>
  
  <td style='padding:10px 7px;'>
  
  <font style='color:#4d4d4f; font-family: Calibri; font-size:18px;'><b>AgendaLAB - Saúde</b></font><br><font style='color:#4d4d4f; font-family: Calibri; font-size:12px;text-transform:uppercase;'>Núcleo de Informática</font><br><font style='color:#4d4d4f; font-family: Calibri ; font-size:10px;'>(19) 3512-3109</font><br><font font=' style='color:#4d4d4f; font-family: Calibri ; font-size:10px;font-weight: bold;'>www.tifac3.com.br</font>
  
  </td>
  </tr>
  
  </tbody></table>
                </div>
                </div>
                </body>
                </html>"; 
  
               $mail = new PHPMailer();
               $mail->isSMTP();
               $mail->SMTPDebug = 0;
               $mail->CharSet = 'UTF-8';
               $mail->isHTML();
               $mail->Host = 'smtp.gmail.com';
               $mail->Port = 465;
               $mail->SMTPAuth = true;
               $mail->SMTPSecure = "ssl";  
               $mail->Username = "fac3.agendamento@gmail.com";
               $mail->Password = "infn@@1414";
               $mail->setFrom('fac3.agendamento@gmail.com', 'Saude - Agendamentos');
               $mail->addAddress($emailUserCoord, $array["Nome"] );
               $mail->AddCC($emailUserFor, $nome);
               $mail->AddCC($emailUserTo, $agendado_para["Nome"]);
               $mail->Subject = $assunto;
               $mail->Body = $mensagem;
               $mail->Send();
                      
              
               echo "<br/><div class=\"container text-center alert alert-warning alert-dismissible fade show\" role=\"alert\">
             <h3>Sucesso!..</h3>
             <p>Sua solicitação foi realizado com sucesso, mas você foi bloqueado por agendar com menos de 1 semana de Antecedência, procure seu coordenador.!</p>
             </div>
                 ";


              }
            }

            public function aprovarAgendamentoSaude(){
                  if(isset($_GET["number"])){
                           $id=$_GET["number"];
                           $query=mysql_query("UPDATE agendadata_saude SET situation='RESERVADO' WHERE ID='$id'")or die("Erro no query ". mysql_error());
                          $flash="<p><h4>AGENDAMENTO AUTORIZADO</h4></p>";
                             $select="SELECT * FROM agendadata_saude WHERE ID='$id'";
                             $execute=mysql_query($select)or die("ERRO".mysql_error());
                             $mfa=mysql_fetch_assoc($execute);
                           
            
                            $situation = "RESERVADO";
                            $dataemail = $mfa["data"];
                            $dataDiv = explode('-', $dataemail);
                            $dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];
                            $agendado_por=$mfa["agendado_por"];
                          
                      //Seleciona o Nome do Coordenador
                      $id_cood=$mfa["Coordenador"];
                      $query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$id_cood'");
                      $array=mysql_fetch_array($query_coord);
                      //Seleciona o Nome da Disciplina
                      $aula=$mfa["Aula"];
                      $disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$aula'") or die(mysql_error());
                      $nome_disciplina=mysql_fetch_assoc($disciplina);
                      //Seleciona o Nome do curso
                      $curso=$mfa["Disciplina"];
                      $cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
                      $nome_curso=mysql_fetch_assoc($cursoMq);
                      //Usuário agendado
                      $userRequest=$mfa["agendado_para"];
                      $userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userRequest'") or die(mysql_error());
                      $agendado_para=mysql_fetch_assoc($userMq);
                      $emailUserTo=$agendado_para["email"];
                      $emailUserFor=$mfa["email"];

                      switch ($mfa["periodo"]) {
                        case 'M12':
                        
                        $emailPerdiodo="Matutino - 1º e 2º Periodo - ".$mfa["aula_total"];
                        break;
              
                        case 'M1':
                              $emailPerdiodo="Matutino - 1º Periodo - ".$mfa["aula_total"];
                        break;
              
                        case 'M2':
                              $emailPerdiodo="Matutino - 2º Periodo - ".$mfa["aula_total"];
                        break;
              
                        case 'N12':
                        
                        $emailPerdiodo="Noturno - 1º e 2º Periodo - ".$mfa["aula_total"];
                        break;
              
                        case 'N1':
                              $emailPerdiodo="Noturno - 1º Periodo - ".$mfa["aula_total"];
                        break;
              
                        case 'N2':
                              $emailPerdiodo="Noturno - 2º Periodo - ".$mfa["aula_total"];
                        break;
              
                    
                    }
          
               $assunto="Agendamento APROVADO";
                         
               $mensagem="<html>            
                          <head>
                          <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                          </head>
                          <body>
                          <h1>AgendaLAB - Leste Sul</h1>
                          <div style='top:80px'>
                          <div>
                          <h4>".ucfirst($agendado_por).",<p>Seu agendamento foi APROVADO e está reservado com sucesso!</p></h4>
                          <fieldset><legend>
                          <h2>Informações</h2></legend><br/>
                          <b>Agendado para: </b>".$agendado_para["Nome"]." (Adicionado em Cópia)<br/>
                          <b>Laboratório: </b>".$mfa["Lab"]."<br/>
                          <b>Data: </b>".$dataBR."<br/>
                          <b>Periodo: </b>".$emailPerdiodo."<br/>
                          <b>Tempo de Aula: </b>".$mfa["aula_total"]."<br/>
                          <b>Curso: </b>".$nome_curso["curso"]."<br/>
                          <b>Disciplina: </b>".$nome_disciplina["disciplina"]."<br/>
                          <b>Status: </b>".$situation."<br/>
                          <b>Materiais: </b>".$mfa["Softwares"]."<br/>
                          </fieldset>
                          <h4 style='color:#003399'><b>ATENÇÃO</b> - Sempre realize seu agendamento ou cancelamento com no mínimo 1 semana de antecedência.</h4>
                          <p>_____________________________________</p>
                          <h4 style='color:#666666'>Por favor não responda a esta mensagem. Este é um e-mail automático.</h4><br/>
                          <img src='css/msgs/Assinatura.png'>
                          </div>
                          </div>
                          </body>
                          </html>"; 
            
            
                         $mail = new PHPMailer();
                         $mail->isSMTP();
                         $mail->SMTPDebug = 0;
                         $mail->CharSet = 'UTF-8';
                         $mail->isHTML();
                         $mail->Host = 'smtp.gmail.com';
                         $mail->Port = 465;
                         $mail->SMTPAuth = true;
                         $mail->SMTPSecure = "ssl";  
                         $mail->Username = "fac3.agendamento@gmail.com";
                         $mail->Password = "infn@@1414";
                         $mail->setFrom('fac3.agendamento@gmail.com', 'AgendaLAB - Saude');
                         $mail->addAddress($emailUserFor, $agendado_por);
                         $mail->AddCC($emailUserTo, $agendado_para["Nome"]);
                         $mail->Subject = $assunto;
                         $mail->Body = $mensagem;
                         $mail->Send();
                        
            
                }elseif(empty($flash)){
                  $flash="Erro de Aprovação";
            }
            
            echo "<div class=\"w3-panel w3-border-blue w3-round-xxlarge w3-blue w3-center w3-display-top\">";
            echo $flash;
            echo "</div>";
            
            }


      public function cancelarAgendamento(){
    if(isset($_GET["number"])){
             $id=$_GET["number"];
       $query=mysql_query("UPDATE agendadata_saude SET situation='CANCELADO' WHERE id='$id'")or die("Erro no query ". mysql_error());
       $flash="<p><h4>Agendamento ".$_GET["number"]." Cancelado!</h4></p>";
               $select="SELECT * FROM agendadata_saude WHERE id='$id'";
               $execute=mysql_query($select)or die("ERRO".mysql_error());
               $mfa=mysql_fetch_assoc($execute);
             


               $situation = "CANCELADO";
               $dataemail = $mfa["data"];
               $dataDiv = explode('-', $dataemail);
               $dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];
 
               $agendado_por=$mfa["agendado_por"];
               //Seleciona o Nome do Coordenador
               $id_cood=$mfa["Coordenador"];
               $query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$id_cood'");
               $array=mysql_fetch_array($query_coord);
               //Seleciona o Nome da Disciplina
               $aula=$mfa["Aula"];
               $disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$aula'") or die(mysql_error());
               $nome_disciplina=mysql_fetch_assoc($disciplina);
               //Seleciona o Nome do curso
               $curso=$mfa["Disciplina"];
               $cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
               $nome_curso=mysql_fetch_assoc($cursoMq);
               //Usuário agendado
               $userRequest=$mfa["agendado_para"];
               $userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userRequest'") or die(mysql_error());
               $agendado_para=mysql_fetch_assoc($userMq);
               $emailUserTo=$agendado_para["email"];
               $emailUserFor=$mfa["email"];

               switch ($mfa["periodo"]) {
                  case 'M12':
                  
                  $emailPerdiodo="Matutino - 1º e 2º Periodo - ".$mfa["aula_total"];
                  break;
        
                  case 'M1':
                        $emailPerdiodo="Matutino - 1º Periodo - ".$mfa["aula_total"];
                  break;
        
                  case 'M2':
                        $emailPerdiodo="Matutino - 2º Periodo - ".$mfa["aula_total"];
                  break;
        
                  case 'N12':
                  
                  $emailPerdiodo="Noturno - 1º e 2º Periodo - ".$mfa["aula_total"];
                  break;
        
                  case 'N1':
                        $emailPerdiodo="Noturno - 1º Periodo - ".$mfa["aula_total"];
                  break;
        
                  case 'N2':
                        $emailPerdiodo="Noturno - 2º Periodo - ".$mfa["aula_total"];
                  break;
        
              
              }

        
 $assunto="Cancelamento de Agendamento";
           
 $mensagem="<html>            
            <head>
            <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
            </head>
            <body>
            <h1>AgendaLAB - Leste Sul</h1>
            <div style='top:80px'>
            <div>
            <h4>".ucfirst($agendado_por).",<p>Seu agendamento foi cancelado com sucesso, caso não reconheça o cancelamento, contate o setor de Saude da unidade!</p></h4>
            <fieldset><legend>
            <h2>Informações</h2></legend><br/>
            <b>Agendado para: </b>".$agendado_para["Nome"]." (Adicionado em Cópia)<br/>
            <b>Laboratório: </b>".$mfa["Lab"]."<br/>
            <b>Data: </b>".$dataBR."<br/>
            <b>Periodo: </b>".$emailPerdiodo."<br/>
            <b>Tempo de Aula: </b>".$mfa["aula_total"]."<br/>
            <b>Curso: </b>".$nome_curso["curso"]."<br/>
            <b>Disciplina: </b>".$nome_disciplina["disciplina"]."<br/>
            <b>Status: </b>".$situation."<br/>
            <b>Observações: </b>".$mfa["Softwares"]."<br/>
            </fieldset>
            <h4 style='color:#003399'><b>ATENÇÃO</b> - Sempre realize seu agendamento ou cancelamento com no mínimo 1 semana de antecedência.</h4>
            <p>_____________________________________</p>
            <h4 style='color:#666666'>Por favor não responda a esta mensagem. Este é um e-mail automático.</h4><br/>
            <img src='http://tifac3/agendalab/css/msgs/Assinatura.png'>
            </div>
            </div>
            </body>
            </html>"; 


           $mail = new PHPMailer();
           $mail->isSMTP();
           $mail->SMTPDebug = 0;
           $mail->CharSet = 'UTF-8';
           $mail->isHTML();
           $mail->Host = 'smtp.gmail.com';
           $mail->Port = 465;
           $mail->SMTPAuth = true;
           $mail->SMTPSecure = "ssl";  
           $mail->Username = "fac3.agendamento@gmail.com";
           $mail->Password = "infn@@1414";
           $mail->setFrom('fac3.agendamento@gmail.com', 'AgendaLAB - Saúde');
           $mail->addAddress($emailUserFor, $agendado_por);
           $mail->AddCC($emailUserTo);
           $mail->Subject = $assunto;
           $mail->Body = $mensagem;
           $mail->Send();
          

  }elseif(empty($flash)){
    $flash="Erro de sistema!";
}
echo "<div class=\"w3-panel w3-border-blue w3-round-xxlarge w3-blue w3-center w3-display-top\">";
echo $flash;
echo "</div>";
}
  }

?>