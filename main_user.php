
<?php
date_default_timezone_set('America/Sao_Paulo');
include("includes/header_config.php");

$usuarioLogado=$_SESSION["email"];
$usuarioNome=$_SESSION["Nome"];
$dataToday = date("Y-m-d");
$dataLast = date( "Y-m-d", strtotime( $dataToday."+5 day" ) );

$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1; 
$sqlTotal="SELECT * FROM agendadata_saude WHERE email='$usuarioLogado' AND data >='$dataToday' AND chamada='' AND data NOT IN (SELECT data FROM agendadata_saude WHERE data BETWEEN '$dataToday' AND '$dataLast') ORDER BY data";
$qrTotal = mysql_query($sqlTotal) or die (mysql_error());
$numTotal= mysql_num_rows($qrTotal);
$qtn = 10;
$totalPagina= ceil($numTotal/$qtn);
$inicio = ($qtn * $pagina) - $qtn;

?>


<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/w3.css"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style type="text/css">
	body{
		background-color: white;
	}

</style>
</head>
<body>
	<br/>
	<div class="w3-container">
		<div class="w3-container">
			<h2>Bem-vindo, <?php echo $usuarioNome;?>.</h2>
			<p>Todos os seus agendamentos pendentes do laboratorio de saúde  estão sendo exibidos aqui..</p>
     
			
		</div>
<hr>
		<div class="container">
			<h4>
				Agendamentos da semana:
			</h4>
			<table class="w3-table w3-small w3-striped w3-hoverable w3-bordered w3-centered table-border">
				<tr class="w3-white">
					<th>Data</th>
					<th>Turno</th>
					<th>Curso</th>
					<th>Disciplina</th>
					<th>Observação</th>
				</tr>
				<?php
				$query_semana=mysql_query("SELECT * FROM agendadata_saude WHERE email='$usuarioLogado' AND data >='$dataToday' AND situation NOT IN ('CANCELADO') AND chamada='' AND data IN (SELECT data FROM agendadata_saude WHERE data BETWEEN '$dataToday' AND '$dataLast') ORDER BY data");

				if (mysql_num_rows($query_semana)  == 0 ) {
					echo "<div class=\"message\"> Sem agendamentos pendentes.. </div>";

				}else{
					while($row=mysql_fetch_array($query_semana)){
						$id_id=$row["ID"];

						switch ($row["periodo"]) {
							case 'M12':
							$pSelect="Matutino - 1º e 2º Turno";
							break;

							case 'M1':
							$pSelect="Matutino - 1º Turno";
							break;

							case 'M2':
							$pSelect="Matutino - 2º Turno";
							break;

							case 'N12':
							$pSelect="Noturno - 1º e 2º Turno";
							break;

							case 'N1':
							$pSelect="Noturno - 1º Turno";
							break;

							case 'N2':
							$pSelect="Noturno - 2º Turno";
							break;

							default:
							$pSelect="";
							break;

						}
						switch ($row["Softwares"]) {
							case "":
							$modal="ND";

							break;

							default:
							$modal="<img src=\"css/images/lupa_obs.png\" title=\"Observação: ".$row["Softwares"]."\" style=\"width: 20px;height: 20px;\">";

							break;

						}

						$dataemail = $row["data"];
						$dataDiv = explode('-', $dataemail);
						$dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];

						$aula=$row["Aula"];
						$disciplina=mysql_query("SELECT * FROM disciplina WHERE id_disc='$aula'") or die(mysql_error());
						$nome_disciplina=mysql_fetch_assoc($disciplina);
						?>

				<tr>
					<td><?php echo $dataBR; ?></td>
					<td><?php echo $pSelect; ?></td>
					<td><?php echo $row["Disciplina"]; ?></td>
					<td><?php echo $nome_disciplina["nome_disc"]; ?></td>
					<td><?php echo $modal; ?></td>
				</tr>
							<?php
					}
				} 
				?>
			</table>
			
		</div>
<hr>
		<div class="container">
			<!-- AQUI VAI A TABELA COM OS LABORATORIOS  E COM MENU DE FECHAR E ABRIR, DESATIVAR E ATIVAR. -->
			<h4>
				Agendamentos por vir:
			</h4>
			<table class="w3-table w3-small w3-striped w3-hoverable w3-bordered w3-centered table-border"> 
				<tr class="w3-white" >
					<th>Data</th>
					<th>Turno</th>
					<th>Curso</th>
					<th>Disciplina</th>
					<th>Observação</th>
					<th>Ação</th>
				</tr>
				<?php
				$query_select=mysql_query("SELECT * FROM agendadata_saude WHERE email='$usuarioLogado' AND chamada='' AND situation NOT IN ('CANCELADO') AND data >='$dataToday' AND data NOT IN (SELECT data FROM agendadata_saude WHERE data BETWEEN '$dataToday' AND '$dataLast') ORDER BY data LIMIT $inicio,$qtn");

				if (mysql_num_rows($query_select)  == 0 ) {
					echo "<div class=\"message\"> Sem agendamentos pendentes.. </div>";

				}else{
					while($row=mysql_fetch_array($query_select)){
						$id_id=$row["ID"];

						switch ($row["periodo"]) {
							case 'M12':
							$pSelect="Matutino - 1º e 2º Turno";
							break;

							case 'M1':
							$pSelect="Matutino - 1º Turno";
							break;

							case 'M2':
							$pSelect="Matutino - 2º Turno";
							break;

							case 'N12':
							$pSelect="Noturno - 1º e 2º Turno";
							break;

							case 'N1':
							$pSelect="Noturno - 1º Turno";
							break;

							case 'N2':
							$pSelect="Noturno - 2º Turno";
							break;

							default:
							$pSelect="";
							break;

						}
						switch ($row["Softwares"]) {
							case "":
							$modal="ND";

							break;

							default:
							$modal="<img src=\"css/images/lupa_obs.png\" title=\"Observação: ".$row["Softwares"]."\" style=\"width: 20px;height: 20px;\">";

							break;

						}

						$dataemail = $row["data"];
						$dataDiv = explode('-', $dataemail);
						$dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];

						$aula=$row["Aula"];
						$disciplina=mysql_query("SELECT * FROM disciplina WHERE id_disc='$aula'") or die(mysql_error());
						$nome_disciplina=mysql_fetch_assoc($disciplina);
						?>

						<tr>
							<td><?php echo $dataBR; ?></td>
							<td><?php echo $pSelect; ?></td>
							<td><?php echo $row["Disciplina"]; ?></td>
							<td><?php echo $nome_disciplina["nome_disc"]; ?></td>
							<td><?php echo $modal; ?></td>
							<td class="buttons">
								<a href="edit_agendamento_saude.php?numberid=<?php echo $id_id; ?>" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span></a>
							</td>
						</tr>

						<?php
					}
				} 
				?>

			</table>
		</div>
		<br/>
		<div class="w3-center">
			<div class="w3-bar w3-round w3-border" >
				<?php
                        //Apresentar a paginação
				for($i = 1; $i <= $totalPagina; $i++){ 
					if($i == $pagina){ ?>
						<a class="w3-button w3-bar-item w3-blue"><b><?php echo $i; ?></b></a>
					<?php }else{ ?>
						<a href="?pagina=<?php echo $i;?>" class="w3-button w3-bar-item" ><?php echo $i; ?></a>
					<?php }     
				}  

				if(empty($msg)){
					$display="display:none;";
				}else{
					$display="display:block;";
				}


				?>
			</div>

		</div>
		<hr>

	</div>




<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>