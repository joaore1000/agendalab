<?php
      $titlepag="FAC3 - Editar Laboratório";
      include("includes/header_config_eng.php");
      header('Content-Type: text/html; charset=utf-8');

  if (isset($_GET["numberid"])) {
  	  $id_lab=$_GET["numberid"];
  	  $query_lab=mysql_query("SELECT * FROM lab_eng WHERE id_eng='$id_lab'");
  	  $rowq=mysql_fetch_array($query_lab);
  	  if ($rowq[2]=="ATIVO") {
  	  	$check="checked";
  	  	$check_d=null;
  	  }elseif($rowq[2]=="INATIVO"){
  	  	$check_d="checked";
  	  	$check=null;
  	  }else{
  	  	$check_d=null;
  	  	$check=null;
  	  }

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php echo $titlepag ?></title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<style type="text/css">
	body{
		background-color: white;
	}
</style>
</head>
<body>
  <div class="w3-card-4 w3-display-middle" style="width: 40%">
    	<header class="w3-container w3-blue w3-center">
    		<h2>Editar Laboratório Engenharia</h2>
    	</header>
	 <div class="message" style="<?php echo $display;?>"><?php echo $msg;?></div>
	<div id="disable" class="w3-container w3-center">
          <form action="?acao=editar" method="post">
          	<input type="text" hidden name="ids" value="<?php echo $id_lab?>"/>
          	<br/>
             <label for="nome_lab" class="w3-left-align"><p><b>Nome do Laboratório:</b></p></label><input autofocus value="<?php echo $rowq[1]; ?>" id="nome_lab" type="text" name="Nomelab" class="w3-input"/>
             <br/>
		     <div class="w3-left-align">
		     <p><b>Status</b></p>
		     <input type="radio" <?php echo $check;?> id="enable" name="status" class="w3-radio" value="ATIVO"/><label for="status"> Ativado</label>
		     <input type="radio" <?php echo $check_d;?> id="disable"  name="status" class="w3-radio" value="INATIVO"/><label for="status"> Desativado</label><br>
		     </div>
		     <br/>
		     <input type="submit" class="w3-btn w3-blue w3-margin w3-right" value="Editar"/>
		     <a href="config_lab_eng.php" class="w3-btn w3-red w3-margin w3-left" >Voltar</a>
		  </form>
        </div>
	 <!--login-->
	</div>
</body>
</html>


<?php 
}else{
  	 ?>
			   <script language="JavaScript">
                  window.location="config_lab_eng.php";
               </script>
   <?php 
  }

  ?>