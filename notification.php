<?php
include("includes/headercancel.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Painel de Inicio.</title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<style>

#Agpendente {
    left: 0px;
    width: 103%;
  
}

#bloqueado {
    left: 63px;
    width: 92%;
   
}

#pendente {
    left: 129px;
    width: 85%;
    
}
div.gallery {
    margin: 5px;
    border: 1px solid #ccc;
    float: left;
    width: 128px;
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
}
div.gallery:hover {
    border: 1px solid #777;
}

div.gallery img {
    width: 100%;
    height: auto;
}
div.desc {
    padding: 15px;
    text-align: center;
    background-color: red;
    color: black;
    font-size: 30px;
    border-top-left-radius: 90px;
    border-top-right-radius: 90px;
}
</style>

</head>
<?php
    //Notifica Usuarios Bloqueados  
    $checkBloqueado=mysql_query("SELECT * FROM usuarios WHERE ESTATO='BLOQUEADO'");
  $bloqueados=mysql_num_rows($checkBloqueado);
    //Notifica agendamentos pendentes
    $checkAgendamentos=mysql_query("SELECT * FROM agendadata WHERE situation='AGUARDANDO'");
  $pendentes=mysql_num_rows($checkAgendamentos);
    //Notifica novos usuarios pendentes
    $checkUsuario=mysql_query("SELECT * FROM usuarios WHERE ESTATO='INATIVO'");
  $novosPendentes=mysql_num_rows($checkUsuario);
 
$contador = 0;

$labo=mysql_query("SELECT * FROM laboratorios WHERE Laboratorio='Laboratório 05' || Laboratorio='Laboratório 06'");
$verificar=mysql_num_rows($labo);
while ($row = mysql_fetch_array($labo, MYSQL_NUM)) {
          $chamada[$contador][0]  = $row[0];
          $chamada[$contador][1]  = $row[1];
          $chamada[$contador][6]  = $row[6];
          
          $contador++;
          $_SESSION["labo"]=$chamada;
          
}
?>

<body>
    <h1 class="w3-center w3-margin w3-animate-zoom">Painel Principal</h1>
    
  <div id="notificacaoTemp" class="w3-container w3-center w3-display-middle w3-card-4 w3-pale-blue w3-border-blue w3-rightbar w3-leftbar w3-half">
  <div class="w3-third w3-panel w3-border w3-row-padding w3-round w3-hover-indigo w3-animate-zoom">
      <a target="iframe_inicial" href="autorizar_agenda_data.php">
       <img id="Agpendente" src="css/images/AG_pendente.png" title="Agendamentos Pendentes">
        <div class="desc"><?php echo $pendentes;?></div></a>
       </div>

      <div class="w3-third w3-panel w3-border w3-round w3-row-padding w3-hover-indigo w3-animate-zoom" >
       <a target="iframe_inicial" href="blockerusers.php">
       <img id="bloqueado" src="css/images/bloqueado.png" title="Usuarios Bloqueados">
       <div class="desc"><?php echo $bloqueados;?></div></a>
       </div>
       <div class="w3-third w3-panel w3-border w3-round w3-row-padding w3-hover-indigo w3-animate-zoom">
       <a target="iframe_inicial" href="newusers.php">
       <img id="pendente" src="css/images/pendente.png" title="Novos usuarios Pendentes">
        <div class="desc"><?php echo $novosPendentes;?></div></a>
       </div>
   </div>


<?php    if ($verificar !== 0){


?>
   <div class="w3-container w3-display-left">
     <table class="w3-table-all w3-small w3-border w3-white" style="width: 23%">
    <tr>
      <th class="w3-center">Laboratório</th>
      <th class="w3-center">Ação</th>
    </tr>
<?php  foreach( $_SESSION["labo"] as &$linha) {
  $number=$linha[0];
?>
    <tr>
      <td class="w3-center"><?php echo $linha[1]?> </td>
      <td>
        <?php if($linha[6]=="ABERTO"){ ?>
        <a href="?acao=fechar&amp;number=<?php echo $number;?>" class="w3-btn w3-green">FECHAR</a>
        <?php } else {?>
        <a href="?acao=abrir&amp;number=<?php echo $number;?>" class="w3-btn w3-red">ABRIR</a>
        <?php } ?>
      </td>
    </tr>
    <?php } ?>
     </table>
     
   </div>

   <?php } ?>
</body>
</html>