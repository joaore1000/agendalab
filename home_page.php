<?php 
date_default_timezone_set('America/Sao_Paulo');
// Pegando paginação 
$pInf = (isset($_GET['paginainf']))? $_GET['paginainf'] : 1;
$pEng = (isset($_GET['paginaeng']))? $_GET['paginaeng'] : 1; 
$pSau = (isset($_GET['paginasau']))? $_GET['paginasau'] : 1; 
// $p = (isset($_GET['pagina']))? $_GET['pagina'] : 1; 
//Includes
include("includes/headerlog.php");
include("classes/paginacao.class.php"); 
//Globais
$nome=$_SESSION["Nome"];
$usuarioLogado=$_SESSION["email"];
$idLogado=$_SESSION["id"];
// Tratamento da Data
$dataToday = date("Y-m-d");
$dataLast = date( "Y-m-d", strtotime( $dataToday."+5 day" ) );

?>

<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Css Bootstrap -->
        <!-- Font Ubunto 300,400,500,700 -->
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,700,700i&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- JS Bootstrap -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
        <!-- Forçando Font ubunto -->
        <style>
        body{
            font-family: 'Ubuntu', sans-serif;
        }
        hr {
            color: #cce5ff;
            background-color: #cce5ff;
            
        }
        </style>
    </head>

<body class="">
    <div class="container-fluid">
<?php

if (isset($nivel)){

    switch($nivel){

        case 8: //Admin TI
            include("Templates/admin_eng.php");

        break;

        case 7: //Admin TI
            include("Templates/admin_eng.php");

        break;

        case 6: //Admin Eng
            include("Templates/admin_eng.php");

        break;

        case 5: //Admin Saude
            include("Templates/home_professor.php");

        break;

        case 4: //Diretor
            include("Templates/home_professor.php");
        
        break;

        case 3: //Coordenador Academico
            include("Templates/home_professor.php");

        break;

        case 2: //Coordenador Curso
            include("Templates/home_professor.php");

        break;

        case 1: // Professor
            include("Templates/home_professor.php");

        break;

        default: // Caso não Exista nivel predefinido.
            include("Templates/home_professor.php");
        
        break;
    }
}else{
    echo "Não existe a variavel Nivel";
}


?>
</div>

</body>
</html>