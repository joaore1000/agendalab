<?php
date_default_timezone_set('America/Sao_Paulo');
include("includes/header_config_saude.php");

$usuarioLogado=$_SESSION["email"];
$usuarioNome=$_SESSION["Nome"];
$dataToday = date("Y-m-d");

?>


<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/w3.css" />

	<style type="text/css">
		body {
			background-color: white;
		}
	</style>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	 crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	 crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	 crossorigin="anonymous"></script>

<script type="text/javascript">
  function sumir()
  {
   disable.style.display='none';
   fountainG.style.display='block';
   document.uploadFoto.submit;

 }
</script>
<script type="text/javascript">
	
	function charge()
	{
	  
	  fountainG.style.display='block';
	  buttons.style.display='none';
	  buttonsN.style.display='none';
	  
	}
	 
 </script>
</head>

<body>
	<br/>
	<div class="w3-container">
		<div class="w3-container">
			
			<h2 class="text-center"><?php echo $semana?> - <?php echo $Dmostra?></h2>
		</div>
		<hr>
		<div class="w3-container-fluid w3-responsive">
			<h2>
				Diurno:
			</h2>
			<table class="w3-table-all w3-hoverable w3-card-2">
				<thead>
					<tr class="w3-black">
						<th>Agendado por</th>
						<th>Agendado para</th>
						<th>Laboratório</th>
						<th>Turno</th>
						<th>Curso</th>
						<th>Disciplina</th>
						<th>Observação</th>
						<th>Ação</th>
					</tr>
				</thead>
				<?php
				$query_semana=mysql_query("SELECT * FROM agendadata_saude WHERE data ='$dataToday' AND situation='RESERVADO' AND periodo IN ('M12','M1','M2') ORDER BY Lab");

				if (mysql_num_rows($query_semana)  == 0 ) {
					echo "<div class=\"message\"> Sem agendamentos para esse Turno.. </div>";

				}else{
					while($row=mysql_fetch_array($query_semana)){
						$id_id=$row["ID"];

						switch ($row["periodo"]) {
							case 'M12':
							$pSelect="Matutino - 1º e 2º Turno";
							break;

							case 'M1':
							$pSelect="Matutino - 1º Turno";
							break;

							case 'M2':
							$pSelect="Matutino - 2º Turno";
							break;

							case 'N12':
							$pSelect="Noturno - 1º e 2º Turno";
							break;

							case 'N1':
							$pSelect="Noturno - 1º Turno";
							break;

							case 'N2':
							$pSelect="Noturno - 2º Turno";
							break;

							default:
							$pSelect="";
							break;

						}
						switch ($row["Softwares"]) {
							case "":
							$modal="ND";

							break;

							default:
							$modal="<img src=\"css/images/lupa_obs.png\" title=\"Observação: ".$row["Softwares"]."\" style=\"width: 20px;height: 20px;\">";

							break;

						}

						$dataemail = $row["data"];
						$dataDiv = explode('-', $dataemail);
						$dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];

						//Seleciona o Nome do Coordenador
						$id_cood=$row["Coordenador"];
						$query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$id_cood'");
						$array=mysql_fetch_array($query_coord);
						//Seleciona o Nome da Disciplina
						$aula=$row["Aula"];
						$disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$aula'") or die(mysql_error());
						$nome_disciplina=mysql_fetch_assoc($disciplina);
						//Seleciona o Nome do curso
						$curso=$row["Disciplina"];
						$cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
						$nome_curso=mysql_fetch_assoc($cursoMq);
						//Usuário agendado
						$userRequest=$row["agendado_para"];
						$userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userRequest'") or die(mysql_error());
						$agendado_para=mysql_fetch_assoc($userMq);
						?>
			<tbody>
				<tr>
					<td>
						<?php echo $row["agendado_por"]; ?>
					</td>
					<td>
						<?php echo $agendado_para["Nome"]; ?>
					</td>
					<td>
						<?php echo $row["Lab"]; ?>
					</td>
					<td>
						<?php echo $pSelect;?>
					</td>
					<td>
						<?php echo $nome_curso["curso"]; ?>
					</td>
					<td>
						<?php echo $nome_disciplina["disciplina"]; ?>
					</td>
					<td>
						<?php echo $modal; ?>
                        <a href="edit_comentario_chamada_saude.php?numberid=<?php echo $id_id; ?>" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span></a>
					
					</td>
					<td>
					<?php if($row["chamada"]==''){ ?>
								<div id="buttons" style="display: block;">
									<a href="?number=<?php echo $id_id ?>&acao=presente"><img src="css/images/teste2.png" style="width: 32px;height: 32px;"></a>
									<a href="?number=<?php echo $id_id ?>&acao=faltou" onclick="charge()"><img src="css/images/chave_red.png" style="width: 32px;height: 32px;"></a>
		             			</div>
					   <?php }elseif($row["chamada"]=="faltou"){ ?>
						<img src="css/images/croix-rouge.png" style="width: 34px;height: 34px;">
		               <?php }else{
						   		if(!empty($row["upload1_foto"]) && !empty($row["upload2_foto"])){ ?>
									<img src="css/images/confirma_chave.png" style="width: 32px;height: 32px;">
									 <?php  }elseif(empty($row["upload1_foto"])){ ?>
									<a href="#" class="w3-hover-text-blue" onclick="uploadAction(<?php echo $id_id ?>)"><img src="css/images/upload-photo-icon.png" style=""></a>
									 <?php }else{ ?>
									<a href="#" class="w3-hover-text-blue" onclick="uploadAction(<?php echo $id_id ?>)"><img src="css/images/upload-photo-icon.png" style=""> - Envie mais 1 foto.</a>
		                

		               <?php } } ?>
					</td>
				</tr>
			</tbody>
				<?php
					}
				} 
				?>
			</table>

		</div>
		<hr>
		<div id="fountainG" style="display: none;">
			<div id="fountainG_1" class="fountainG"></div>
			<div id="fountainG_2" class="fountainG"></div>
			<div id="fountainG_3" class="fountainG"></div>
			<div id="fountainG_4" class="fountainG"></div>
			<div id="fountainG_5" class="fountainG"></div>
			<div id="fountainG_6" class="fountainG"></div>
			<div id="fountainG_7" class="fountainG"></div>
			<div id="fountainG_8" class="fountainG"></div>
		</div>
		<div class="w3-container-fluid w3-responsive">
			<!-- AQUI VAI A TABELA COM OS LABORATORIOS  E COM MENU DE FECHAR E ABRIR, DESATIVAR E ATIVAR. -->
			<h2>
				Noturno:
			</h2>
			<table class="w3-table-all w3-hoverable w3-card-2">
			<thead>
				<tr class="w3-black">
					<th>Agendado por</th>
					<th>Agendado para</th>
					<th>Laboratório</th>
					<th>Turno</th>
					<th>Curso</th>
					<th>Disciplina</th>
					<th>Observação</th>
					<th>Ação</th>
				</tr>
			</thead>
				<?php
				$query_select=mysql_query("SELECT * FROM agendadata_saude WHERE data ='$dataToday' AND situation='RESERVADO' AND periodo IN ('N12','N1','N2') ORDER BY Lab");

				if (mysql_num_rows($query_select)  == 0 ) {
					echo "<div class=\"message\"> Sem agendamentos para esse Turno.. </div>";

				}else{
					while($row=mysql_fetch_array($query_select)){
						$id_id=$row["ID"];

						switch ($row["periodo"]) {
							case 'M12':
							$pSelect="Matutino - 1º e 2º Turno";
							break;

							case 'M1':
							$pSelect="Matutino - 1º Turno";
							break;

							case 'M2':
							$pSelect="Matutino - 2º Turno";
							break;

							case 'N12':
							$pSelect="Noturno - 1º e 2º Turno";
							break;

							case 'N1':
							$pSelect="Noturno - 1º Turno";
							break;

							case 'N2':
							$pSelect="Noturno - 2º Turno";
							break;

							default:
							$pSelect="";
							break;

						}
						switch ($row["Softwares"]) {
							case "":
							$modal="ND";

							break;

							default:
							$modal="<img src=\"css/images/lupa_obs.png\" title=\"Observação: ".$row["Softwares"]."\" style=\"width: 20px;height: 20px;\">";

							break;

						}

						$dataemail = $row["data"];
						$dataDiv = explode('-', $dataemail);
						$dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];

						//Seleciona o Nome do Coordenador
						$id_cood=$row["Coordenador"];
						$query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$id_cood'");
						$array=mysql_fetch_array($query_coord);
						//Seleciona o Nome da Disciplina
						$aula=$row["Aula"];
						$disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$aula'") or die(mysql_error());
						$nome_disciplina=mysql_fetch_assoc($disciplina);
						//Seleciona o Nome do curso
						$curso=$row["Disciplina"];
						$cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
						$nome_curso=mysql_fetch_assoc($cursoMq);
						//Usuário agendado
						$userRequest=$row["agendado_para"];
						$userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userRequest'") or die(mysql_error());
						$agendado_para=mysql_fetch_assoc($userMq);
						?>
			<tbody>			
				<tr>
				<td>
						<?php echo $row["agendado_por"]; ?>
					</td>
					<td>
						<?php echo $agendado_para["Nome"]; ?>
					</td>
					<td>
						<?php echo $row["Lab"]; ?>
					</td>
					<td>
						<?php echo $pSelect;?>
					</td>
					<td>
						<?php echo $nome_curso["curso"]; ?>
					</td>
					<td>
						<?php echo $nome_disciplina["disciplina"]; ?>
					</td>
					<td>
						<?php echo $modal; ?>
						<a href="edit_comentario_chamada_saude.php?numberid=<?php echo $id_id; ?>" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span></a>
					</td>
					<td>
					<?php if($row["chamada"]==''){ ?>
								<div id="buttons" style="display: block;">
									<a href="?number=<?php echo $id_id ?>&acao=presente"><img src="css/images/teste2.png" style="width: 32px;height: 32px;"></a>
									<a href="?number=<?php echo $id_id ?>&acao=faltou" onclick="charge()"><img src="css/images/chave_red.png" style="width: 32px;height: 32px;"></a>
		             			</div>
					   <?php }elseif($row["chamada"]=="faltou"){ ?>
						<img src="css/images/croix-rouge.png" style="width: 34px;height: 34px;">
		               <?php }else{
						   		if(!empty($row["upload1_foto"]) && !empty($row["upload2_foto"])){ ?>
									<img src="css/images/confirma_chave.png" style="width: 32px;height: 32px;">
									 <?php  }elseif(empty($row["upload1_foto"])){ ?>
									<a href="#" class="w3-hover-text-blue" onclick="uploadAction(<?php echo $id_id ?>)"><img src="css/images/upload-photo-icon.png" style=""></a>
									 <?php }else{ ?>
									<a href="#" class="w3-hover-text-blue" onclick="uploadAction(<?php echo $id_id ?>)"><img src="css/images/upload-photo-icon.png" style=""> - Envie mais 1 foto.</a>
		                

		               <?php } } ?>
					</td>
				</tr>
			</tbody>			
				<?php
					}
				} 
				?>

			</table>
		</div>
		<br/>
<?php	
	if(empty($msg)){
		$display="display:none;";
	}else{
		$display="display:block;";
	}
			
?>
		
		<hr>

	</div>

<script>
function uploadAction(id){
	document.getElementById('upload').style.display='block';
	document.getElementById('uploadFoto').action="chamada_saude.php?&acao=upload&number="+id;
	
 }

</script>

<!--- Modal de Upload de imagem -->
<div id="upload" class="w3-modal">
  	<div class="w3-modal-content w3-animate-zoom" style="width: 30%">
		<div class="w3-container w3-Indigo w3-center">
			<h1 style="font-family: 'Ubuntu',sans-serif;">Enviar Foto da Aula</h1>
			<span onclick="document.getElementById('upload').style.display='none'" 
			class="w3-button w3-display-topright w3-red">&times;</span>
		</div>
  		<div class="w3-container w3-center w3-card-4">
  			
  	 			<form id="uploadFoto" method="post" enctype="multipart/form-data">
				   	<h4>Selecione a Foto:</h4>
    				<input class="w3-input" type="file" name="fileToUpload" id="fileToUpload">
					<br/>
     				<div id="disable">
       					<input type="submit" value="Upload" onclick="sumir()" class="w3-btn w3-center w3-indigo"/>
     				</div>
  				</form>
				  <div id="fountainG" style="display: none;">
					<div id="fountainG_1" class="fountainG"></div>
					<div id="fountainG_2" class="fountainG"></div>
					<div id="fountainG_3" class="fountainG"></div>
					<div id="fountainG_4" class="fountainG"></div>
					<div id="fountainG_5" class="fountainG"></div>
					<div id="fountainG_6" class="fountainG"></div>
					<div id="fountainG_7" class="fountainG"></div>
					<div id="fountainG_8" class="fountainG"></div>
				</div>
  			<br/>
		</div>
	</div>

</body>

</html>
