<?php
include("includes/header_config_saude.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/w3.css"/>
</head>
<?php if (isset($_GET["numberid"])) {
  $id=$_GET["numberid"];

  $agendamento=mysql_query("SELECT * FROM agendadata_saude WHERE ID='$id'") or die(mysql_error());
  $servico=mysql_fetch_array($agendamento);
}
  ?>

  <body>
   <div id="seldata" class="w3-container w3-animate-zoom w3-pale-green w3-topbar w3-bottombar w3-border-green w3-rightbar w3-leftbar w3-center" style="display: block; width: 40%; margin-left: 30%; margin-top: 09%">
     <div class="w3-left-align">
       <a href="chamada_saude.php" class="w3-btn w3-card-4 w3-blue w3-left">Voltar</a>
     </div>
     <br/>
     <div class="w3-panel w3-pale-red w3-leftbar w3-rightbar w3-border-red" style="<?php echo $display;?>"><?php echo $msg;?></div>
     <div><h2>Agendamento <?php echo $id; ?></h2></div><br>
     <form action="?acao=editando_saude" method="post"> 
        <input type="hidden" name="reffer" id="reffer" value="chamada"/>
       <input type="hidden" name="cId" id="tId" value="<?php echo $id ?>"/>
       <label><b>Insira alguma observação para este agendamento:</b></label>
       <textarea name="comentario" autofocus id="comentario" rows="5" style="font-size: 16px" cols="35" class="w3-input w3-border" maxlength="1000" placeholder="Insira alguma observação.."><?php echo $servico['Softwares'];?></textarea><small class="caracteres"></small><br>
       <input type="submit" class="w3-btn w3-green w3-margin" value="Adicionar Observação" />
     </form> 
   </div>   


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

<script type="text/javascript">
$(document).on("input", "#comentario", function() {
        var limite = 1000;
        var informativo = "caracteres restantes.";
        var caracteresDigitados = $(this).val().length;
        var caracteresRestantes = limite - caracteresDigitados;

        if (caracteresRestantes <= 0) {
            var comentario = $("textarea[name=comentario]").val();
            $("textarea[name=comentario]").val(comentario.substr(0, limite));
            $(".caracteres").text("0 " + informativo);
        } else {
            $(".caracteres").text(caracteresRestantes + " " + informativo);
        }
    });
  </script>
 </body>
</html>