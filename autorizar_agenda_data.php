<?php
include("includes/validacaouser.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Autorizar Agendamento</title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<script type="text/javascript">
	function Aproved()
   {
	buttonsAction.style.display='none';
    fountainG.style.display='block';
     
   }
	
</script>
</head>
<body>


    <h1 class="w3-center w3-margin" style="margin-top:7%!important" >Agendamentos pendentes para aprovação</h1>
	 <div id="tValidar" class="w3-container w3-border w3-round-xlarge w3-center" style="top:30px;">
<div id="fountainG" style="display: none;">
	<div id="fountainG_1" class="fountainG"></div>
	<div id="fountainG_2" class="fountainG"></div>
	<div id="fountainG_3" class="fountainG"></div>
	<div id="fountainG_4" class="fountainG"></div>
	<div id="fountainG_5" class="fountainG"></div>
	<div id="fountainG_6" class="fountainG"></div>
	<div id="fountainG_7" class="fountainG"></div>
	<div id="fountainG_8" class="fountainG"></div>
</div>
	    <table class="w3-table-all w3-hoverable w3-tiny w3-card-4">
	    
	<tr class="w3-small w3-light-gray">
		<th style="width: 5%;text-align: center;">ID</th>
	    <th style="width: 10%;text-align: center;">Data</th>
		<th style="width: 10%;text-align: center;">Agendado Por</th>
		<th style="width: 10%;text-align: center;">Agendado Para</th>
		<th style="width: 10%;text-align: center;">Turno</th>
		<th style="width: 10%;text-align: center;">Coordenador</th>
		<th style="text-align: center;">Curso</th>
		<th style="text-align: center;">Disciplina</th>
		<th style="text-align: center;">Alunos</th>
		<th style="width: 10%;text-align: center;">Laboratório</th>
		<th style="text-align: center;">Observação</th>
		<th style="width: 10%;text-align: center;">Ação</th>
		
	</tr>
	    <?php 
	$reservados=mysql_query("SELECT * FROM agendadata WHERE situation='AGUARDANDO' ORDER BY data");
if(mysql_num_rows($reservados)==0){
	$msg="Nenhum Agendamento Pendente!";
	 ?>
	<div class="w3-container w3-pale-red w3-display-container"><?php echo $msg ;?></div>
	<?php }else{
	while($linha=mysql_fetch_array($reservados)){ 
		$id=$linha["ID"];
		$data = $linha["data"];
		$dataDiv = explode('-',$data);
		$dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];
		//Seleciona o Nome do Coordenador
		$id_cood=$linha["Coordenador"];
		$query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$id_cood'");
		$array=mysql_fetch_array($query_coord);
		//Seleciona o Nome da Disciplina
        $aula=$linha["Aula"];
		$disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$aula'") or die(mysql_error());
		$nome_disciplina=mysql_fetch_assoc($disciplina);
		//Seleciona o Nome do curso
		$curso=$linha["Disciplina"];
		$cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
		$nome_curso=mysql_fetch_assoc($cursoMq);
		//Usuário agendado
		$userRequest=$linha["agendado_para"];
		$userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userRequest'") or die(mysql_error());
		$agendado_para=mysql_fetch_assoc($userMq);
		?>
	<tr>
	<td style="text-align: center;"> <?php echo $id;?> </td>
	    <td style="text-align: center;"> <?php echo $dataBR;?> </td>
		<td style="text-align: center;"> <?php echo $linha["agendado_por"];?></td>
		<td style="text-align: center;"> <?php echo $agendado_para["Nome"];?></td>
		<td style="text-align: center;"> <?php echo $linha["periodo"];?></td>
		<td style="text-align: center;"> <?php echo $array["Nome"];?> </td>
		<td style="text-align: center;"> <?php echo $nome_curso["curso"];?> </td>
		<td style="text-align: center;"> <?php echo $nome_disciplina["disciplina"];?> </td>
		<td style="text-align: center;"> <?php echo $linha["Quantidade"];?> </td>
		<td style="text-align: center;"> <?php echo $linha["Lab"];?> </td>
		<td style="text-align: center;"> <?php echo $linha["Softwares"];?> </td>
		<td style="text-align: center;"> 
			<div id="fountainG" style="display: none;">
				<div id="fountainG_1" class="fountainG"></div>
				<div id="fountainG_2" class="fountainG"></div>
				<div id="fountainG_3" class="fountainG"></div>
				<div id="fountainG_4" class="fountainG"></div>
				<div id="fountainG_5" class="fountainG"></div>
				<div id="fountainG_6" class="fountainG"></div>
				<div id="fountainG_7" class="fountainG"></div>
				<div id="fountainG_8" class="fountainG"></div>
			</div>
			<div id="buttonsAction" style="display: block;">
				<?php echo "<a href=\"?acao=autorizar&amp;number=$id\" onClick=\"Aproved()\" ><img src=\"css/images/confirma_chave.png\" style=\"width:32px;height:32px;\" /></a>"; ?> 
				<?php echo "<a href=\"alterar_agenda.php?acao=alterar&amp;numberid=$id\"><img src=\"css/images/editar.png\" style=\"width:32px;height:32px;\" /></a>"; ?></td> 
			</div>
	</tr>
	<?php  } } ?>
        </table>
        
	 </div>
</body>
</html>
<?php if(empty($msg)){
	$display="display:none;";
}else{
	$display="display:block;";
}
?>
