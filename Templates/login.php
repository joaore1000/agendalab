<?php  
$titlepag="AgendaLAB - FAC 3";
header('Content-Type: text/html; charset=utf-8');

?>
<!DOCTYPE html>
<html>
<head>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="css/images/favicon2.png" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title><?php echo $titlepag ?></title>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/w3.css"/>
  <style type="text/css">

  body{
    background-image: url("css/images/fac3_op.jpg");
    background-size: 100% 100% ;
  }
</style>
<script type="text/javascript">
  function sumir()
  {
   disable.style.display='none';
   fountainG.style.display='block';

 }

</script>
</head>
<body>

  <div id="cadastrar" class="w3-btn w3-blue w3-display-bottomright"><a href="#" title="Versão do site!">Versão 2.019.10</a></div>
  <div class="w3-container w3-third w3-card-4 w3-border-blue w3-topbar w3-bottombar w3-display-middle w3-white" >	 
    <div class="w3-container w3-center w3-animate-zoom">
        <a href="index.php"><img src="css/images/logo_agendalab.png" alt="<?php echo $title;?>" title="<?php echo $title;?>" width="70%" height="30%"/></a>
    </div>
    <div class="w3-container w3-pale-red w3-display-container" style="<?php echo $display;?>">
     <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-tiny w3-display-topright">&times;
     </span>
     <p class="w3-large"><?php echo $msg;?></p>
   </div>
   <?php  if (isset($gravaUnidades) AND !empty($gravaUnidades)){ ?>
   <form action="?acao=logar" method="post" class="w3-animate-zoom" >
   <p>
      <label for="unidadeLs"> <h5>Unidade</h5></label>
      <select required id="unidadeLs" name="unidadeLs" class="w3-select w3-left">
        <option disabled selected>Selecione a Unidade..</option>
        <?php foreach($gravaUnidades as &$dadoUnidades) { ?>
        <option value="<?php echo $dadoUnidades[4]?>"><?php echo $dadoUnidades[0]?> - <?php echo $dadoUnidades[3] ?></option>
        <?php } ?>
      </select>
   </p>

   <p>
     <label for="Logon"><h5>Login:</h5></label>
     <input required id="Logon" type="text" class="w3-input" name="Logon" placeholder="Digite o login aqui.." />
   </p>
   <p>
     <label for="Senha"><h5>Senha:</h5></label>
     <input required id="Senha" type="password" class="w3-input" name="Senha" placeholder="Digite a senha aqui.." />
   </p>
   <div class="w3-center w3-margin w3-large">
    <input type="submit" class="w3-btn w3-blue" value="Entrar"/>
  </div>
</form>
<?php } ?>
<div id="reset-senha" class="w3-container w3-right-align">
 <p><a href="#" class="w3-hover-text-blue" onclick="document.getElementById('reset').style.display='block'">Esqueci meu Login/Senha</a></p>
 <br/>
</div>

</div>	
<div id="reset" class="w3-modal">
  <div class="w3-modal-content w3-animate-zoom" style="width: 30%">
   <div class="w3-container w3-Indigo w3-center">
    <h1 style="font-family: 'Ubuntu',sans-serif;">Resetar Senha</h1>
    <span onclick="document.getElementById('reset').style.display='none'" 
    class="w3-button w3-display-topright w3-red">&times;</span>
  </div>
  <div class="w3-container w3-center w3-card-4">
  <?php  if (isset($gravaUnidades)){ ?>
   <form action="index.php?acao=resetar" method="post">
   <br/>
      <select required id="unidadeLs" name="unidadeLs" class="w3-select w3-left">
        <option disabled selected>Selecione a Unidade..</option>
  <?php foreach($gravaUnidades as &$dadoUnidades) { ?>
    <option value="<?php echo $dadoUnidades[4] ?>"><?php echo $dadoUnidades[0]?> - <?php echo $dadoUnidades[3] ?></option>
  <?php } ?>
      </select>
    <br/><br/>
     <p class="w3-left-align">Digite o e-mail cadastrado no sistema:</p>
     <input type="text" class="w3-input" name="eemail" id="eemail" placeholder="Digite aqui.." />
     <br/>
     <div id="disable">
       <input type="submit" value="Resetar" onclick="sumir()" class="w3-btn w3-center w3-indigo"/>
     </div>
     <div id="fountainG" style="display: none;">
      <div id="fountainG_1" class="fountainG"></div>
      <div id="fountainG_2" class="fountainG"></div>
      <div id="fountainG_3" class="fountainG"></div>
      <div id="fountainG_4" class="fountainG"></div>
      <div id="fountainG_5" class="fountainG"></div>
      <div id="fountainG_6" class="fountainG"></div>
      <div id="fountainG_7" class="fountainG"></div>
      <div id="fountainG_8" class="fountainG"></div>
    </div>
  </form>
  <?php } ?>
  <br/>
</div>
</div>




<div id="footer" class="w3-container w3-display-bottommiddle">
  <p class="w3-text-black">© Copyright</p>
</div>
</div>
</body>
</html>