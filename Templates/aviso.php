<?php 
  $titlepag="Quadro de Avisos";
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php echo $titlepag ?></title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
</head>
<body>
    <h1 class="w3-center">Informações</h1>
	 <div id="login" class="w3-container " style="top:80px">
	     <div class="w3-third w3-border w3-round-xlarge w3-center w3-small w3-border-red w3-white" style="color: #000 ">
	         <h5>Bem-vindo ao Sistema de Agendamento de Laboratório de Informática!</h5><br/>
	         <p class="w3-margin" style="text-align: left">Com a intenção de otimizar e facilitar o processo de agendamento dos laboratórios de informática da Anhanguera Unidade Taquaral, desenvolvemos o portal online.</p><p class="w3-margin" style="text-align: left"> De qualquer computador da unidade será possível realizar solicitações de agendamento, efetuar cancelamento, consulta de laboratórios disponíveis, entre outras consultas que estarão disponíveis. </p>
             <br/>
	         <h5>Atenciosamente Equipe de TI FAC 3</h5>
	        
	     </div>
	 </div>
</body>
</html>