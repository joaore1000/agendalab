<?php
  $nome=$_SESSION["Nome"];
  $id_logado=$_SESSION["id"];
  $unidadeLs=$_SESSION["unidadeLs"];
  $titlepag="AgendaLAB - $nome";
  $site="";
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="refresh" content="1200">
<link rel="shortcut icon" href="css/images/favicon2.png" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<title><?php echo $titlepag ?></title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
</head>
<body>
<div id="bem_vindo" class="w3-container w3-blue w3-center"><h6><?php echo "AgendaLAB - Bem-vindo, $nome";?></h6>

	<a href="profile.php" title="Perfil" target="iframe_inicial" class=""><div id="btn2" class=" w3-left w3-hover-orange w3-display-topleft w3-blue w3-container"><h6>Perfil</h6></div></a>

		
</div>

<?php switch ($nivel) {
case 8:// SE O USUARIO FOR MASTER EXIBIRÁ ESSES MENUS
     
// $site="notification.php"; ?>
<a href="index.php?" title="Inicio"><div id="cadastrar" class="w3-display-topright w3-hover-orange w3-right w3-blue w3-container"><h6>Inicio</div></a></h6>


<div class="menu w3-container w3-pale-blue">
<ul>
<li><a href="#">Informática</a>
		<ul class="submenu">
			<li><a href="agendamento_informatica.php?inicial=S" target="iframe_inicial">Agendar </a></li>
			<li><a href="autorizar_agenda_data.php" target="iframe_inicial">Aprovar </a></li>
			<li><a href="cancelADM.php" target="iframe_inicial">Cancelar</a></li>
			<li><a href="meus_agendamento.php?volta=Y" target="iframe_inicial">Consultar</a></li>
			<li><a href="#">Chamada &raquo;</a>
				<ul>
					<li style="top: 180px"><a href="chamada.php?periodo=Manha" target="iframe_inicial">Diurno</a></li>
					<li style="top: 180px"><a href="chamada.php?periodo=Noite" target="iframe_inicial">Noturno</a></li>						
				</ul>
			</li>
		</ul>
</li>
<li><a href="#">Saúde</a>
		<ul class="submenu">
			<li><a href="agendar_lab_saude.php?inicial=S" target="iframe_inicial">Agendar </a></li>
			<li><a href="aprovar_agenda_saude.php" target="iframe_inicial">Aprovar</a></li>
			<li><a href="cancel_saude_adm.php" target="iframe_inicial">Cancelar</a></li>
			<li><a href="consultar_saude.php?volta=Y" target="iframe_inicial">Consultar</a></li>
			<li><a href="chamada_saude.php" target="iframe_inicial">Chamada</a></li>
		</ul>
</li>
<li><a href="#">Eng & Arquitetura</a>
		<ul class="submenu">
			<li><a href="agendar_lab_eng.php?inicial=S" target="iframe_inicial">Agendar </a></li>
			<li><a href="aprovar_agenda_eng.php" target="iframe_inicial">Aprovar</a></li>
			<li><a href="cancel_eng_adm.php" target="iframe_inicial">Cancelar</a></li>
			<li><a href="consultar_eng.php?volta=Y" target="iframe_inicial">Consultar</a></li>
			<li><a href="chamada_eng.php" target="iframe_inicial">Chamada</a></li>
		</ul>
</li>
<li><a href="#">Configurações</a>
		<ul class="submenu">
			<li><a href="#">Laboratórios &raquo;</a>
				<ul>
					<li><a href="config_lab.php" target="iframe_inicial">Informática</a></li>
					<li><a href="config_lab_saude.php" target="iframe_inicial">Saúde</a></li>
					<li><a href="config_lab_eng.php" target="iframe_inicial">Eng & Arquitetura</a></li>
				</ul>
			</li>
			<li><a href="config_cursos.php" target="iframe_inicial">Cursos</a></li>
			
		</ul>
</li>
<li><a href="panel_user.php" target="iframe_inicial">Usuários</a>
		<ul>
			<li><a href="cadastroadm.php" target="iframe_inicial">Cadastrar</a></li>
			<li><a href="remove_usersADM.php" target="iframe_inicial">Remover</a></li>
			<li><a href="newusers.php" target="iframe_inicial">Autorizar</a></li>
			<li><a href="blockerusers.php" target="iframe_inicial">Bloqueio/Desbloq.</a></li>
			
		</ul>
 </li>       
<li><a href="config_disc.php" target="iframe_inicial">Disciplina</a></li>
 <li><a href="new_dashboard.php" target="iframe_inicial">Dashboard</a></li>
 <li><a href="index.php?acao=logout" title="Sair">Sair &raquo;</a></li>       

</ul>
</div>
<?php	break;

	case 7:// SE O USUARIO FOR TI EXIBIRÁ ESSES MENUS
     
	    // $site="notification.php"; ?>
		<a href="index.php" title="Inicio"><div id="cadastrar" class="w3-display-topright w3-hover-orange w3-right w3-blue w3-container"><h6>Inicio</div></a></h6>


<div class="menu w3-container w3-pale-blue">
	<ul>
		<li><a href="#">Informática</a>
		        <ul class="submenu">
		        	<li><a href="agendamento_informatica.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="autorizar_agenda_data.php" target="iframe_inicial">Aprovar </a></li>
		        	<li><a href="cancelADM.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="meus_agendamento.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        	<li><a href="#">Chamada &raquo;</a>
                        <ul>
							<li style="top: 180px"><a href="chamada.php?periodo=Manha" target="iframe_inicial">Diurno</a></li>
							<li style="top: 180px"><a href="chamada.php?periodo=Noite" target="iframe_inicial">Noturno</a></li>						
						</ul>
					</li>
		        </ul>
		</li>
		<li><a href="#">Saúde</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_saude.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancel_saude_prof.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="consultar_saude.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        	
		        </ul>
		</li>
		<li><a href="#">Eng & Arquitetura</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_eng.php?inicial=S" target="iframe_inicial">Agendar </a></li>
					<li><a href="cancel_eng_prof.php" target="iframe_inicial">Cancelar</a></li>
					<li><a href="consultar_eng.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="#">Configurações</a>
		        <ul class="submenu">
		        	<li><a href="#">Laboratórios &raquo;</a>
		        		<ul>
		        			<li><a href="config_lab.php" target="iframe_inicial">Informática</a></li>
		        		</ul>
		        	</li>
		        	<li><a href="config_cursos.php" target="iframe_inicial">Cursos</a></li>
		        	
		        </ul>
		</li>
		<li><a href="panel_user.php" target="iframe_inicial">Usuários</a>
		        <ul>
		        	<li><a href="cadastroadm.php" target="iframe_inicial">Cadastrar</a></li>
		        	<li><a href="remove_usersADM.php" target="iframe_inicial">Remover</a></li>
		        	<li><a href="newusers.php" target="iframe_inicial">Autorizar</a></li>
		        	<li><a href="blockerusers.php" target="iframe_inicial">Bloqueio/Desbloq.</a></li>
		        	
		        </ul>
		 </li>       
		 <li><a href="index.php?acao=logout" title="Sair">Sair &raquo;</a></li>       
		
	</ul>
  </div>
	<?php	break;
case 6: // SE O USUARIO FOR ENGE EXIBIRÁ ESSES MENUS
     
	    // $site="aulas_eng.php"; ?>
		<a href="index.php" title="Inicio" class=""><div id="cadastrar" class="w3-display-topright w3-hover-orange w3-right w3-blue w3-container"><h6>Inicio</div></a></h6>


<div class="menu w3-container w3-pale-blue">
	<ul>
		<li><a href="#">Informática</a>
		        <ul class="submenu">
		        	<li><a href="agendamento_informatica.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancelamento.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="meus_agendamento.php?volta=Y" target="iframe_inicial">Consultar</a></li>
			</ul>
		</li>
		<li><a href="#">Saúde</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_saude.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancel_saude_prof.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="consultar_saude.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="#">Eng & Arquitetura</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_eng.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="aprovar_agenda_eng.php" target="iframe_inicial">Aprovar</a></li>
					<li><a href="cancel_eng_adm.php" target="iframe_inicial">Cancelar</a></li>
					<li><a href="consultar_eng.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        	<li><a href="chamada_eng.php" target="iframe_inicial">Chamada</a></li>
		        </ul>
		</li>
		<li><a href="#">Configurações</a>
		        <ul class="submenu">
		        	<li><a href="#">Laboratórios &raquo;</a>
		        		<ul>
							<li><a href="config_lab_eng.php" target="iframe_inicial">Eng & Arquitetura</a></li>
		        		</ul>
		        	</li>
		        	<li><a href="config_cursos.php" target="iframe_inicial">Cursos</a></li>
		        	
		        </ul>
		</li>
		<li><a href="panel_user.php" target="iframe_inicial">Usuários</a>
		        <ul>
		        	<li><a href="cadastroadm.php" target="iframe_inicial">Cadastrar</a></li>
		        	<li><a href="remove_usersADM.php" target="iframe_inicial">Remover</a></li>
		        	<li><a href="newusers.php" target="iframe_inicial">Autorizar</a></li>
		        	<li><a href="blockerusers.php" target="iframe_inicial">Bloqueio/Desbloq.</a></li>
		        </ul>
		 </li>      
		 <li><a href="index.php?acao=logout" title="Sair">Sair &raquo;</a></li>       
		
	</ul>
  </div>
	<?php	break;
	case 5:// SE O USUARIO FOR SAUDE EXIBIRÁ ESSES MENUS
     
	    // $site="aulas_saude.php"; ?>
		<a href="index.php" title="Inicio" class=""><div id="cadastrar" class="w3-display-topright w3-hover-orange w3-right w3-blue w3-container"><h6>Inicio</div></a></h6>


<div class="menu w3-container w3-pale-blue">
	<ul>
		<li><a href="#">Informática</a>
		        <ul class="submenu">
		        	<li><a href="agendamento_informatica.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancelamento.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="meus_agendamento.php?volta=Y" target="iframe_inicial">Consultar</a></li>
			</ul>
		</li>
		<li><a href="#">Saúde</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_saude.php?inicial=S" target="iframe_inicial">Agendar </a></li>
					<li><a href="aprovar_agenda_saude.php" target="iframe_inicial">Aprovar </a></li>
		        	<li><a href="cancel_saude_adm.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="consultar_saude.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        	<li><a href="chamada_saude.php" target="iframe_inicial">Chamada</a></li>
		        </ul>
		</li>
		<li><a href="#">Eng & Arquitetura</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_eng.php?inicial=S" target="iframe_inicial">Agendar </a></li>
					<li><a href="cancel_eng_prof.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="consultar_eng.php?volta=Y" target="iframe_inicial">Consultar</a></li>		        	
		        </ul>
		</li>
		<li><a href="#">Configurações</a>
		        <ul class="submenu">
		        	<li><a href="#">Laboratórios &raquo;</a>
		        		<ul>
						<li><a href="config_lab_saude.php" target="iframe_inicial">Saúde</a></li>
		        		</ul>
		        	</li>
		        	<li><a href="config_cursos.php" target="iframe_inicial">Cursos</a></li>
		        	
		        </ul>
		</li>
		<li><a href="panel_user.php" target="iframe_inicial">Usuários</a>
		        <ul>
		        	<li><a href="cadastroadm.php" target="iframe_inicial">Cadastrar</a></li>
		        	<li><a href="remove_usersADM.php" target="iframe_inicial">Remover</a></li>
		        	<li><a href="newusers.php" target="iframe_inicial">Autorizar</a></li>
		        	<li><a href="blockerusers.php" target="iframe_inicial">Bloqueio/Desbloq.</a></li>
		        </ul>
		 </li>       
		 <li><a href="index.php?acao=logout" title="Sair">Sair &raquo;</a></li>       
		
	</ul>
  </div>
	<?php	break;
         case 4:
         // SE O USUARIO FOR DIRETOR EXIBIRÁ ESSES MENUS ?>
         
  <div class="menu w3-container w3-pale-blue">
	<ul>

		<li><a href="#">Informática</a>
		        <ul class="submenu">
		        	<li><a href="agendamento_informatica.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancelamento.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="meus_agendamento.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="#">Saúde</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_saude.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancel_saude_prof.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="consultar_saude.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="#">Eng & Arquitetura</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_eng.php?inicial=S" target="iframe_inicial">Agendar</a></li>
					<li><a href="cancel_eng_prof.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="consultar_eng.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="panel_user.php">Usuários</a>
		        <ul>
		        	<li><a href="cadastroadm.php" target="iframe_inicial">Cadastrar</a></li>
		        	<li><a href="remove_usersDIRACAD.php" target="iframe_inicial">Remover</a></li>
		        	<li><a href="blockerusers.php" target="iframe_inicial">Bloqueio/Desbloq.</a></li>
		        	
		        </ul>
		 </li>  
		 <li><a href="index.php?acao=logout" title="Sair">Sair &raquo;</a></li>       
		
	</ul>
  </div>

<?php	break;
         case 3:// SE O USUARIO FOR Acadêmico EXIBIRÁ ESSES MENUS ?>
       
  <div class="menu w3-container w3-pale-blue">
  <ul>

	<li><a href="#">Informática</a>
			<ul class="submenu">
				<li><a href="agendamento_informatica.php?inicial=S" target="iframe_inicial">Agendar </a></li>
				<li><a href="cancelamento.php" target="iframe_inicial">Cancelar</a></li>
				<li><a href="meus_agendamento.php?volta=Y" target="iframe_inicial">Consultar</a></li>
			</ul>
	</li>
	<li><a href="#">Saúde</a>
			<ul class="submenu">
				<li><a href="agendar_lab_saude.php?inicial=S" target="iframe_inicial">Agendar </a></li>
				<li><a href="cancel_saude_prof.php" target="iframe_inicial">Cancelar</a></li>
				<li><a href="consultar_saude.php?volta=Y" target="iframe_inicial">Consultar</a></li>
			</ul>
	</li>
	<li><a href="#">Eng & Arquitetura</a>
			<ul class="submenu">
				<li><a href="agendar_lab_eng.php?inicial=S" target="iframe_inicial">Agendar</a></li>
				<li><a href="cancel_eng_prof.php" target="iframe_inicial">Cancelar</a></li>
				<li><a href="consultar_eng.php?volta=Y" target="iframe_inicial">Consultar</a></li>
			</ul>
	</li>
	<li><a href="panel_user.php">Usuários</a>
			<ul>
				<li><a href="cadastroadm.php" target="iframe_inicial">Cadastrar</a></li>
				<li><a href="remove_usersDIRACAD.php" target="iframe_inicial">Remover</a></li>
				<li><a href="blockerusers.php" target="iframe_inicial">Bloqueio/Desbloq.</a></li>
				
			</ul>
	</li>  
	<li><a href="index.php?acao=logout" title="Sair">Sair &raquo;</a></li>       

	</ul>
  </div>

<?php break;
	 case 2:  //SE O USUARIO FOR COORDENADOR IRA EXIBIR ESSES MENUS 
	//  $site="main_user.php"; ?>
        
 
 <div class="menu w3-container w3-pale-blue">
	<ul>
		<li><a href="#">Informática</a>
		        <ul class="submenu">
		        	<li><a href="agendamento_informatica.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancelamento.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="meus_agendamento.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="#">Saúde</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_saude.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancel_saude_prof.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="consultar_saude.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="#">Eng & Arquitetura</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_eng.php?inicial=S" target="iframe_inicial">Agendar </a></li>
					<li><a href="cancel_eng_prof.php" target="iframe_inicial">Cancelar</a></li>
					<li><a href="consultar_eng.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="#">Usuarios</a>
		        <ul>
		        	<li><a href="cadastroadm.php" target="iframe_inicial">Cadastrar</a></li>
		        	<li><a href="remove_usersCOOD.php" target="iframe_inicial">Remover</a></li>
		        	<li><a href="blockCOOD.php" target="iframe_inicial">Bloqueio/Desbloq.</a></li>
		        	
		        </ul>
		</li>    

		 <li><a href="index.php?acao=logout" title="Sair">Sair &raquo;</a></li>       
		
	</ul>
	 
	
  </div>

<?php break;
	 case 1:
	//   $site="main_user.php";  ?>
   <a href="index.php" title="Inicio"><div id="cadastrar" class="w3-display-topright w3-hover-orange w3-right w3-blue w3-container"><h6>Inicio</div></a></h6>
 <div class="menu w3-container w3-pale-blue">
	<ul>
		<li><a href="#">Informática</a>
		        <ul class="submenu">
		        	<li><a href="agendamento_informatica.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancelamento.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="meus_agendamento.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="#">Saúde</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_saude.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancel_saude_prof.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="consultar_saude.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="#">Eng & Arquitetura</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_eng.php?inicial=S" target="iframe_inicial">Agendar </a></li>
					<li><a href="cancel_eng_prof.php" target="iframe_inicial">Cancelar</a></li>
					<li><a href="consultar_eng.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
	
		<li><a href="index.php?acao=logout" title="Sair">Sair &raquo;</a></li>       
		
	</ul>
  </div>
<?php break;
         default: ?>
       <a href="index.php" title="Inicio"><div id="cadastrar" class="w3-display-topright w3-hover-orange w3-right w3-blue w3-container"><h6>Inicio</div></a></h6>
 <div class="menu w3-container w3-pale-blue">
 <ul>
		<li><a href="#">Informática</a>
		        <ul class="submenu">
		        	<li><a href="agendamento_informatica.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancelamento.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="meus_agendamento.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="#">Saúde</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_saude.php?inicial=S" target="iframe_inicial">Agendar </a></li>
		        	<li><a href="cancel_saude_prof.php" target="iframe_inicial">Cancelar</a></li>
		        	<li><a href="consultar_saude.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
		<li><a href="#">Eng & Arquitetura</a>
		        <ul class="submenu">
		        	<li><a href="agendar_lab_eng.php?inicial=S" target="iframe_inicial">Agendar </a></li>
					<li><a href="cancel_eng_prof.php" target="iframe_inicial">Cancelar</a></li>
					<li><a href="consultar_eng.php?volta=Y" target="iframe_inicial">Consultar</a></li>
		        </ul>
		</li>
	
		<li><a href="index.php?acao=logout" title="Sair">Sair &raquo;</a></li>       
		
	</ul>
  </div>
	 <?php break; 
	} ?>
<div id="w3-container">
  	<iframe src="home_page.php" name="iframe_inicial" style="height: 89%;width:100%"></iframe>
  	
  </div>
</body>
</html>