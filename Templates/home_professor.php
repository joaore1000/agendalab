
<div class="container-fluid py-md-3 pl-md-5">
    <div class="row">
        <div class="col alert alert-primary" role="alert">
            <h1>Bem-vindo ao <strong>AgendaLAB</strong>, <?php echo $nome;?>!</h1>
            <h6>Portal de agendamentos de laboratórios da Regional Leste/Sul.</h6>
        </div>
    </div>
    <hr>
</div>

<div class="container-fluid pl-md-5">
    <div class="row">
        <div class="col text-dark">
            <h3>Proximos agendamentos..</h3>
        </div>
    </div>
    
    <!-- INFORMATICA -->
    <div>
        <div>
            <?php
                // Paginando tabela da informática
                $sqlTotal="SELECT * FROM agendadata WHERE agendado_para='$idLogado' AND data >='$dataToday' AND chamada='' AND situation NOT IN ('CANCELADO') ORDER BY data";
                $qrTotal = mysql_query($sqlTotal) or die (mysql_error());
                $numTotal= mysql_num_rows($qrTotal);
                $paginarInf="inf";
                $pag = new Paginacao($numTotal, $pInf);
                $pag->paginar($paginarInf);
                
                

                $sql= $sqlTotal." ASC LIMIT ".$pag->limite();
                $mq = mysql_query($sql);
            ?>
            <table class="table table-striped table-bordered table-hover table-sm text-center">
                <caption class="text-dark">Laboratórios Informática</caption>
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Data</th>
                        <th scope="col">Agendado por</th>
                        <th scope="col">Turno</th>
                        <th scope="col">Curso</th>
                        <th scope="col">Disciplina</th>
                        <th scope="col">Laboratorio</th>
                        <!-- <th scope="col">Ação</th> -->
                    </tr>
                </thead>
                <?php 
                if($numTotal==0){
                    $msgInf="Sem agendamentos em seu nome..";
                ?>
                <tbody class="alert alert-danger"><tr><th colspan="7"><?php echo $msgInf ;?></th></tr></tbody>
                <?php 
                }else{
                ?>
                <tbody>
                <?php while($w = mysql_fetch_array($mq, MYSQL_ASSOC)){
                   $dataDiv1 = explode('-', $w["data"]);
                   $dataBRI = $dataDiv1[2].'/'.$dataDiv1[1].'/'.$dataDiv1[0];

                   //Seleciona o Nome do Coordenador
                    $id_cood=$w["Coordenador"];
                    $query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$id_cood'");
                    $array=mysql_fetch_array($query_coord);
                    //Seleciona o Nome da Disciplina
                    $aula=$w["Aula"];
                    $disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$aula'") or die(mysql_error());
                    $nome_disciplina=mysql_fetch_assoc($disciplina);
                    //Seleciona o Nome do curso
                    $curso=$w["Disciplina"];
                    $cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
                    $nome_curso=mysql_fetch_assoc($cursoMq);
                    //Usuário agendado
                    $userRequest=$w["agendado_para"];
                    $userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userRequest'") or die(mysql_error());
                    $agendado_para=mysql_fetch_assoc($userMq);
           
                     ?>
                    <tr>
                        <th scope="row"><?php echo $w["ID"];?></th>
                        <td><?php echo $dataBRI;?></td>
                        <td><?php echo $w["agendado_por"];?></td>
                        <td><?php echo $w["periodo"];?></td>
                        <td><?php echo $nome_curso["curso"];?></td>
                        <td><?php echo $nome_disciplina["disciplina"];?></td>
                        <td><?php echo $w["Lab"];?></td>
                        <!-- <td>Ação</td> -->
                    </tr>
                <?php } ?>
                </tbody>
                <?php } ?>
            </table>
        </div>
    <hr/>
    <!-- ENGENHARIA -->
        <div>
            <?php
                $slqEngTotal="SELECT * FROM agendadata_eng WHERE agendado_para='$idLogado' AND data >='$dataToday' AND chamada='' AND situation NOT IN ('CANCELADO') ORDER BY data";
                $mqEngTotal = mysql_query($slqEngTotal) or die (mysql_error());
                $mnrEngTotal= mysql_num_rows($mqEngTotal);
                $paginarEng="eng";
                $pagEng = new Paginacao($mnrEngTotal, $pEng);
                $pagEng->paginar($paginarEng);
                

                $sqlEng= $slqEngTotal." ASC LIMIT ".$pagEng->limite();
                $mqEng = mysql_query($sqlEng);
            ?>
            <table class="table table-striped table-bordered table-hover table-sm text-center">
                <caption class="text-dark">Laboratórios Engenharia</caption>
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Data</th>
                        <th scope="col">Agendado Por</th>
                        <th scope="col">Turno</th>
                        <th scope="col">Curso</th>
                        <th scope="col">Disciplina</th>
                        <th scope="col">Laboratorio</th>
                        <!-- <th scope="col">Ação</th> -->
                    </tr>
                </thead>
                <?php 
                if($mnrEngTotal==0){
                    $msgEng="Sem agendamentos em seu nome..";
                ?>
                <tbody class="alert alert-danger"><tr><th colspan="7"><?php echo $msgEng ;?></th></tr></tbody>
                <?php 
                }else{
                ?>
                <tbody>
                <?php while($eng = mysql_fetch_array($mqEng, MYSQL_ASSOC)){
                    $dataDivEng = explode('-', $eng["data"]);
                    $dataBrEng = $dataDivEng[2].'/'.$dataDivEng[1].'/'.$dataDivEng[0];

                    //Seleciona o Nome do Coordenador
                    $id_cood=$eng["Coordenador"];
                    $query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$id_cood'");
                    $array=mysql_fetch_array($query_coord);
                    //Seleciona o Nome da Disciplina
                    $aula=$eng["Aula"];
                    $disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$aula'") or die(mysql_error());
                    $nome_disciplina=mysql_fetch_assoc($disciplina);
                    //Seleciona o Nome do curso
                    $curso=$eng["Disciplina"];
                    $cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
                    $nome_curso=mysql_fetch_assoc($cursoMq);
                    //Usuário agendado
                    $userRequest=$eng["agendado_para"];
                    $userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userRequest'") or die(mysql_error());
                    $agendado_para=mysql_fetch_assoc($userMq);
                     ?>
                    <tr>
                        <th scope="row"><?php echo $eng["ID"];?></th>
                        <td><?php echo $dataBrEng; ?></td>
                        <td><?php echo $eng["agendado_por"];?></td>
                        <td><?php echo $eng["periodo"];?></td>
                        <td><?php echo $nome_curso["curso"];?></td>
                        <td><?php echo $nome_disciplina["disciplina"];?></td>
                        <td><?php echo $eng["Lab"];?></td>
                        <!-- <td>Ação</td> -->
                    </tr>
                    <?php } ?>
                </tbody>
                <?php } ?>
            </table>
        </div>
    <hr/>
    <!-- SAUDE -->
        <div>
            <?php
                $slqSauTotal="SELECT * FROM agendadata_saude WHERE agendado_para='$idLogado' AND data >='$dataToday' AND chamada='' AND situation NOT IN ('CANCELADO') ORDER BY data";
                $mqSauTotal = mysql_query($slqSauTotal) or die (mysql_error());
                $mnrSauTotal= mysql_num_rows($mqSauTotal);
                $paginarSau="sau";
                $pagSau = new Paginacao($mnrSauTotal, $pSau);
                $pagSau->paginar($paginarSau);
                

                $sqlSau= $slqSauTotal." ASC LIMIT ".$pagSau->limite();
                $mqSau = mysql_query($sqlSau);
            ?>
            <table class="table table-striped table-bordered table-hover table-sm text-center">
                <caption class="text-dark">Laboratórios Saúde</caption>
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Data</th>
                        <th scope="col">Agendado Por</th>
                        <th scope="col">Turno</th>
                        <th scope="col">Curso</th>
                        <th scope="col">Disciplina</th>
                        <th scope="col">Laboratorio</th>
                        <!-- <th scope="col">Ação</th> -->
                    </tr>
                </thead>
                <?php 
                if($mnrSauTotal==0){
                    $msg="Sem agendamentos em seu nome..";
                ?>
                <tbody class="alert alert-danger"><tr><th colspan="7"><?php echo $msg ;?></th></tr></tbody>
                <?php 
                }else{
                ?>
                <tbody>
                <?php while($sau = mysql_fetch_array($mqSau, MYSQL_ASSOC)){
                    $dataDivsau = explode('-', $sau["data"]);
                    $dataBrsau = $dataDivsau[2].'/'.$dataDivsau[1].'/'.$dataDivsau[0];

                    //Seleciona o Nome do Coordenador
                    $id_cood=$sau["Coordenador"];
                    $query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$id_cood'");
                    $array=mysql_fetch_array($query_coord);
                    //Seleciona o Nome da Disciplina
                    $aula=$sau["Aula"];
                    $disciplina=mysql_query("SELECT * FROM new_disciplina WHERE cod_disci='$aula'") or die(mysql_error());
                    $nome_disciplina=mysql_fetch_assoc($disciplina);
                    //Seleciona o Nome do curso
                    $curso=$sau["Disciplina"];
                    $cursoMq=mysql_query("SELECT * FROM cursos WHERE cod_curso='$curso'") or die(mysql_error());
                    $nome_curso=mysql_fetch_assoc($cursoMq);
                    //Usuário agendado
                    $userRequest=$sau["agendado_para"];
                    $userMq=mysql_query("SELECT * FROM usuarios WHERE id='$userRequest'") or die(mysql_error());
                    $agendado_para=mysql_fetch_assoc($userMq);
                     ?>
                    <tr>
                        <th scope="row"><?php echo $sau["ID"];?></th>
                        <td><?php echo $dataBrsau;?></td>
                        <td><?php echo $sau["agendado_por"];?></td>
                        <td><?php echo $sau["periodo"];?></td>
                        <td><?php echo $nome_curso["curso"];?></td>
                        <td><?php echo $nome_disciplina["disciplina"];?></td>
                        <td><?php echo $sau["Lab"];?></td>
                        <!-- <td>Ação</td> -->
                    </tr>
                    <?php } ?>
                </tbody>
                <?php } ?>
            </table>
        </div>
    <hr/>
    
    </div>
</div>

