<?php 
include("includes/header_config.php");
header('Content-Type: text/html; charset=utf-8');


?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title><?php echo $titlepag ?></title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
  <link href="/open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/open-iconic-master/font/css/open-iconic-bootstrap.css"/>



</head>
<style type="text/css">
*{
  margin:0;
  padding: 0;
  outline:none;
  list-style:none;
  font-family: 'Ubuntu',sans-serif;
}
</style>



<body>

  <div class="container" name="header" id="">
    <br/>
    <h2>Painel de disciplinas</h2>
    <p> Cadastro de novas disciplinas, editar disciplina, excluir disciplina..</p>
    <hr>
  </div>
  <div name="" class="navbar container">
    <a href="#" onclick="javascript: if (confirm('Essa ação irá DESATIVAR todas as disciplinas ativas. Tem certeza que deseja fazer isso?'))location.href='?acao=desativarDisci'" class="btn btn-warning float-left"><span class="glyphicon glyphicon-erase"></span> Desativar tudo</a>
    <a href="nova_disci.php" class="btn btn-primary float-right"><span class="oi oi-plus" aria-hidden="true"></span> Novo</a> 
  </div> 
  <br/>

<div class="container table-responsive">
       
        <input autofocus class="form-control" type="text" placeholder="Pesquisar por nome da disciplina" id="myInput" onkeyup="myFunction()">
        <br/>
         <table id="tab" class="table table-sm table-bordered table-striped table-hover table-condesed"> 
            <thead class="thead-dark">
               <tr class="text-center" style="font-size: 15px">
                   <th>#</th>
                   <th>Disciplina</th>
                   <th>Curso</th>
                   <th>Turma</th>
                   <th>Semestre</th>
                   <th>Tipo</th>
                   <th>Carga Horária</th>
                   <th>Situação</th>
               </tr>
             </thead>
               <?php
               $query_select=mysql_query("SELECT * FROM new_disciplina ORDER BY disciplina");

               if (mysql_num_rows($query_select)  == 0 ) {
                    echo "<div class=\"message\"> Sem disciplinas cadastradas e configurados. Clique em 'Novo' para adicionar uma nova disciplina. </div>";
                    
                }else{
                  while($row=mysql_fetch_array($query_select)){

                 ?>

                  <tr class="text-center" style="font-size: 14px">
                   <td><?php echo $row["cod_disci"]; ?></td>
                   <td><?php echo $row["disciplina"]; ?></td>
                   <td><?php echo $row["curso"]; ?></td>
                   <td><?php echo $row["turma"]; ?></td>
                   <td><?php echo $row["etapa"]; ?></td>
                   <td><?php echo $row["tipo_disci"]; ?> </td>
                   <td><?php echo $row["carga_horaria"]; ?></td>
                   <td><?php echo $row["situacao_turma"]; ?></td>
               </tr>

              <?php
                    }
                }
               ?>

           </table>
    </div>
    <br/>
  
   

</div>
 
<script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("tab");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script> 

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>