
<?php
date_default_timezone_set('America/Sao_Paulo');
include("includes/header_config_eng.php");

$usuarioLogado=$_SESSION["email"];
$usuarioNome=$_SESSION["Nome"];
$dataToday = date("Y-m-d");
$dataLast = date( "Y-m-d", strtotime( $dataToday."+5 day" ) );

$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1; 
$sqlTotal="SELECT * FROM agendadata_eng WHERE email='$usuarioLogado' AND data >='$dataToday' AND chamada='' AND situation NOT IN ('CANCELADO') ORDER BY data";
$qrTotal = mysql_query($sqlTotal) or die (mysql_error());
$numTotal= mysql_num_rows($qrTotal);
$qtn = 10;
$totalPagina= ceil($numTotal/$qtn);
$inicio = ($qtn * $pagina) - $qtn;

?>


<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/w3.css"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style type="text/css">
	body{
		background-color: white;
	}

</style>
</head>
<body>
	<br/>
	<div class="w3-container">
		<div class="container">
			<h2>Aulas Agendadas Engenharia</h2>
			<p>Todos os agendamentos do laboratorio de <b>Engenharia</b> estão sendo exibidos aqui..</p>
		</div>
<hr>
		<div class="container">
			<!-- AQUI VAI A TABELA COM OS LABORATORIOS  E COM MENU DE FECHAR E ABRIR, DESATIVAR E ATIVAR. -->
			<h4>
				Agendamentos previstos:
			</h4>
			<table class="w3-table w3-small w3-striped w3-hoverable w3-bordered w3-centered table-border"> 
				<tr class="w3-white" >
                    <th>#</th>
					<th>Data</th>
                    <th>Nome</th>
					<th>Turno</th>
					<th>Curso</th>
					<th>Disciplina</th>
					<th>Observação</th>
					<th>Ação</th>
				</tr>
				<?php
				$query_select=mysql_query("SELECT * FROM agendadata_eng WHERE chamada='' AND data >='$dataToday' AND situation NOT IN ('CANCELADO') ORDER BY data");

				if (mysql_num_rows($query_select)  == 0 ) {
					echo "<div class=\"message\"> Sem agendamentos pendentes.. </div>";

				}else{
					while($row=mysql_fetch_array($query_select)){
						$id_id=$row["ID"];

						switch ($row["Softwares"]) {
							case "":
							$modal="ND";

							break;

							default:
							$modal="<img src=\"css/images/lupa_obs.png\" title=\"Observação: ".$row["Softwares"]."\" style=\"width: 20px;height: 20px;\">";

							break;

						}

						$dataemail = $row["data"];
						$dataDiv = explode('-', $dataemail);
						$dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];

						$aula=$row["Aula"];
						$disciplina=mysql_query("SELECT * FROM disciplina WHERE id_disc='$aula'") or die(mysql_error());
						$nome_disciplina=mysql_fetch_assoc($disciplina);
						?>

						<tr>
                            <td><?php echo $id_id; ?></td>
							<td><?php echo $dataBR; ?></td>
                            <td><?php echo $row["Nome"]." ".$row["sobrenome"];?></td>
							<td><?php echo $row["periodo"]; ?></td>
							<td><?php echo $row["Disciplina"]; ?></td>
							<td><?php echo $nome_disciplina["nome_disc"]; ?></td>
							<td><?php echo $modal; ?></td>
							<td class="buttons">
								<a href="edit_agendamento_eng_admin.php?numberid=<?php echo $id_id; ?>" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span></a>
							</td>
						</tr>

						<?php
					}
				} 
				?>

			</table>
		</div>
		<br/>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>