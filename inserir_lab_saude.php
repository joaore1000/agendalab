<?php 
  $titlepag="FAC3 - Novo Laboratório";
  include("includes/header_config_saude.php");
  header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php echo $titlepag ?></title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
</head>
<body>
  <div class="w3-card-4 w3-display-middle" style="width: 40%;">
    	<header class="w3-container w3-blue w3-center">
    		<h2>Adicionar Laboratório Saúde</h2>
    	</header>
	 <div class="message" style="<?php echo $display;?>"><?php echo $msg;?></div>
	<div id="disable" class="w3-container w3-center">
          <form action="?acao=addLab" method="post">
          	<br/>
             <label for="nome_lab" class="w3-left-align"><p><b>Nome do Laboratório:</b></p></label><input autofocus id="nome_lab" type="text" name="Nomelab" required class="w3-input"/>
             <br/>
		 
		     <div class="w3-left-align">
		     <p><b>Status</b></p>
		     <input type="radio" id="enable" name="status" class="w3-radio" value="1"/><label for="status"> Ativado</label>
		     <input type="radio" checked id="disable"  name="status" class="w3-radio" value="0"/><label for="status"> Desativado</label><br>
		     </div>
		     <br/>
		     <input type="submit" class="w3-btn w3-blue w3-margin w3-right" value="Adicionar"/>
		     <a href="config_lab_saude.php" class="w3-btn w3-red w3-margin w3-left" >Voltar</a>
		  </form>
        </div>
	
	</div>
</body>
</html>