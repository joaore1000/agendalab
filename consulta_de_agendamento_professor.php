<?php
include( "includes/header_consultaAG.php" );
$nome = $_SESSION[ "Nome" ];
if(isset($_GET['volta']) && $_GET['volta'] == 'Y' && isset($_SESSION['agendaconsul'])){
	unset($_SESSION['agendaconsul']);
}


?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title></title>
	
	<link rel="stylesheet" type="text/css" href="css/w3.css"/>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
	<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
	<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
	<script LANGUAGE="JavaScript">
		$(function() {
			$("#tInicial").datepicker({
				dateFormat: 'dd/mm/yy',
				dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
				dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
				dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
				monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
				monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
				

			});
		});
		$(function() {
			$("#tFinal").datepicker({
				dateFormat: 'dd/mm/yy',
				dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
				dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
				dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
				monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
				monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
				

			});
		});
	</script>


</head>

<body>

	

	<h2 class="w3-center">Consulta de Agendamentos</h2>
	<div id="container-1" class="w3-margin">
		<div class="w3-panel w3-pale-red w3-leftbar w3-rightbar w3-border-red" style="<?php echo $display;?>"><?php echo $msg;?></div>
		<form name="cUsers" method="post" action="?acao=filtrar">
			<fieldset class="w3-container w3-border w3-white w3-border-blue w3-card-4 w3-margin">
				<legend class="w3-blue"><strong>Filtro de Consulta</strong></legend>
				<div class="w3-half">
					<label for="tNome"><b>Nome: </b></label><input type="text" style="width:95%" name="cNome" id="tNome" class="w3-input w3-border" placeholder="<?php echo $nome;?>" disabled />
				</div>
				<div class="w3-half">
					<label for="tSituacao"><p><b>Situação: </b></p></label>
					<select id="tSituacao" name="cSit" class="w3-select w3-border" style="width: 30%" >
						<option value="RESERVADO">RESERVADOS</option>
						<option value="CANCELADO">CANCELADOS</option>
						<option value="AGUARDANDO">AGUARDANDO</option>
						<option value="A">TODOS</option>
					</select><br/><br/>
				</div>
				<label><b>Turno: </b></label>
				<input type="radio" class="w3-radio" name="cPeriodo" id="tNoite" value="Noite" checked /><label for="tNoite"> Noite </label> |
				<input type="radio" class="w3-radio" name="cPeriodo" id="tManha" value="Manha" /><label for="tManha"> Manhã </label> |
				<input type="radio" class="w3-radio" name="cPeriodo" id="tTodos" value="Todos" /><label for="tTodos"> Todos </label>
				
				<br/><br/>
				<div class="w3-quarter">
					<label for="tInicial"><b>Data Inicial: </b></label><input type="text" name="cInicial" id="tInicial" class="w3-input w3-border" placeholder="01/01/2018" style="width: 50%" />
				</div>
				<div class="w3-quarter">
					<label for="tFinal"><b>Data Final: </b></label><input type="text" name="cFinal" id="tFinal" class="w3-input w3-border" placeholder="31/12/2018" style="width: 50%" />
				</div>
				
				<input type="submit" value="Filtrar" class="w3-btn w3-blue w3-right w3-margin" /><br/>
			</fieldset>
		</form>
	</div>
	<br/>
	<?php 
	if (isset($_SESSION['agendaconsul'])){

		
		?>
		<div id="resultado" class="w3-container w3-border w3-round-xlarge w3-center" style="overflow: auto;">
			<?php
			$sqlTotal=$_SESSION["pesquisar"];
			$qrTotal = mysql_query($sqlTotal) or die (mysql_error());
			$numTotal= mysql_num_rows($qrTotal);
			$totalPagina= ceil($numTotal/$qtn);
			?>

			<div class=" w3-panel w3-center w3-small">
				<div class="w3-bar w3-round w3-border" >
					<?php
						//Apresentar a paginação
					for($i = 1; $i <= $totalPagina; $i++){ 
						if($i == $pagina){ ?>
							<a class="w3-button w3-bar-item w3-blue"><b><?php echo $i; ?></b></a>
							<?php }else{ ?>
								<a href="?pagina=<?php echo $i;?>" class="w3-button w3-bar-item" ><?php echo $i; ?></a>
								<?php } 	  
							}   ?>
						</div>
					</div>
					<div class="w3-panel w3-small">
						<p class="w3-left w3-margin-top"><b>Resultado:</b> <?php echo $numTotal;?> Agendamentos - Mostrando <b>10</b> por página</p>
						<p><a href="?acao=exportar" class="w3-right"><img src="css/images/icoExcel.png" style="width:35px;height: 35px;bottom:5%"></a></p>
					</div>
					<div id="resultado" class="w3-panel w3-responsive w3-animate-top">

						<table class="w3-table-all w3-hoverable w3-border-blue w3-tiny w3-card-2 w3-border">
							<thead>
								<tr class="w3-orange w3-small">

									<th style="text-align: center;">Data</th>
									<th style="text-align: center;">Nome</th>
									<th style="text-align: center;">Sobrenome</th>
									<th style="text-align: center;">Turno</th>
									<th style="text-align: center;">Laboratório</th>
									<th style="text-align: center;">Alunos</th>
									<th style="text-align: center;">Disciplina</th>
									<th style="text-align: center;">Curso</th>
									<th style="text-align: center;">Coordenador</th>
									<th style="text-align: center;">Proj</th>
									<th style="text-align: center;">Mic</th>
									<th style="text-align: center;">Som</th>
									<th style="text-align: center;">Obs.</th>
									<th style="text-align: center;">Situação</th>
									
									
								</tr>
							</thead>
							<tbody>	
								<?php  foreach($_SESSION['agendaconsul'] as &$linha) {
									$data = $linha[6];
									$dataDiv = explode('-', $data);
									$dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];
									

									$id_cood=$linha[3];
									$id_user=$_SESSION["id"];
									$query_coord=mysql_query("SELECT Nome FROM coordenadores WHERE id_cood='$id_cood'");
									$array=mysql_fetch_array($query_coord);
									$query_user=mysql_query("SELECT Sobrenome FROM usuarios WHERE id='$id_user'");
									$auser=mysql_fetch_array($query_user);
									$aula=$linha[9];
			                        $disciplina=mysql_query("SELECT * FROM disciplina WHERE id_disc='$aula'") or die(mysql_error());
                                    $nome_disciplina=mysql_fetch_assoc($disciplina);



									switch ($linha[17]) {
										case 'RESERVADO':
										$cor="green";
										break;
										
										case 'CANCELADO':
										$cor="red";
										break;
										case 'AGUARDANDO':
										$cor="blue";
										
										break;
										default:
										$cor="blue";
										
										break;    	
									}
									switch ($linha[15]) {
										case "":
										$modal="ND";
										
										break;
										
										default:
										$modal="<img src=\"css/images/lupa_obs.png\" title=\"Observação: ".$linha[15]."\" style=\"width: 20px;height: 20px;\">";
										
										break;
										
									}
									if (empty($linha[2])) {
										$linha[2]="ND";
									}
									?>

									<tr>
										
										<td style="text-align: center;"> <?php echo $dataBR; ?> </td>
										<td style="text-align: center;"> <?php echo $linha[1]; ?> </td>
										<td style="text-align: center;"> <?php echo $linha[2]; ?> </td>
										<td style="text-align: center;"> <?php echo $linha[7]; ?> </td>
										<td style="text-align: center;"> <?php echo $linha[16]; ?> </td>
										<td style="text-align: center;"> <?php echo $linha[8];?> </td>
										<td style="text-align: center;"> <?php echo $aula; ?> </td>
										<td style="text-align: center;"> <?php echo $linha[4];?> </td>
										<td style="text-align: center;"> <?php echo $array["Nome"]; ?> </td>
										<td style="text-align: center;"> <?php echo $linha[14]; ?> </td>
										<td style="text-align: center;"> <?php echo $linha[13]; ?> </td>
										<td style="text-align: center;"> <?php echo $linha[12]; ?> </td>
										<td style="text-align: center;"> <?php echo $modal; ?> </td>
										<td style="text-align: center;color:<?php echo $cor;?>"> <?php echo $linha[17]; ?> </td>
										
									</tr>	
								</tbody>	
							</table>
							<?php } ?>	
							<br/>

						</div>
						<div class=" w3-panel w3-center w3-small">
							<div class="w3-bar w3-round w3-border" >
								<?php
						//Apresentar a paginação
								for($i = 1; $i <= $totalPagina; $i++){ 
									if($i == $pagina){ ?>
										<a class="w3-button w3-bar-item w3-blue"><b><?php echo $i; ?></b></a>
										<?php }else{ ?>
											<a href="?pagina=<?php echo $i;?>" class="w3-button w3-bar-item" ><?php echo $i; ?></a>
											<?php } 	  
										}   ?>
									</div>
								</div>
								<?php } ?>
								<br/>	
								<br/>

							</body>

							</html>