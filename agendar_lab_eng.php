<?php
include("includes/header_agendar_eng.php");
header('Content-Type: text/html; charset=utf-8');

if(isset($_GET['inicial']) && $_GET['inicial'] == 'S' && isset($_SESSION['dado_eng'])){
	unset($_SESSION['dado_eng']);
}

if(isset($_POST['dataAg']) AND isset($_POST['periodoSelect'])){
	$grava=$_POST['dataAg'];
	$vSelect=$_POST['periodoSelect'];
}else{
	$grava="";
	$vSelect="";
	
}

 ?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<!-- JQuery DATAPICKER -->
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
  <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
  <!-- aqui termina o JQuery -->
	<!--*************** BOOTSTRAP CSS ***************-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/saude.css">

	<!--- JS Bootstrap --->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
	
	<style type="text/css">
	.change_disci{
		color:#ff0000;
		display: none;
	}
	.change_turma{
		color:#ff0000;
		display: none;
	}
</style>
</head>
<script LANGUAGE="JavaScript">

  function Verificar()
  {
   document.agendaEng.action="?acao=verificar";
   document.forms.agendaEng.submit();

 }
 function Agendar()
 {
   document.agendaEng.action="?acao=agendar";
   document.forms.agendaEng.submit();
   Carregando.style.display='block';
   seldata.style.display='none';
 }

 $(function() {
  $("#idDataAg").datepicker({
    dateFormat: 'dd/mm/yy',
    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
    minDate:"+0", maxDate: "+7M"

  });
});

</script>

<body>
	<?php if ($grava==''){?>
    <br/>
	<div id="seldata" class="container-fluid text-center">
		<header>
			<div class="row">
				<div class="col-md-12">
					<h1>
						Agendar Laboratório de Eng & Arquitetura
					</h1>
				</div>
			</div>
		</header>
        <br/>

        <div class="container alert alert-danger alert-dismissible fade show" style="<?php echo $display;?>" role="alert">
        	<small><?php echo $msg;?></small>
        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        		<span aria-hidden="true">&times;</span>
        	</button>
        </div>

		<div class="row">
			<section class="col-md-6 form-container border rounded border-primary offset-md-3 div-teste">
				<form class="text-center" name="agendaEng" method="post">
					<br/>
					<div class="row">
						<div class="col-xs-6 col-md-6">
							<legend class="text-left"><h5>Etapa 1:</h5></legend>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<label><b><rem style="color:red">*</rem>Data:</b></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<input type="text" placeholder="Dia/Mês/Ano" class="form-control text-center" name="dataAg" id="idDataAg" required />
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-12">
							<label><b><rem style="color:red">*</rem>Turno:</b></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<select required name="periodoSelect" class="form-control" >
								<option  selected value="">Selecione um turno..</option>
								<option value="Manha">Matutino</option>
								<option value="Noite">Noturno</option>
							</select>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<button  class="btn btn-primary" value="Pesquisar" onclick="Verificar()">Pesquisar</button>
						</div>
					</div>
					<br/>
					<div class="row text-right">
						<div class="col-md-12">
							<small><rem style="color:red"><b>*</b></rem> Obrigatório</small>
						</div>
					</div>
				</form>
			</section>
		</div>
		<br/>
	</div>

<?php }else{
	
	if($vSelect=="Manha"){
		$turn="M";
	}else{
		$turn="N";
	}
	
	?>
  <div id="Carregando" class="windows8" style="background-color: #fff; opacity: 0.5;">
    <div class="wBall" id="wBall_1">
      <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_2">
      <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_3">
      <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_4">
      <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_5">
      <div class="wInnerBall"></div>
    </div>
  </div>

	<div id="seldata" class="container-fluid text-center">
		<header>
			<div class="row">
				<div class="col-md-12">
					<h1>
						Agendar Laboratório de Eng & Arquitetura
					</h1>
				</div>
			</div>
		</header>
		<br/>

		<div class="container alert alert-danger alert-dismissible fade show" style="<?php echo $display;?>" role="alert">
        	<small><?php echo $msg;?></small>
        	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        		<span aria-hidden="true">&times;</span>
        	</button>
        </div>

        <div class="row text-left">
			<section class="col-md-6 form-container border rounded border-success offset-md-3">
				<form class="" name="agendaEng" method="post">
					<br/>
					<div class="row">
						<div class="col-xs-6 col-md-6">
							<legend class="text-left"><h5>Etapa 2:</h5></legend>
						</div>
						<div class="col-xs-6 col-md-6">
							<a href="agendar_lab_eng.php?inicial=S" class="btn-link float-right">Voltar</a>
						</div>
					</div>
					<div class="row">
						<div class="offset-md-2 col-md-4">
							<label><b><rem style="color:red">*</rem>Data:</b></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<input readonly style="background-color: #ddffdd;" value="<?php echo $grava ?>" type="text" class="form-control text-center border-success" name="dataAg"/>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="offset-md-2 col-md-4">
							<label><b><rem style="color:red">*</rem>Turno:</b></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 offset-md-2">
						<input readonly style="background-color: #ddffdd;" value="<?php echo $vSelect ?>" type="text" class="form-control text-center border-success" name="periodoSelect"/>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="offset-md-2 col-md-4">
							<label><b><rem style="color:red">*</rem>Quantidade de Alunos:</b></label>
						</div>
					</div>
					<div class="row">
						<div class="offset-md-2 col-md-6">
							<div class="form-check-inline">
								<input type="radio" class="form-check-input" name="cQuant" id="1" value="Até 30">
								<label class="form-check-label" for="1"> Até 30 </label>
							</div>
							<div class="form-check-inline">
								<input type="radio" class="form-check-input" name="cQuant" id="2" value="30 a 50">
								<label class="form-check-label" for="2"> 30 á 50 </label>
							</div>
							<div class="form-check-inline">
								<input type="radio" class="form-check-input" name="cQuant" id="3" value="Mais que 50">
								<label class="form-check-label" for="3"> 50+</label>
							</div>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="offset-md-2 col-md-4">
							<label><b><rem style="color:red">*</rem>Tempo de Aula:</b></label>
						</div>
					</div>
					<?php 
						if ($vSelect=="Manha"){
							//Horário de inicio Matutino
							$horaInicio[0]="08:20";
							$horaInicio[1]="09:10";
							$horaInicio[2]="10:20";
							$horaInicio[3]="11:10";
							//Horário de Final Matutino
							$horaFinal[0]="09:10";
							$horaFinal[1]="10:00";
							$horaFinal[2]="11:10";
							$horaFinal[3]="12:00";
						}else{
							//Horário inicio Noturno
							$horaInicio[0]="19:10";
							$horaInicio[1]="20:00";
							$horaInicio[2]="21:10";
							$horaInicio[3]="22:00";
							//Horário de Final Noturno
							$horaFinal[0]="20:00";
							$horaFinal[1]="20:50";
							$horaFinal[2]="22:00";
							$horaFinal[3]="22:50";
						}
					?>
					<div class="form-row">
						<div class="form-group col-md-2 offset-md-2">
							<select name="cQaula[]" class="form-control" required>
								<option selected value="">Inicio..</option>
								<option value="<?php echo $horaInicio[0];?>"><?php echo $horaInicio[0];?></option>
								<option value="<?php echo $horaInicio[1];?>"><?php echo $horaInicio[1];?></option>
								<option value="<?php echo $horaInicio[2];?>"><?php echo $horaInicio[2];?></option>
								<option value="<?php echo $horaInicio[3];?>"><?php echo $horaInicio[3];?></option>
							</select>
						</div>
						<label>Até</label>
						<div class="form-group col-md-2">
							<select name="cQaula[]" class="form-control" required>
								<option selected value="">Fim..</option>
								<option value="<?php echo $horaFinal[0];?>"><?php echo $horaFinal[0];?></option>
								<option value="<?php echo $horaFinal[1];?>"><?php echo $horaFinal[1];?></option>
								<option value="<?php echo $horaFinal[2];?>"><?php echo $horaFinal[2];?></option>
								<option value="<?php echo $horaFinal[3];?>"><?php echo $horaFinal[3];?></option>
							</select>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="offset-md-2 col-md-4">
							<label><rem style="color:red">*</rem><b>Agendar para:</b></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<select name="selectUser" class="form-control" required>
								<option selected value="">Selecione o professor..</option>
							<?php
							$query_users=mysql_query("SELECT * FROM usuarios WHERE nivel <=3 ORDER BY Nome");
							while($row_user_post = mysql_fetch_assoc($query_users) ) {
								echo '<option value="'.$row_user_post['id'].'">'.ucwords($row_user_post['Nome']).' '.ucwords($row_user_post['Sobrenome']).'</option>';
							} 
							?>
							</select>
						</div>
					</div>
					<br/>
 <?php  if (isset($_SESSION["dado_eng"])){ ?>
					<div class="row">
						<div class="offset-md-2 col-md-4">
							<label><b><rem style="color:red">*</rem>Laboratório:</b></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<select name="selectLab" class="form-control" required>
								<option selected value="">Escolha um Laboratório..</option>
									<?php foreach($_SESSION["dado_eng"] as &$lab_eng) {
										echo '<option value="'.$lab_eng[1].'">'.$lab_eng[1].' - '.$lab_eng[2].' Alunos</option>'; 
									}?>
							</select>
						</div>
					</div>
					<?php  } ?>
					<br/>
					<div class="row">
						<div class="offset-md-2 col-md-4">
							<label><b><rem style="color:red">*</rem>Curso:</b></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<select name="selectCurso" id="selectCurso" class="form-control" required>
								<option selected value="">Selecione o curso..</option>
									<?php
									$query_cursos=mysql_query("SELECT * FROM cursos WHERE turno='$turn' ORDER BY curso ASC");
									while($row_cat_post = mysql_fetch_assoc($query_cursos) ) {
										echo '<option value="'.$row_cat_post['cod_curso'].'">'.$row_cat_post['curso'].'</option>';
									} 
									?>
							</select>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="offset-md-2 col-md-4">
							<label><b><rem style="color:red">*</rem>Disciplina:</b></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<span class="change_disci">Sem disciplinas cadastradas para esse curso.</span>
							<select name="selectDisci" id="selectDisci" class="form-control" required>
								<option disabled selected value="">Escolha o Curso.. </option>
							</select>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="offset-md-2 col-md-4">
							<label for="selectTurma"><b><rem style="color:red">*</rem>Turmas:</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<span class="change_turma">Sem Turma cadastradas para essa disciplina.</span>
							<select multiple name="selectTurma[]" class="form-control" id="selectTurma">
								<option disabled value="">Escolha a Disciplina..</option>
							</select>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="offset-md-2 col-md-4">
							<label><b>Materiais:</b></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 offset-md-2">
							<textarea class="form-control" name="observacao" id="observacao" placeholder="Descreva com clareza os materiais necessários para a Aula" rows="5" cols="35" maxlength="500" name="observacao"></textarea><small class="caracteres"></small><br>
						</div>
					</div>
					<br/>
					<div class="row text-center">
						<div class="col-md-8 offset-md-2">
							<input type="button" class="btn btn-primary" value="Agendar" onclick="Agendar()" />
						</div>
					</div>

					<br/>
					<div class="row text-right">
						<div class="col-md-12">
							<small><rem style="color:red"><b>*</b></rem> Campo Obrigatório</small>
						</div>
						
					</div>
				</form>
			</section>
		</div>
		<br/>

	</div>
	
	<script type="text/javascript">
		$(function(){
			$('#selectCurso').change(function(){
				if( $(this).val() ) {
					$('#selectDisci').hide();
					$('.change_disci').show();
					$.getJSON('disciplina_post.php?search=',{selectCurso: $(this).val(), ajax: 'true'}, function(j){
						var options = '<option disabled selected>Cód. Disciplina - Disciplina</option>'; 
						for (var i = 0; i < j.length; i++) {
							options += '<option value="' + j[i].id + '">' + j[i].id +' - '+ j[i].nome_sub_categoria+'</option>';
						} 
						$('#selectDisci').html(options).show();
						$('.change_disci').hide();
					});
				} else {
					$('#selectDisci').html('<option disabled>Sem Disciplina Cadastrada</option>');
				}
			});
		});


		$(function(){
			$('#selectDisci').change(function(){
				if( $(this).val() ) {
					$('#selectTurma').hide();
					$('.change_turma').show();
					$.getJSON('turma_post.php?search=',{selectDisci: $(this).val(), ajax: 'true'}, function(t){
						var option = '<option disabled>Selecione as Turmas</option>'; 
						for (var n = 0; n < t.length; n++) {
							option += '<option value="'+ t[n].turma +'">'+ t[n].turma +'</option>';
						} 
						$('#selectTurma').html(option).show();
						$('.change_turma').hide();
					});
				} else {
					$('#selectTurma').html('<option disabled>Sem Turma cadastrada..</option>');
				}
			});
		});
	</script>

<?php } ?>


<script type="text/javascript">
$(document).on("input", "#observacao", function() {
        var limite = 1000;
        var informativo = "caracteres restantes.";
        var caracteresDigitados = $(this).val().length;
        var caracteresRestantes = limite - caracteresDigitados;

        if (caracteresRestantes <= 0) {
            var observacao = $("textarea[name=observacao]").val();
            $("textarea[name=observacao]").val(observacao.substr(0, limite));
            $(".caracteres").text("0 " + informativo);
        } else {
            $(".caracteres").text(caracteresRestantes + " " + informativo);
        }
    });
  </script>
</body>
</html>