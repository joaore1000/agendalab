<?php
      include("includes/header_config.php");



      $pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1; 
      $sqlTotal="SELECT id_cood,Curso,Nome,sigla_curso,status FROM coordenadores ORDER BY Curso";
      $qrTotal = mysql_query($sqlTotal) or die (mysql_error());
      $numTotal= mysql_num_rows($qrTotal);
      $qtn = 10;
      $totalPagina= ceil($numTotal/$qtn);
      $inicio = ($qtn * $pagina) - $qtn;
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<style type="text/css">
    body{
        background-color: white;
    }
</style>
</head>
<body>
    <br/>
<div class="w3-container">
    <div class="w3-container">
        <img src="css/images/cursos.png" class="w3-left" width="10%" height="10%">
       <h2>Cursos</h2>
       <p>Configuração de novos cursos, coordenadores do curso, cursos ativos e inativos..</p>
       <hr>
    </div>
    
   <div name="menu" class="w3-container">
        <a href="novo_curso.php" class="btn btn-primary w3-right"><span class="glyphicon glyphicon-plus-sign"></span> Novo</a>
    </div>
    <br/>
    <div class="w3-container">
        <!-- AQUI VAI A TABELA COM OS LABORATORIOS  E COM MENU DE FECHAR E ABRIR, DESATIVAR E ATIVAR. -->
         <table class="w3-table w3-striped w3-hoverable w3-bordered w3-centered"> 
               <tr class="w3-white" >
                   <th>#</th>
                   <th>Curso</th>
                   <th>Coordenador(a)</th>
                   <th>Abreviação</th>
                   <th>Status</th>
                   <th>Ação</th>
               </tr>
               <?php
               $query_select=mysql_query("SELECT id_cood,Curso,Nome,sigla_curso,status FROM coordenadores ORDER BY Curso LIMIT $inicio,$qtn");

               if (mysql_num_rows($query_select)  == 0 ) {
                    $msg="Sem Laboratórios cadastrados e configurados. Clique em 'Novo' para adicionar um laboratório.";
                    ?>

                    <div class="message"><?php echo $msg;?></div>
            <?php
                }else{
                    while($row=mysql_fetch_array($query_select)){
                        $id_id=$row["id_cood"];

                        if ($row["status"]=='ATIVO') {
                          $cor="green";
                        }else{
                          $cor="red";
                        }
                        ?>
               <tr>
                   <td><?php echo $id_id;?></td>
                   <td><?php echo $row["Curso"];?></td>
                   <td><?php echo $row["Nome"];?></td>
                   <td><?php echo $row["sigla_curso"];?></td>
                   <td style="color:<?php echo $cor;?>"><?php echo $row["status"];?></td>
                   <td class="buttons">
                       <a href="editar_curso.php?numberid=<?php echo $id_id; ?>" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span></a>
                       <a href="#" onclick="javascript: if (confirm('Você realmente deseja excluir este Curso?'))location.href='?acao=remover_curso&amp;numberid=<?php echo $id_id; ?>'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
                   </td>
               </tr>

              <?php
                    }
                } 
               ?>
           </table>
    </div>
    <br/>
    <div class="w3-center">
                <div class="w3-bar w3-round w3-border" >
                    <?php
                        //Apresentar a paginação
                        for($i = 1; $i <= $totalPagina; $i++){ 
                            if($i == $pagina){ ?>
                            <a class="w3-button w3-bar-item w3-blue"><b><?php echo $i; ?></b></a>
                      <?php }else{ ?>
                            <a href="?pagina=<?php echo $i;?>" class="w3-button w3-bar-item" ><?php echo $i; ?></a>
                      <?php }     
                      }   ?>
                </div>
                
            </div>
    <hr>

</div>



	    
</body>
</html>