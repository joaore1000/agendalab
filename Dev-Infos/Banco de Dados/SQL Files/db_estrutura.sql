-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 10-Out-2019 às 21:38
-- Versão do servidor: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_estrutura`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agendadata`
--

CREATE TABLE `agendadata` (
  `ID` int(6) NOT NULL,
  `agendado_por` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sobrenome` varchar(50) NOT NULL,
  `Coordenador` varchar(50) NOT NULL,
  `agendado_para` varchar(50) NOT NULL,
  `Disciplina` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `data` date NOT NULL,
  `periodo` varchar(25) NOT NULL,
  `Quantidade` varchar(25) NOT NULL,
  `Aula` varchar(50) NOT NULL,
  `turmas` varchar(50) NOT NULL,
  `hora_aula` varchar(30) NOT NULL,
  `aula_total` varchar(30) NOT NULL,
  `Mic` varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Som` varchar(3) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Dshow` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_mysql500_ci NOT NULL,
  `Softwares` varchar(200) NOT NULL,
  `Lab` varchar(45) NOT NULL,
  `situation` varchar(45) NOT NULL,
  `chamada` varchar(10) NOT NULL,
  `adicionado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `upload1_foto` varchar(30) NOT NULL,
  `upload2_foto` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `agendadata_eng`
--

CREATE TABLE `agendadata_eng` (
  `ID` int(6) NOT NULL,
  `agendado_por` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sobrenome` varchar(50) NOT NULL,
  `Coordenador` varchar(50) NOT NULL,
  `agendado_para` varchar(50) NOT NULL,
  `Disciplina` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `data` date NOT NULL,
  `periodo` varchar(25) NOT NULL,
  `Quantidade` varchar(25) NOT NULL,
  `Aula` varchar(50) NOT NULL,
  `turmas` varchar(50) NOT NULL,
  `hora_aula` varchar(30) NOT NULL,
  `aula_total` varchar(30) NOT NULL,
  `Softwares` varchar(1000) NOT NULL,
  `Lab` varchar(45) NOT NULL,
  `situation` varchar(45) NOT NULL,
  `chamada` varchar(10) NOT NULL,
  `adicionado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `upload1_foto` varchar(30) NOT NULL,
  `upload2_foto` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `agendadata_saude`
--

CREATE TABLE `agendadata_saude` (
  `ID` int(6) NOT NULL,
  `agendado_por` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sobrenome` varchar(50) NOT NULL,
  `Coordenador` varchar(50) NOT NULL,
  `agendado_para` varchar(50) NOT NULL,
  `Disciplina` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `data` date NOT NULL,
  `periodo` varchar(25) NOT NULL,
  `Quantidade` varchar(25) NOT NULL,
  `Aula` varchar(50) NOT NULL,
  `turmas` varchar(50) NOT NULL,
  `hora_aula` varchar(30) NOT NULL,
  `aula_total` varchar(30) NOT NULL,
  `Softwares` text NOT NULL,
  `Lab` varchar(45) NOT NULL,
  `situation` varchar(45) NOT NULL,
  `chamada` varchar(10) NOT NULL,
  `adicionado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `upload1_foto` varchar(30) NOT NULL,
  `upload2_foto` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `coordenadores`
--

CREATE TABLE `coordenadores` (
  `id_cood` int(6) NOT NULL,
  `Nome` varchar(45) NOT NULL,
  `Sobrenome` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `Curso` varchar(50) NOT NULL,
  `sigla_curso` varchar(45) CHARACTER SET utf16 NOT NULL,
  `status` varchar(45) NOT NULL,
  `chart_id` varchar(30) NOT NULL,
  `hora_total` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos`
--

CREATE TABLE `cursos` (
  `id_curso` int(10) NOT NULL,
  `cod_curso` int(10) NOT NULL,
  `curso` varchar(100) CHARACTER SET utf8 NOT NULL,
  `coordenador_curso` varchar(100) CHARACTER SET utf8 NOT NULL,
  `turno` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `laboratorios`
--

CREATE TABLE `laboratorios` (
  `ID` int(6) NOT NULL,
  `Laboratorio` varchar(45) NOT NULL,
  `Maquinas` varchar(45) NOT NULL,
  `Quantidade` varchar(50) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `acesso` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `lab_eng`
--

CREATE TABLE `lab_eng` (
  `id_eng` int(6) NOT NULL,
  `nome_lab` varchar(50) CHARACTER SET utf8 NOT NULL,
  `capacidade` int(100) NOT NULL,
  `status_eng` varchar(15) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `lab_saude`
--

CREATE TABLE `lab_saude` (
  `id_saude` int(6) NOT NULL,
  `nome_lab` varchar(50) CHARACTER SET utf8 NOT NULL,
  `capacidade` int(100) NOT NULL,
  `status_saude` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `new_disciplina`
--

CREATE TABLE `new_disciplina` (
  `id_disci` int(10) NOT NULL,
  `cod_disci` int(10) NOT NULL,
  `disciplina` varchar(100) NOT NULL,
  `curso` int(50) NOT NULL,
  `turma` varchar(15) CHARACTER SET latin1 NOT NULL,
  `etapa` int(2) NOT NULL,
  `tipo_disci` varchar(30) CHARACTER SET latin1 NOT NULL,
  `situacao_turma` varchar(30) CHARACTER SET latin1 NOT NULL,
  `carga_horaria` varchar(10) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(6) NOT NULL,
  `Nome` varchar(50) NOT NULL,
  `Sobrenome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `Coodernador` varchar(50) NOT NULL,
  `Logon` varchar(25) NOT NULL,
  `Senha` varchar(45) NOT NULL,
  `nivel` int(1) NOT NULL,
  `Estato` varchar(20) NOT NULL,
  `bloqueio` int(4) NOT NULL,
  `bloqueio_total` int(4) NOT NULL,
  `faltas_total` int(6) NOT NULL,
  `acesso` int(1) NOT NULL,
  `adicionado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agendadata`
--
ALTER TABLE `agendadata`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `agendadata_eng`
--
ALTER TABLE `agendadata_eng`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `agendadata_saude`
--
ALTER TABLE `agendadata_saude`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `coordenadores`
--
ALTER TABLE `coordenadores`
  ADD PRIMARY KEY (`id_cood`),
  ADD KEY `id_coord_fk` (`id_cood`);

--
-- Indexes for table `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id_curso`),
  ADD UNIQUE KEY `cod_curso_fk` (`cod_curso`),
  ADD KEY `id_coord_fk` (`coordenador_curso`);

--
-- Indexes for table `laboratorios`
--
ALTER TABLE `laboratorios`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `lab_eng`
--
ALTER TABLE `lab_eng`
  ADD PRIMARY KEY (`id_eng`);

--
-- Indexes for table `lab_saude`
--
ALTER TABLE `lab_saude`
  ADD PRIMARY KEY (`id_saude`);

--
-- Indexes for table `new_disciplina`
--
ALTER TABLE `new_disciplina`
  ADD PRIMARY KEY (`id_disci`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agendadata`
--
ALTER TABLE `agendadata`
  MODIFY `ID` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `agendadata_eng`
--
ALTER TABLE `agendadata_eng`
  MODIFY `ID` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `agendadata_saude`
--
ALTER TABLE `agendadata_saude`
  MODIFY `ID` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coordenadores`
--
ALTER TABLE `coordenadores`
  MODIFY `id_cood` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id_curso` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `laboratorios`
--
ALTER TABLE `laboratorios`
  MODIFY `ID` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lab_eng`
--
ALTER TABLE `lab_eng`
  MODIFY `id_eng` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lab_saude`
--
ALTER TABLE `lab_saude`
  MODIFY `id_saude` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `new_disciplina`
--
ALTER TABLE `new_disciplina`
  MODIFY `id_disci` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
