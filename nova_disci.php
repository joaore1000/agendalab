<?php 
$titlepag="FAC3 - Nova Disciplina";
include("includes/inserir.header.php");

$sql=mysql_query("SELECT * FROM coordenadores WHERE Nome!='' AND status='ATIVO' ORDER BY Curso");
$contar=0;
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title><?php echo $titlepag ?></title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
  <link href="/open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/open-iconic-master/font/css/open-iconic-bootstrap.css"/>



</head>
<style type="text/css">
*{
  margin:0;
  padding: 0;
  outline:none;
  list-style:none;
  font-family: 'Ubuntu',sans-serif;
}
</style>


<?php  while($linha = mysql_fetch_array($sql, MYSQL_NUM)) {
  $coordenador[$contar][0]  = $linha[0];
  $coordenador[$contar][1]  = $linha[1];
  $coordenador[$contar][3]  = $linha[3];
  $coordenador[$contar][4]  = $linha[4];
  $coordenador[$contar][5]  = $linha[5];
  $contar++;
}  ?>

<body>

  <div class="container" name="header" id="">
  	<br/>
    <h2>Nova disciplina</h2>
    <p style="color: red"> * = Campos obrigatórios</p>
    <hr>
  </div>
  <br/>

  <div class="container" name="conteudo-pagina" id="">
    <form action="?acao=inserirDisc" method="post">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputCod">Cód. Disciplina *</label>
          <input type="text" required class="form-control" id="inputCod" name="inputCod" placeholder="Código da disciplina">
        </div>
        <div class="form-group col-md-6">
          <label for="inputCarga">Carga Horária Pratica *</label>
          <input type="text" required class="form-control" id="inputCarga" name="inputCarga" placeholder="Carga horária prática">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputNome" class="col-sm-2 col-form-label">Nome da Disciplina *</label>
        <div class="col-sm-10">
          <input type="text" required class="form-control" id="inputNome" name="inputNome" placeholder="Nome da disciplina">
        </div>
      </div>
     <div class="form-row">
        <div class="form-group col-md-2">
          <label for="selectEnsalado">Ensalado? *</label>
          <select class="form-control" id="selectEnsalado" name="selectEnsalado" onchange="optionCheck()">
            <option value="sim">Sim</option>
            <option value="não" selected>Não</option>
          </select>
        </div>
      </div>

 <div class="form-row">
        <div class="form-group col-md-6" id="selectCurso">
          <label>Curso *</label>
          <select id="InputCurso" required name="InputCurso" class="form-control" >
            <option value="">Escolha o curso..</option>
            <?php
            foreach ($coordenador as $show) {
              ?>
              <option value="<?php echo $show[5];?>"><?php echo $show[4]?></option>
            <?php } ?>
          </select>
        </div>


        <div class="form-group col-auto" id="radioSerie">
          <label>Semestre *</label><br/>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="r1" name="serieradio" class="custom-control-input" value="1" >
            <label class="custom-control-label" for="r1">1º Semestre</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="r2" name="serieradio" class="custom-control-input" value="2">
            <label class="custom-control-label" for="r2">2º Semestre</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="r3" name="serieradio" class="custom-control-input" value="3">
            <label class="custom-control-label" for="r3">3º Semestre</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="r4" name="serieradio" class="custom-control-input" value="4">
            <label class="custom-control-label" for="r4">4º Semestre</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="r5" name="serieradio" class="custom-control-input" value="5">
            <label class="custom-control-label" for="r5">5º Semestre</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="r6" name="serieradio" class="custom-control-input" value="6">
            <label class="custom-control-label" for="r6">6º Semestre</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="r7" name="serieradio" class="custom-control-input" value="7">
            <label class="custom-control-label" for="r7">7º Semestre</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="r8" name="serieradio" class="custom-control-input" value="8">
            <label class="custom-control-label" for="r8">8º Semestre</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="r9" name="serieradio" class="custom-control-input" value="9">
            <label class="custom-control-label" for="r9">9º Semestre</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="r10" name="serieradio" class="custom-control-input" value="0">
            <label class="custom-control-label" for="r10">10º Semestre</label>
          </div>
        </div>



        <div class="form-group col-auto" id="selectSerie" style="display: none;">
          <label>Semestre *</label><br/>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="1" name="serie[]" class="custom-control-input" value="1" >
            <label class="custom-control-label" for="1">1º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="2" name="serie[]" class="custom-control-input" value="2">
            <label class="custom-control-label" for="2">2º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="3" name="serie[]" class="custom-control-input" value="3">
            <label class="custom-control-label" for="3">3º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="4" name="serie[]" class="custom-control-input" value="4">
            <label class="custom-control-label" for="4">4º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="5" name="serie[]" class="custom-control-input" value="5">
            <label class="custom-control-label" for="5">5º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="6" name="serie[]" class="custom-control-input" value="6">
            <label class="custom-control-label" for="6">6º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="7" name="serie[]" class="custom-control-input" value="7">
            <label class="custom-control-label" for="7">7º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="8" name="serie[]" class="custom-control-input" value="8">
            <label class="custom-control-label" for="8">8º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="9" name="serie[]" class="custom-control-input" value="9">
            <label class="custom-control-label" for="9">9º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="10" name="serie[]" class="custom-control-input" value="0">
            <label class="custom-control-label" for="10">10º Semestre</label>
          </div>
        </div>
</div>
<div class="form-row" id="cursoEnsalado2" style="display: none;">
  <hr>
        <div class="form-group col-md-6">
          <label id="labelCurso">• Curso 2</label>
          <select id="InputCurso2" name="InputCurso2" class="form-control" >
            <option value="">Escolha o 2º curso..</option>
            <?php
            foreach ($coordenador as $show) {
              ?>
              <option value="<?php echo $show[5];?>"><?php echo $show[4]?></option>
            <?php } ?>
          </select>
        </div>

         <label for="checkSerie">Semestre 2</label>
        <div  class="form-group col-auto" >
         
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="serie1" name="serie2[]" class="custom-control-input" value="1" >
            <label class="custom-control-label" for="serie1">1º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="serie2" name="serie2[]" class="custom-control-input" value="2">
            <label class="custom-control-label" for="serie2">2º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="serie3" name="serie2[]" class="custom-control-input" value="3">
            <label class="custom-control-label" for="serie3">3º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="serie4" name="serie2[]" class="custom-control-input" value="4">
            <label class="custom-control-label" for="serie4">4º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="serie5" name="serie2[]" class="custom-control-input" value="5">
            <label class="custom-control-label" for="serie5">5º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="serie6" name="serie2[]" class="custom-control-input" value="6">
            <label class="custom-control-label" for="serie6">6º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="serie7" name="serie2[]" class="custom-control-input" value="7">
            <label class="custom-control-label" for="serie7">7º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="serie8" name="serie2[]" class="custom-control-input" value="8">
            <label class="custom-control-label" for="serie8">8º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="serie9" name="serie2[]" class="custom-control-input" value="9">
            <label class="custom-control-label" for="serie9">9º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="serie10" name="serie2[]" class="custom-control-input" value="0">
            <label class="custom-control-label" for="serie10">10º Semestre</label>
          </div>
        </div>
</div>
<div class="form-row" id="cursoEnsalado3" style="display: none;">
  <hr>
        <div class="form-group col-md-6" >
          <label id="labelCurso">• Curso 3</label>
          <select id="InputCurso" name="InputCurso3" class="form-control" >
            <option value="">Escolha o curso..</option>
            <?php
            foreach ($coordenador as $show) {
              ?>
              <option value="<?php echo $show[5];?>"><?php echo $show[4]?></option>
            <?php } ?>
          </select>
        </div>

        <label for="checkSerie">Semestre 3</label>
        <div  class="form-group col-auto" >
          
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="1-3" name="serie3[]" class="custom-control-input" value="1" >
            <label class="custom-control-label" for="1-3">1º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="2-3" name="serie3[]" class="custom-control-input" value="2">
            <label class="custom-control-label" for="2-3">2º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="3-3" name="serie3[]" class="custom-control-input" value="3">
            <label class="custom-control-label" for="3-3">3º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="4-3" name="serie3[]" class="custom-control-input" value="4">
            <label class="custom-control-label" for="4-3">4º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="5-3" name="serie3[]" class="custom-control-input" value="5">
            <label class="custom-control-label" for="5-3">5º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="6-3" name="serie3[]" class="custom-control-input" value="6">
            <label class="custom-control-label" for="6-3">6º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="7-3" name="serie3[]" class="custom-control-input" value="7">
            <label class="custom-control-label" for="7-3">7º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="8-3" name="serie3[]" class="custom-control-input" value="8">
            <label class="custom-control-label" for="8-3">8º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="9-3" name="serie3[]" class="custom-control-input" value="9">
            <label class="custom-control-label" for="9-3">9º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="10-3" name="serie3[]" class="custom-control-input" value="0">
            <label class="custom-control-label" for="10-3">10º Semestre</label>
          </div>
        </div>
</div>

<div class="form-row" id="cursoEnsalado4" style="display: none;">
  <hr>
        <div class="form-group col-md-6" >
          <label id="labelCurso">• Curso 4</label>
          <select id="InputCurso" name="InputCurso4" class="form-control" >
            <option value="">Escolha o curso..</option>
            <?php
            foreach ($coordenador as $show) {
              ?>
              <option value="<?php echo $show[5];?>"><?php echo $show[4]?></option>
            <?php } ?>
          </select>
        </div>

         <label for="checkSerie">Semestre 4</label>
        <div class="form-group col-auto" >
         
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="1-4" name="serie4[]" class="custom-control-input" value="1" >
            <label class="custom-control-label" for="1-4">1º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="2-4" name="serie4[]" class="custom-control-input" value="2">
            <label class="custom-control-label" for="2-4">2º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="3-4" name="serie4[]" class="custom-control-input" value="3">
            <label class="custom-control-label" for="3-4">3º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="4-4" name="serie4[]" class="custom-control-input" value="4">
            <label class="custom-control-label" for="4-4">4º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="5-4" name="serie4[]" class="custom-control-input" value="5">
            <label class="custom-control-label" for="5-4">5º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="6-4" name="serie4[]" class="custom-control-input" value="6">
            <label class="custom-control-label" for="6-4">6º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="7-4" name="serie4[]" class="custom-control-input" value="7">
            <label class="custom-control-label" for="7-4">7º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="8-4" name="serie4[]" class="custom-control-input" value="8">
            <label class="custom-control-label" for="8-4">8º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="9-4" name="serie4[]" class="custom-control-input" value="9">
            <label class="custom-control-label" for="9-4">9º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="10-4" name="serie4[]" class="custom-control-input" value="0">
            <label class="custom-control-label" for="10-4">10º Semestre</label>
          </div>
        </div>
</div>

<div class="form-row" id="cursoEnsalado5" style="display: none;">
<hr>
        <div class="form-group col-md-6">
          <label id="labelCurso">• Curso 5</label>
          <select id="InputCurso5" name="InputCurso5" class="form-control" >
            <option value="">Escolha o curso..</option>
            <?php
            foreach ($coordenador as $show) {
              ?>
              <option value="<?php echo $show[5];?>"><?php echo $show[4]?></option>
            <?php } ?>
          </select>
        </div>

        <label for="checkSerie">Semestre 5</label>
        <div class="form-group col-auto" >
          
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="1-5" name="serie5[]" class="custom-control-input" value="1" >
            <label class="custom-control-label" for="1-5">1º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="2-5" name="serie5[]" class="custom-control-input" value="2">
            <label class="custom-control-label" for="2-5">2º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="3-5" name="serie5[]" class="custom-control-input" value="3">
            <label class="custom-control-label" for="3-5">3º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="4-5" name="serie5[]" class="custom-control-input" value="4">
            <label class="custom-control-label" for="4-5">4º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="5-5" name="serie5[]" class="custom-control-input" value="5">
            <label class="custom-control-label" for="5-5">5º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="6-5" name="serie5[]" class="custom-control-input" value="6">
            <label class="custom-control-label" for="6-5">6º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="7-5" name="serie5[]" class="custom-control-input" value="7">
            <label class="custom-control-label" for="7-5">7º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="8-5" name="serie5[]" class="custom-control-input" value="8">
            <label class="custom-control-label" for="8-5">8º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="9-5" name="serie5[]" class="custom-control-input" value="9">
            <label class="custom-control-label" for="9-5">9º Semestre</label>
          </div>
          <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" id="10-5" name="serie5[]" class="custom-control-input" value="0">
            <label class="custom-control-label" for="10-5">10º Semestre</label>
          </div>
        </div>
<hr>
</div>
      








      








      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="selectTurma">Turma *</label>
          <select class="form-control" id="selectTurma" name="selectTurma">
            <option value="A">A</option>
            <option value="B">B</option>
          </select>
        </div>
        
      </div>


      <fieldset class="form-group">
        <div class="row">
          <legend class="col-form-label col-sm-2 pt-0">Turno *</legend>
          <div class="col-sm-10">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="gridRadios" id="gridDiurno" value="diurno" checked>
              <label class="form-check-label" for="gridDiurno">
                Diurno
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="gridRadios" id="gridNoturno" value="noturno">
              <label class="form-check-label" for="gridNoturno">
                Noturno
              </label>
            </div>
          </div>
        </div>
      </fieldset>
      <fieldset class="form-group">
        <div class="row">
          <legend class="col-form-label col-sm-2 pt-0">Modalidade *</legend>
          <div class="col-sm-10">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="gridMods" id="gridEad" value="EAD" checked>
              <label class="form-check-label" for="gridEad">
                EAD
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="gridMods" id="gridPresencial" checked value="Presencial">
              <label class="form-check-label" for="gridPresencial">
                Presencial
              </label>
            </div>
          </div>
        </div>
      </fieldset>
      <div class="form-group row">
        <div class="col-sm-10">
          <button type="submit" class="btn float-right btn-primary btn-lg">Salvar</button>
        </div>
      </div>
    </form>
  </div>



  <script type="text/javascript">
    function optionCheck(){
      var option = document.getElementById("selectEnsalado").value;
      if(option == "sim"){
      
      document.getElementById("selectSerie").style.display ="block";
      document.getElementById("cursoEnsalado2").style.display ="block";
      document.getElementById("cursoEnsalado3").style.display ="block";
      document.getElementById("cursoEnsalado4").style.display ="block";
      document.getElementById("cursoEnsalado5").style.display ="block";
      document.getElementById("radioSerie").style.display ="none";
          
        
      }
      else{
      document.getElementById("selectSerie").style.display ="none";
      document.getElementById("cursoEnsalado2").style.display ="none";
      document.getElementById("cursoEnsalado3").style.display ="none";
      document.getElementById("cursoEnsalado4").style.display ="none";
      document.getElementById("cursoEnsalado5").style.display ="none";
      document.getElementById("radioSerie").style.display ="block";
       
      }
    }
  </script>





  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>