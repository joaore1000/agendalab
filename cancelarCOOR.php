<?php
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
include("includes/headercancel.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Quadro de Avisos</title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<script type="text/javascript">
	function Cancelar()
   {
     
     fountainG.style.display='block';
     
   }
	
</script>
</head>
<body>
    <h1 style="text-align: center; padding-top: 50px">Cancelar Agendamento</h1>
    <h4 style="text-align: center; color: red">ATENÇÃO - Só é possivel cancelar o agendamento no intervalo acima de 24 horas!</h4>
	 <div id="tValidar" class="w3-container w3-border w3-round-xlarge w3-center" style="top:50px">

	 <?php
	 $dataATU=date("Y-m-d");
	$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1; 
    $sqlTotal="SELECT * FROM agendadata WHERE email='$usuario' AND data>='$dataATU' AND Coordenador= ORDER BY data ASC";
    $qrTotal = mysql_query($sqlTotal) or die (mysql_error());
    $numTotal= mysql_num_rows($qrTotal);
    $qtn = 10;
    $totalPagina= ceil($numTotal/$qtn);
    $inicio = ($qtn * $pagina) - $qtn;
?>

<div class="w3-center">
				<div class="w3-bar w3-round w3-border" >
					<?php
						//Apresentar a paginação
						for($i = 1; $i <= $totalPagina; $i++){ 
							if($i == $pagina){ ?>
							<a class="w3-button w3-bar-item w3-blue"><b><?php echo $i; ?></b></a>
				      <?php }else{ ?>
							<a href="?pagina=<?php echo $i;?>" class="w3-button w3-bar-item" ><?php echo $i; ?></a>
					  <?php } 	  
					  }   ?>
				</div>
			</div>

<div id="tValidar" class="w3-container w3-border w3-round-xlarge w3-center">

 <div id="fountainG" style="display: none;">
	<div id="fountainG_1" class="fountainG"></div>
	<div id="fountainG_2" class="fountainG"></div>
	<div id="fountainG_3" class="fountainG"></div>
	<div id="fountainG_4" class="fountainG"></div>
	<div id="fountainG_5" class="fountainG"></div>
	<div id="fountainG_6" class="fountainG"></div>
	<div id="fountainG_7" class="fountainG"></div>
	<div id="fountainG_8" class="fountainG"></div>
 </div>

  Resultados: <b><?php echo $numTotal;?></b> Agendamentos - Mostrando 10 por página
	    <table width="100%" class="w3-table-all w3-hoverable w3-small w3-card-4" border="1">
	    
	<tr>
	    <th style="text-align: center">ID</th>
		<th style="text-align: center">Data</th>
		<th style="text-align: center">Laboratório</th>
		<th style="text-align: center">Nome</th>
		<th style="text-align: center">Turno</th>
		<th style="text-align: center">Curso</th>
		<th style="text-align: center">Disciplina</th>
		<th style="text-align: center">Intervalo</th>
		<th style="text-align: center">Ação</th>
	</tr>
	    <?php 
	$todosagendamentos=mysql_query("SELECT * FROM agendadata WHERE email='$usuario' AND data>='$dataATU' ORDER BY data ASC LIMIT $inicio,$qtn");
if(mysql_num_rows($todosagendamentos)==0){
	$msg="Nenhum agendamento Encontrado!";
	?>
	<div class="w3-container w3-pale-red w3-display-container"><?php echo $msg ;?></div>
	<?php
}else{
	while($linha=mysql_fetch_array($todosagendamentos)){ 
		$aula=$linha["Aula"];
	    $disciplina=mysql_query("SELECT * FROM disciplina WHERE id_disc='$aula'") or die(mysql_error());
        $nome_disciplina=mysql_fetch_assoc($disciplina);
		$id=$linha["ID"];
           
           $data = $linha["data"];
		   $dataDiv = explode('-', $data);
           $dataBR = $dataDiv[2].'/'.$dataDiv[1].'/'.$dataDiv[0];

           if ($linha["periodo"]=="Noite") {
           	$dateAG=$data." 19:20:00";

           }else{
           	$dateAG=$data." 08:20:00";
           }
        $datatime1= new DateTime("now");
        $datatime2= new DateTime($dateAG);
       
       $intervalo= $datatime1->diff($datatime2);
       $horas= $intervalo->h + ($intervalo->days * 24);

		?>
	<tr>
	    <td style="text-align: center"> <?php echo $linha["ID"];?> </td>
		<td style="text-align: center"> <?php echo $dataBR;?></td>
		<td style="text-align: center"> <?php echo $linha["Lab"];?> </td>
		<td style="text-align: center"> <?php echo $linha["Nome"];?> </td>
		<td style="text-align: center"> <?php echo $linha["periodo"];?> </td>
		<td style="text-align: center"> <?php echo $linha["Disciplina"]?></td>
		<td style="text-align: center"> <?php echo $aula;?> </td>
		<td style="text-align: center"> <?php if($dateAG>date("Y-m-d H:i:s") and $horas>=0){ echo $horas." horas";} else {echo "----";} ?></td>
		<td style="text-align: center"> <?php if($dateAG>date("Y-m-d H:i:s") and $horas>=24 && ($linha["situation"]=='RESERVADO' || $linha["situation"]=='AGUARDANDO')){ echo "<a href=\"?acao=cancelar&amp;number=$id;\" class=\"w3-btn w3-blue\" onClick=\"Cancelar()\">Cancelar</a>";} elseif ($linha["situation"]=='CANCELADO') {echo "<p style=\"color:red\"><b>CANCELADO<b></p>";} else {echo "--------";}  ?> </td>
	</tr>
	<?php  } } ?>
        </table>
        
	 </div>
</body>
</html>