<?php 
$titlepag="FAC3 - Agendar Data";
include("includes/headeragenda.php");
header('Content-Type: text/html; charset=utf-8');
if(isset($_GET['inicial']) && $_GET['inicial'] == 'S' && isset($_SESSION['dado'])){
	unset($_SESSION['dado']);
}
if(isset($_POST['cData'])){
  $grava=$_POST['cData'];
  $radio=$_POST['cPeriod'];
}else{
  $grava='';
  $radio='';
}

$query_cursos=mysql_query("SELECT * FROM coordenadores WHERE status='ATIVO' ORDER BY Curso ASC");
$cont_curso=0;


?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title><?php echo $titlepag ?></title>
  <!-- JQuery DATAPICKER -->
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
  <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
  <!-- aqui termina o JQuery -->
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/w3.css"/>

  <!-- 2 FUNÇOES EM JAVASCRIPT PARA O ACTION DO FORMULARIO -->
  <style type="text/css">
  .change{
    color:#ff0000;
    display: none;
  }
</style>

<script LANGUAGE="JavaScript">

  function Verificar()
  {
   document.agendaDATA.action="?acao=verificar";
   document.forms.agendaDATA.submit();

 }
 function Agendar()
 {
   document.agendaDATA.action="?acao=agendar";
   document.forms.agendaDATA.submit();
   Carregando.style.display='block';
   seldata.style.display='none';
 }
 $(function() {
  $("#tData").datepicker({
    dateFormat: 'dd/mm/yy',
    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
    minDate:"+0", maxDate: "+7M"

  });
});

</script>


</head>

<body>

 <!-- USUARIO NAO DIGITOU DATA NEM TURNO -->

 <?php if ($grava==''){?>

  <!-- Container de escolha de data -->
  <div id="seldata" class="w3-container w3-animate-zoom w3-pale-green w3-topbar w3-bottombar w3-border-green w3-rightbar w3-leftbar w3-center" style="display: block; width: 40%; margin-left: 30%; margin-top: 11%">

    <div>
      <h2 style="text-align: center;">Agendar Informática</h2>
      <h6 style="text-align: center;color: red">* Campos Obrigatórios!</h6>
    </div>

    <div class="w3-panel w3-pale-red w3-leftbar w3-rightbar w3-border-red" style="<?php echo $display;?>"><?php echo $msg;?></div>

    <!--- Formulario de Agendamento -->
    <form name="agendaDATA" method="post">

     <!-- Formulario de verificação de LAbs disponivel -->
     <fieldset id="verificacao" class="w3-border w3-center w3-round"><legend style="text-align: center;"><strong>Verificar se há laboratórios disponível</strong> </legend>

      <!--INPUT DATE PARA SELECIONAR A DATA-->
      <label for="tData" class="w3-left-align"><p><strong>* Selecione a Data:</strong></label></p>
      <input style="text-align: center; font-size: 16px;margin-left: 17% "  class="w3-input w3-center w3-twothird w3-border" type="text" id="tData" name="cData"  placeholder=" Dia/Mês/Ano " required value="<?php echo $grava ?>" />

      <br/><br/>

      <!-- RADIOBUTTONS PARA ESCOLHER PERIODO NOTURNO OU DIURNO -->
      <p class="w3-left-align"><strong>* Selecione o Turno</strong></p>
      <p class="w3-center">
        <input style="margin-left: 6px;" checked type="radio" id="tNoit" name="cPeriod" class="w3-radio" value="Noite"/><label for="tNoit"> Noite</label>
        <input type="radio" id="tManh"  name="cPeriod" class="w3-radio" value="Manha"/><label for="tManh"> Manhã</label><br>
      </p>

      <!-- BOTÃO COM SCPRIT QUE ENVIA ACTION VERIFICAR PARA O HEADERAGENDA -->     
      <input type="submit" class="w3-btn w3-blue w3-margin" value="Verificar" style="margin-left: 15px; margin-bottom: 10px" onclick="Verificar()" />

    </fieldset><br>
  </form>
</div>
<br/><br/><br/><br/>
<!--Grava data digitada pelo usuario -->
<?php }else{?>
<!-- Loading -->
  <div id="Carregando" class="windows8" style="background-color: #fff; opacity: 0.5;">
    <div class="wBall" id="wBall_1">
      <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_2">
      <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_3">
      <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_4">
      <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_5">
      <div class="wInnerBall"></div>
    </div>
  </div>

  <div id="seldata" class="w3-container w3-animate-zoom w3-pale-green w3-topbar w3-bottombar w3-border-green w3-rightbar w3-leftbar w3-center" style="display: block; width: 40%; margin-left: 30%; margin-top:11%">
    <div class="w3-left-align">
      <a href="agendar_dia.php?inicial=S" class="w3-btn w3-card-4 w3-blue w3-left">Voltar</a>
    </div>
    <div>
      <h2 class="w3-center" style="margin-right: 18%">Agendar Data</h2>
      <h6 class="w3-center w3-text-red">* Campos Obrigatórios!</h6>
    </div>
    <div class="message" style="<?php echo $display;?>"><?php echo $msg;?></div>
    <!--- Formulario de Agendamento -->
    <form name="agendaDATA" method="post" class="w3-center">
     <!-- Formulario de verificação de LAbs disponivel -->
     <fieldset><legend><strong>Data e Turno</strong> </legend>
      <!--INPUT PARA SELECIONAR A DATA-->
      <label for="tData"><strong>Data Selecionada:</strong></label><br/>
      <input  readonly  class="w3-input w3-gray w3-center w3-twothird w3-border" type="text" id="tData2" name="cData"  placeholder=" Dia/Mês/Ano " value="<?php echo $grava ?>" style="margin-left: 17%" />
      <br/><br/>
      <!-- RADIOBUTTONS PARA ESCOLHER PERIODO NOTURNO OU DIURNO -->

      <strong>Turno Selecionado:</strong>
      <input type="text" id="tManh" readonly name="cPeriod" value="<?php echo $radio ?>" class="w3-input w3-gray w3-center w3-twothird w3-border" style="margin-left:17%"/><br>
      <br/>
    </fieldset><br>

    <!--TERMINA FORMULARIO DE VERIFICAÇÃO DO LAB -->
    <!--COMEÇA O PREENCHIMENTO DA 2ª PARTE DO FORMULARIO -->
    <!-- INPUT TEXT PARA QUANTIDADE DE ALUNOS QUE VAI TER (ESTIMATIVA) --> 
    <fieldset  class="bradius"><legend style="text-align: center;"><b> Quantidade</b></legend>

      <label for="tQuant" class="w3-left-align"><p><strong>*Alunos:</strong></label>

        <input type="radio" class="w3-radio" name="cQuant" id="1" style="margin-left: 6px;" value="Até 30"><label for="1"> Até 30 </label>
        <input type="radio" class="w3-radio" name="cQuant" id="2" style="margin-left: 6px;" value="30 a 50"><label for="2"> 30 á 50 </label>
        <input type="radio" class="w3-radio" name="cQuant" id="3" style="margin-left: 6px;" value="Mais que 50"><label for="3"> 50+</label>
        <br/><br/>
        <label for="tQaula" class="w3-left-align"><p><strong>*Aulas:</strong></label>

          <input type="radio" class="w3-radio" name="cQaula" id="a1" style="margin-left: 6px;" value="1"><label for="a1"> 1 </label>
          <input type="radio" class="w3-radio" name="cQaula" id="a2" style="margin-left: 6px;" value="2"><label for="a2"> 2 </label>
          <input type="radio" class="w3-radio" name="cQaula" id="a3" style="margin-left: 6px;" value="3"><label for="a3"> 3 </label>
          <input type="radio" class="w3-radio" name="cQaula" id="a4" style="margin-left: 6px;" value="4"><label for="a4"> 4 </label><br/>
        </fieldset><br/>

        <!-- SELECT COM OS NOSSOS LABORATORIOS E O NUMERO DE MAQUINAS DISPONIVEIS EM CADA LAB -->
      <?php  if (isset($_SESSION['dado'])){ ?>
       <label for="tLab" class="w3-left-align"><p><strong>* Laboratório</strong></label></p>
       <select required id="tLab" name="cLab" class="w3-select w3-left">
        <option disabled selected>Escolha um Laboratório..</option>
        <?php foreach($_SESSION['dado'] as &$lab) { ?>
          <option value="<?php echo $lab[0] ?>"><?php echo $lab[0] ?> - <?php echo $lab[2] ?></option>
        <?php } ?>
      </select>
      <br/>


      <!-- SELECT COM OS CURSOS -->
      <label for="tDis" class="w3-left-align"><p><strong>* Curso</strong></label></p>
      <select name="cDis" id="cDis" class="w3-select w3-left">
       <option value="">Escolha o curso..</option>
       <?php
       $query_cursos=mysql_query("SELECT * FROM coordenadores WHERE status='ATIVO' ORDER BY Curso ASC");
       while($row_cat_post = mysql_fetch_assoc($query_cursos) ) {
        echo '<option value="'.$row_cat_post['sigla_curso'].'">'.$row_cat_post['Curso'].'</option>';
      } 
      ?>
      
    </select>
    <br/>


    <!-- SELECT COM AS DISCIPLINAS DE ACORDO COM O CURSO SELECIONADO ACIMA -->
    <label for="tAula" class="w3-left-align"><p><strong>* Disciplina </strong></p>
      <input required style="font-size: 16px;" placeholder="Digite o nome da disciplina" type="text" name="cAula" id="cAula" class="w3-input w3-border">
    </label>
    
    <br/>
    <!-- FIELDSET PARA ESCOLHA DOS EQUIPAMENTOS NECESSARIOS NO LABORATORIO -->
    <fieldset  class="bradius"><legend style="text-align: center;"><b> Selecione os equipamentos </b></legend>
     <input checked type="checkbox" name="cDshow" id="tDatashow" style="margin-left: 6px;" value="Sim" /><label for="tDatashow"> Datashow </label><br>
     <input type="checkbox" name="cMic" id="tMic" style="margin-left: 6px;" value="Sim" /><label for="tMic"> Microfone</label> <br>
     <input type="checkbox" name="cSom" id="tSom" style="margin-left: 6px;" value="Sim"><label for="tSom"> Caixa de Som </label><br>
   </fieldset> <br/>
   <!-- AREA DE TEXTO PARA O USUARIO ESCREVER PROGRAMA QUE DESEjA UTILIZAR NO LABORATORIO -->
   <label><p><strong>Observações</strong></p>
    <textarea name="cSof" id="tSof" rows="5" style="font-size: 16px" cols="35"  class="w3-input w3-border" placeholder="Insira alguma observação.."></textarea></label><br>
    <input type="button" class="w3-btn w3-blue w3-margin" value="Agendar" style="margin-left: 30px;" onclick="Agendar()" />
  </form>
</div>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("jquery", "1.4.2");
</script>
<!-- <script type="text/javascript">
  $(function(){
    $('#cDis').change(function(){
      if( $(this).val() ) {
        $('#cAula').hide();
        $('.change').show();
        $.getJSON('disciplina_post.php?search=',{cDis: $(this).val(), ajax: 'true'}, function(j){
          var options = '<option disabled selected>Nome - Série - Turma - Código rêferencia</option>'; 
          for (var i = 0; i < j.length; i++) {
            options += '<option value="' + j[i].id + '">'+ j[i].curso+' - ' + j[i].nome_sub_categoria+' - ' + j[i].semestre +'ª Série - Turma: '+ j[i].turma_disc +' - '+ j[i].referencia + '</option>';
          } 
          $('#cAula').html(options).show();
          $('.change').hide();
        });
      } else {
        $('#cAula').html('<option disabled>Sem Disciplina Cadastrada</option>');
      }
    });
  });
</script> -->
</div>




<?php } } ?>



</body>
</html>