<?php


      $titlepag="FAC3 - Editar Curso";
      include("includes/header_config.php");
      header('Content-Type: text/html; charset=utf-8');

if (isset($_GET["numberid"])) {
      $id_lab=$_GET["numberid"];
      $query_lab=mysql_query("SELECT * FROM coordenadores WHERE id_cood='$id_lab'");
      $rowq=mysql_fetch_array($query_lab);

      $query_n2=mysql_query("SELECT * FROM usuarios WHERE nivel>=2");
      $conta=0;
       if ($rowq[6]=="ATIVO") {
        $check="checked";
        $check_d=null;
      }elseif($rowq[6]=="DESATIVADO"){
        $check_d="checked";
        $check=null;
      }else{
        $check_d=null;
        $check=null;
      }
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php echo $titlepag ?></title>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<style type="text/css">
	body{
		background-color: white;
	}
</style>
</head>
<?php  while($linha = mysql_fetch_array($query_n2, MYSQL_NUM)) {
          $coordenador[$conta][0]  = $linha[0];
          $coordenador[$conta][1]  = $linha[1];
          $conta++;
      }  ?>
<body>
  <div class="w3-card-4 w3-display-middle" style="width: 40%">
    	<header class="w3-container w3-blue w3-center">
    		<h2>Editar Curso</h2>
    	</header>
	 <div class="message" style="<?php echo $display;?>"><?php echo $msg;?></div>
	<div id="disable" class="w3-container w3-center">
          <form action="?acao=editar_c" method="post">
          	<input type="text" hidden name="ids" value="<?php echo $id_lab?>"/>
          	<br/>
             <label for="nome_cur" class="w3-left-align"><p><b>Nome do Curso:</b></p></label><input autofocus value="<?php echo $rowq[4]; ?>" id="nome_cur" type="text" name="nome_cur" class="w3-input"/>
             <br/>
		     <label for="abrev" class="w3-left-align"><p><b>Abreviação</b></p></label><input value="<?php echo $rowq[5]; ?>" id="abrev" name="abrev" type="text" class="w3-input"/>
		     <br/>
		     <label for="coord_resp" class="w3-left-align"><p><b>Coordenador Responsável:</b></p></label><select name="coord_resp" id="coord_resp" class="w3-select">
           <?php
          foreach ($coordenador as $show) {

            if ($show[1]==$rowq[1]) {
              $selected="selected";
            }else{
              $selected=null;
            }
        ?>
        <option <?php echo $selected; ?> value="<?php echo $show[0];?>"><?php echo $show[1]?></option>
        <?php } ?>
         </select>
		     <br/>

		     <div class="w3-left-align">
		     <p><b>Status</b></p>
		     <input type="radio" <?php echo $check;?> id="enable" name="status" class="w3-radio" value="ATIVO"/><label for="status"> Ativado</label>
		     <input type="radio" <?php echo $check_d;?> id="disable"  name="status" class="w3-radio" value="DESATIVADO"/><label for="status"> Desativado</label><br>
		     </div>
		     <br/>
		     <input type="submit" class="w3-btn w3-blue w3-margin w3-right" value="Salvar"/>
		     <a href="config_cursos.php" class="w3-btn w3-red w3-margin w3-left" >Voltar</a>
		  </form>
        </div>
	 <!--login-->
	</div>
</body>
</html>


<?php 
}else{
  	 ?>
			   <script language="JavaScript">
                  window.location="config_cursos.php";
               </script>
   <?php 
  }

  ?>