
<?php
include("includes/header_config_saude.php");



$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1; 
$sqlTotal="SELECT * FROM lab_saude ORDER BY id_saude";
$qrTotal = mysql_query($sqlTotal) or die (mysql_error());
$numTotal= mysql_num_rows($qrTotal);
$qtn = 10;
$totalPagina= ceil($numTotal/$qtn);
$inicio = ($qtn * $pagina) - $qtn;
?>


<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/w3.css"/>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <style type="text/css">
  body{
    background-color: white;
  }

</style>
</head>
<body>
  <br/>
  <div class="w3-container">
    <div class="w3-container">
      <img src="css/images/config.png" class="w3-left" width="10%" height="10%">
      <h2>Configuração de Laboratórios de Saúde</h2>
      <p>Configuração de laboratórios ativos, inativos, fechado, abertos, novos laboratórios e muito mais..</p>
      <hr>
    </div>

    <div name="menu" class="w3-container">
      <a href="inserir_lab_saude.php" class="btn btn-primary w3-right"><span class="glyphicon glyphicon-plus-sign"></span> Novo</a>
    </div>
    
    <br/>
    <div class="w3-container">
      <!-- AQUI VAI A TABELA COM OS LABORATORIOS  E COM MENU DE FECHAR E ABRIR, DESATIVAR E ATIVAR. -->
      <table class="w3-table w3-striped w3-hoverable w3-bordered w3-centered"> 
       <tr class="w3-white" >
         <th>#</th>
         <th>Nome</th>
         <th>Capacidade</th>
         <th>Status</th>
         <th>Ação</th>
       </tr>
       <?php
       $query_select=mysql_query("SELECT * FROM lab_saude ORDER BY id_saude LIMIT $inicio,$qtn");

       if (mysql_num_rows($query_select)  == 0 ) {
        echo "<div class=\"message\"> Sem Laboratórios cadastrados e configurados. Clique em 'Novo' para adicionar um laboratório. </div>";

      }else{
        while($row=mysql_fetch_array($query_select)){
          $id_id=$row["id_saude"];

          if ($row["status_saude"]==1) {
            $cor_status="green";
            $status="ATIVO";
          }else{
            $cor_status="red";
            $status="INATIVO";
          }
          ?>

          <tr>
           <td><?php echo $row["id_saude"]; ?></td>
           <td><?php echo $row["nome_lab"]; ?></td>
           <td><?php echo $row["capacidade"]; ?> Alunos</td>
           <td style="color: <?php echo $cor_status?>"><?php echo $status; ?></td>
           <td class="buttons">
             <a href="editar_lab_saude.php?numberid=<?php echo $id_id; ?>" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span></a>
             <a href="#" onclick="javascript: if (confirm('Você realmente deseja excluir este laboratório?'))location.href='?acao=remover&amp;numberid=<?php echo $id_id; ?>'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
           </td>
         </tr>

         <?php
       }
     } 
     ?>

   </table>
 </div>
 <br/>
 <div class="w3-center">
  <div class="w3-bar w3-round w3-border" >
    <?php
                        //Apresentar a paginação
    for($i = 1; $i <= $totalPagina; $i++){ 
      if($i == $pagina){ ?>
        <a class="w3-button w3-bar-item w3-blue"><b><?php echo $i; ?></b></a>
      <?php }else{ ?>
        <a href="?pagina=<?php echo $i;?>" class="w3-button w3-bar-item" ><?php echo $i; ?></a>
      <?php }     
    }  

    ?>
  </div>

</div>
<hr>

</div>





</body>
</html>