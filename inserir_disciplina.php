<?php 
$titlepag="FAC3 - Nova Disciplina";
include("includes/inserir.header.php");
header('Content-Type: text/html; charset=utf-8');
$sql=mysql_query("SELECT * FROM coordenadores WHERE Nome!='' AND status='ATIVO' ORDER BY Curso");
$contar=0;
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title><?php echo $titlepag ?></title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
  <link href="/open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/open-iconic-master/font/css/open-iconic-bootstrap.css"/>



</head>
<style type="text/css">
*{
  margin:0;
  padding: 0;
  outline:none;
  list-style:none;
  font-family: 'Ubuntu',sans-serif;
}
</style>


<?php  while($linha = mysql_fetch_array($sql, MYSQL_NUM)) {
  $coordenador[$contar][0]  = $linha[0];
  $coordenador[$contar][1]  = $linha[1];
  $coordenador[$contar][3]  = $linha[3];
  $coordenador[$contar][4]  = $linha[4];
  $coordenador[$contar][5]  = $linha[5];
  $contar++;
}  ?>

<body>

  <div class="container" name="header" id="">
  	<br/>
    <h2>Nova disciplina</h2>
    <p style="color: red"> * = Campos obrigatórios</p>
    <hr>
  </div>
  <br/>

  <div class="container" name="conteudo-pagina" id="">
    <form action="?acao=inserirDisc" method="post">
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputCod">Cód. Disciplina *</label>
          <input type="text" class="form-control" id="inputCod" name="inputCod" placeholder="Código da disciplina">
        </div>
        <div class="form-group col-md-4">
          <label for="inputCodEsp">Cód. Especialidade *</label>
          <input type="text" class="form-control" id="inputCodEsp" name="inputCodEsp" placeholder="Código da Especialidade">
        </div>
        <div class="form-group col-md-4">
          <label for="inputCarga">Carga Horária *</label>
          <input type="text" class="form-control" id="inputCarga" name="inputCarga" placeholder="Carga horária">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputNome" class="col-sm-2 col-form-label">Nome da Disciplina *</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="inputNome" name="inputNome" placeholder="Nome da disciplina">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-auto">
          <label for="selectEnsalado">Ensalado? *</label>
          <select class="form-control" id="selectEnsalado" name="selectEnsalado" onchange="optionCheck()">
            <option value="sim">Sim</option>
            <option value="não" selected>Não</option>
          </select>
        </div>
        <div class="form-group col-md-6" style="display: none;" id="divcheck">
          <label for="checkCurso" id="labelCurso">Curso *</label>

          <?php
          foreach ($coordenador as $show) {
            ?>
            <div class="custom-control custom-checkbox custom-control">
              <input type="checkbox" class="custom-control-input" name="curso[]" id="<?php echo $show[5];?>" value="<?php echo $show[5];?>">
              <label class="custom-control-label" for="<?php echo $show[5];?>"><?php echo $show[4]?></label>
            </div>
          <?php } ?>
        </div>
        <div class="form-group col-md-6" id="selectCurso">
          <label for="checkCurso" id="labelCurso">Curso *</label>
          <select id="InputCurso" name="InputCurso" class="form-control" >
                <option value="null">Escolha o curso..</option>
            <?php
            foreach ($coordenador as $show) {
              ?>
              <option value="<?php echo $show[5];?>"><?php echo $show[4]?></option>
            <?php } ?>
          </select>
        </div>

        
      </div>






      <label for="checkSerie">Semestre *</label>
      <div id="checkSemestre" class="form-group col-md-6" >

        <div class="custom-control custom-checkbox custom-control-inline">
          <input type="checkbox" id="1" name="serie[]" class="custom-control-input" value="1" >
          <label class="custom-control-label" for="1">1º Semestre</label>
        </div>
        <div class="custom-control custom-checkbox custom-control-inline">
          <input type="checkbox" id="2" name="serie[]" class="custom-control-input" value="2">
          <label class="custom-control-label" for="2">2º Semestre</label>
        </div>
        <div class="custom-control custom-checkbox custom-control-inline">
          <input type="checkbox" id="3" name="serie[]" class="custom-control-input" value="3">
          <label class="custom-control-label" for="3">3º Semestre</label>
        </div>
        <div class="custom-control custom-checkbox custom-control-inline">
          <input type="checkbox" id="4" name="serie[]" class="custom-control-input" value="4">
          <label class="custom-control-label" for="4">4º Semestre</label>
        </div>
        <div class="custom-control custom-checkbox custom-control-inline">
          <input type="checkbox" id="5" name="serie[]" class="custom-control-input" value="5">
          <label class="custom-control-label" for="5">5º Semestre</label>
        </div>
        <div class="custom-control custom-checkbox custom-control-inline">
          <input type="checkbox" id="6" name="serie[]" class="custom-control-input" value="6">
          <label class="custom-control-label" for="6">6º Semestre</label>
        </div>
        <div class="custom-control custom-checkbox custom-control-inline">
          <input type="checkbox" id="7" name="serie[]" class="custom-control-input" value="7">
          <label class="custom-control-label" for="7">7º Semestre</label>
        </div>
        <div class="custom-control custom-checkbox custom-control-inline">
          <input type="checkbox" id="8" name="serie[]" class="custom-control-input" value="8">
          <label class="custom-control-label" for="8">8º Semestre</label>
        </div>
        <div class="custom-control custom-checkbox custom-control-inline">
          <input type="checkbox" id="9" name="serie[]" class="custom-control-input" value="8">
          <label class="custom-control-label" for="9">9º Semestre</label>
        </div>
        <div class="custom-control custom-checkbox custom-control-inline">
          <input type="checkbox" id="10" name="serie[]" class="custom-control-input" value="8">
          <label class="custom-control-label" for="10">10º Semestre</label>
        </div>
      </div>

      <div id="radioSemestre" class="form-group col-md-6" style="display: none" >

        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="radios" id="radios1" class="custom-control-input" value="1" >
          <label class="custom-control-label" for="radios1">1º Semestre</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="radios" id="radios2" class="custom-control-input" value="2">
          <label class="custom-control-label" for="radios2">2º Semestre</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="radios" id="radios3" class="custom-control-input" value="3">
          <label class="custom-control-label" for="radios3">3º Semestre</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="radios" id="radios4" class="custom-control-input" value="4">
          <label class="custom-control-label" for="radios4">4º Semestre</label>
        </div>

        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="radios" id="radios5" class="custom-control-input" value="5">
          <label class="custom-control-label" for="radios5">5º Semestre</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="radios" id="radios6" class="custom-control-input" value="6">
          <label class="custom-control-label" for="radios6">6º Semestre</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="radios" id="radios7" class="custom-control-input" value="7">
          <label class="custom-control-label" for="radios7">7º Semestre</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="radios" id="radios8" class="custom-control-input" value="8">
          <label class="custom-control-label" for="radios8">8º Semestre</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="radios" id="radios9" class="custom-control-input" value="8">
          <label class="custom-control-label" for="radios9">9º Semestre</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="radios" id="radios10" class="custom-control-input" value="8">
          <label class="custom-control-label" for="radios10">10º Semestre</label>
        </div>
      </div>


      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="selectTurma">Turma *</label>
          <select class="form-control" id="selectTurma" name="selectTurma">
            <option value="A">A</option>
            <option value="B">B</option>
          </select>
        </div>
        
      </div>


      <fieldset class="form-group">
        <div class="row">
          <legend class="col-form-label col-sm-2 pt-0">Turno *</legend>
          <div class="col-sm-10">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="gridRadios" id="gridDiurno" value="diurno" checked>
              <label class="form-check-label" for="gridDiurno">
                Diurno
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="gridRadios" id="gridNoturno" value="noturno">
              <label class="form-check-label" for="gridNoturno">
                Noturno
              </label>
            </div>
          </div>
        </div>
      </fieldset>
      <fieldset class="form-group">
        <div class="row">
          <legend class="col-form-label col-sm-2 pt-0">Modalidade *</legend>
          <div class="col-sm-10">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="gridMods" id="gridEad" value="EAD" checked>
              <label class="form-check-label" for="gridEad">
                EAD
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="gridMods" id="gridPresencial" checked value="Presencial">
              <label class="form-check-label" for="gridPresencial">
                Presencial
              </label>
            </div>
          </div>
        </div>
      </fieldset>
      <div class="form-group row">
        <div class="col-sm-10">
          <button type="submit" class="btn float-right btn-primary btn-lg">Salvar</button>
        </div>
      </div>
    </form>
  </div>



  <script type="text/javascript">
    function optionCheck(){
      var option = document.getElementById("selectEnsalado").value;
      if(option == "sim"){
        document.getElementById("divcheck").style.display ="block";
        document.getElementById("selectCurso").style.display ="none";
        document.getElementById("radioSemestre").style.display ="block";
        document.getElementById("checkSemestre").style.display ="none";
      }
      else{
        document.getElementById("divcheck").style.display ="none";
        document.getElementById("selectCurso").style.display ="block";
        document.getElementById("radioSemestre").style.display ="none";
        document.getElementById("checkSemestre").style.display ="block";
      }
    }
  </script>





  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>