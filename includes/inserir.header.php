<?php
//STARTS
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
ob_start();
session_start();
header('Content-Type: text/html; charset=utf-8');
//Globais
$home="http://localhost/agendalab";
$title="LAB FAC 3";
$startaction="";
$msg="";
$flash="";
$aparecer="";
$id="";
$nivel=$_SESSION["nivel"];

//include das classes
include("classes/DB.class.php");
include("classes/inserir_lab.class.php");
//Conexão com o Banco de Dados
if (isset($_SESSION["dbname"])){
    $dbname = $_SESSION["dbname"];
	conect($dbname);
}
function conect($dbname){
    $conectar=new DB;
    $conectar=$conectar->conectar($dbname);
}

// Testar Conexão com BD caso precise
// $query=mysql_query("SELECT * FROM usuarios");
// echo mysql_num_rows($query);

if(isset($_GET["acao"])){
	$acao=$_GET["acao"];
	$startaction=1;
}

// METODO DE INSERIR LABORATORIO

if($startaction==1){
	if($acao=="inserir"){$nomelab=$_POST["cNomelab"]; $maquina=$_POST["cMaquina"];$type=$_POST["cType"];$quantidademaq=$_POST["cQuantidademaquina"];$acess=$_POST["acess"];$status=$_POST["status"];
	if(empty($nomelab)|| empty($maquina)|| empty($quantidademaq) || empty($type))
		{$msg="Preencha todos os campos";}
	else{
		
		$conectar=new inserir;
		echo"<div class=\"flash\">";
		$conectar=$conectar->insert ($nomelab,$maquina,$type,$quantidademaq,$acess,$status);
		echo"</div>";
	}		 
}


// Metodo inserir Curso
if($acao=="inserir_c"){$nomecurso=$_POST["nome_curso"]; $sigla=$_POST["sigla_curso"];$status=$_POST["status"];
if(empty($nomecurso)|| empty($sigla))
	{$msg="Preencha todos os campos";}
else{

	$conectar=new inserir;
	echo"<div class=\"flash\">";
	$conectar=$conectar->novo_curso($nomecurso,$sigla,$status);
	echo"</div>";

}
}

// Metodo inserir Disciplina

if ($acao=="inserirDisc" AND $nivel >= 1) {
	$ensaladoDisci=$_POST["selectEnsalado"];
	if ($ensaladoDisci == "sim") { // Se a disciplina for Ensalada

		$codDisciplina=$_POST["inputCod"];//Codigo Disci
	    $cargaHoraria=$_POST["inputCarga"];//Carga horaria
	    $nomeDisciplina=$_POST["inputNome"];//Nome da Disci
	    $turmaDisci=$_POST["selectTurma"];//Turma Disci
	    $turnoDisci=$_POST["gridRadios"];// Turno
	    $modalidadeDisc=$_POST["gridMods"];// Modalidade da Disci
	    $last=date("d/m/Y H:i"); // ultima modificação
	    $count=array( 
	    	array(),
	        array(),
	    );
	    for ($c=0; $c < 1; $c++) { 
	    	$cod_refe=uniqid();
	    }

	    if (isset($_POST["InputCurso"])AND !$_POST["InputCurso"]==null) {
	        	$count[0][]=$_POST["InputCurso"]; //verifica se select 1 está selecionado
	        	if (isset($_POST["serie"])) {
	        		$check1=$_POST["serie"];
                    $ensalado="";
	        	foreach ($check1 as $k => $v) {
	    	        $ensalado.=$v;
	        	}
	        	 $count[1][]=$ensalado;
	    }

	        }
	        if (isset($_POST["InputCurso2"])AND !$_POST["InputCurso2"]==null) {
	        	$count[0][].=$_POST["InputCurso2"];//verifica se select 2 está selecionado

	        	if (isset($_POST["serie2"])) {
	        		$check1=$_POST["serie2"];
                    $ensalado="";
	        	foreach ($check1 as $k => $v) {
	    	        $ensalado.=$v;
	        	}
	        	$count[1][].=$ensalado;
	    }



	        }
	        if (isset($_POST["InputCurso3"])AND !$_POST["InputCurso3"]==null) {
	        	$count[0][]=$_POST["InputCurso3"];//verifica se select 3 está selecionado

	        	if (isset($_POST["serie3"])) {
	        		$check1=$_POST["serie3"];
                   $ensalado="";
	        	foreach ($check1 as $k => $v) {
	    	         $ensalado.=$v;
	        	}
	        	$count[1][].=$ensalado;
	    }


	        }
	        if (isset($_POST["InputCurso4"])AND !$_POST["InputCurso4"]==null) {
	        	$count[0][].=$_POST["InputCurso4"];//verifica se select 4 está selecionado

	        	if (isset($_POST["serie4"])) {
	        		$check1=$_POST["serie4"];
                 $ensalado="";
	        	foreach ($check1 as $k => $v) {
	    	         $ensalado.=$v;
	        	}
	        	$count[1][].=$ensalado;
	    }

	        }
	        if (isset($_POST["InputCurso5"])AND !$_POST["InputCurso5"]==null) {
	        	$count[0][]=$_POST["InputCurso5"];//verifica se select 5 está selecionado

	        	if (isset($_POST["serie5"])) {
	        		$check1=$_POST["serie5"];
                  $ensalado="";
	        	foreach ($check1 as $k => $v) {
	    	         $ensalado.=$v;
	        	}
	        	$count[1][].=$ensalado;
	    }

	        }

	        
	        $result=count($count[0]);
	        for ($i=0; $i < $result; $i++){
	        	    $qtnserie=$count[1][$i];
	        		$series= str_split($qtnserie);
	        		$vezes=strlen($qtnserie);
	        		
	        		for ($n=0; $n < $vezes; $n++) { 
	        			$cursoCheck=$count[0][$i];
	        			$ensaladoSerie=$series[$n];
	        			
	        			
	        			$conectar=new inserir;
	        			$conectar=$conectar->novo_disciplina_ensalada($codDisciplina,$cod_refe,$ensaladoDisci,$cargaHoraria,$nomeDisciplina,$cursoCheck,$turmaDisci,$turnoDisci,$modalidadeDisc,$ensaladoSerie,$last);
	        			




	        			
	        		}
	        		
	        	
	        }




	        







    }else{ //Se não for Ensalado
    	for ($c=0; $c < 1; $c++) { 
	    	$cod_refe=uniqid();
	    }

    	$codDisciplina=$_POST["inputCod"];//Codigo Disci
	    $cargaHoraria=$_POST["inputCarga"];//Carga horaria
	    $nomeDisciplina=$_POST["inputNome"];//Nome da Disci
	    $turmaDisci=$_POST["selectTurma"];//Turma Disci
	    $turnoDisci=$_POST["gridRadios"];// Turno
	    $modalidadeDisc=$_POST["gridMods"];// Modalidade da Disci
	    $series=$_POST["serieradio"];// Check Serie
	    $cursoCheck=$_POST["InputCurso"];// Select curso Cursos
	    $last=date("d/m/Y H:i"); // ultima modificação
       
        
	    $conectar=new inserir;
	    $conectar=$conectar->novo_disciplina($codDisciplina,$cod_refe,$ensaladoDisci,$cargaHoraria,$nomeDisciplina,$cursoCheck,$turmaDisci,$turnoDisci,$modalidadeDisc,$series,$last);
	    


	}





}






}






       //Variaveis de estilo
if(empty($msg)){
	$display="display:none;";
}else{
	$display="display:block;";
}


?>
