<?php 
$titlepag="FAC3 - Cadastro";
include("includes/header.php");
header('Content-Type: text/html; charset=utf-8');

$sql=mysql_query("SELECT * FROM usuarios WHERE nivel=2 ORDER BY Nome");
$contar=0;
$cursos=mysql_query("SELECT * FROM cursos WHERE ORDER BY Curso ");
$cont_curso=0;
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title><?php echo $titlepag ?></title>
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/w3.css"/>
</head>
<?php  while($linha = mysql_fetch_array($sql, MYSQL_NUM)) {
	$coordenador[$contar][0]  = $linha[0];
	$coordenador[$contar][1]  = $linha[1];
	$coordenador[$contar][3]  = $linha[3];
	$coordenador[$contar][4]  = $linha[4];
	$coordenador[$contar][5]  = $linha[5];
	$contar++;
}	 ?>
<body>
	<div id="login" class="w3-container w3-animate-zoom w3-pale-blue w3-topbar w3-bottombar w3-border-blue w3-rightbar w3-leftbar w3-center" style="display: block; width: 40%; margin-left: 30%; margin-top: 11%">
		<div>
			<h2 style="text-align: center;">Cadastrar Usuário</h2>
			<h5 style="text-align: center;color: red">Todos os campos são Obrigatórios!</h5>
		</div>
		<div class="w3-panel w3-pale-red w3-leftbar w3-rightbar  w3-border-red" style="<?php echo $display;?>"><?php echo $msg;?></div>


		<form action="?acao=cadastrar" method="post" class="w3-center">
			<label for="nome" class="w3-left-align"><p><b>Nome:</b></p></label><input autofocus id="nome" type="text" class="w3-input" name="nome" placeholder="Nome" />
			<br/>
			<label for="sobrenome" class="w3-left-align"><p><b>Sobrenome:</b></p></label><input id="sobrenomenome" type="text" class="w3-input" placeholder="Sobrenome" name="sobrenome"/>
			<br/>
			<label for="email" class="w3-left-align"><p><b>E-mail:</b></p></label><input id="email" type="text" class="w3-input" name="email"/>
			<br/>
			<label for="cood" class="w3-left-align"><p><b>Coordenador responsável:</b></p></label><select id="cood" class="w3-select" name="cood"/>
			<?php
			foreach ($coordenador as $show) {
				?>
				<option value="<?php echo $show[0];?>"><?php echo $show[1]?> - <?php echo $show[5]?></option>
			<?php } ?>
		</select>
		<br/>
		<br/>
		<?php  while($row = mysql_fetch_array($cursos, MYSQL_NUM)) {
			$cur_n[$cont_curso][4]  = $row[4];
			$cur_n[$cont_curso][5]  = $row[5];
			$cont_curso++;
		}	 ?>
		<label for="nivel" class="w3-left-align"><p><b>Função:</b></p></label><select id="nivel" class="w3-select" onchange="optionCheck()" name="nivel">
			<option value="1">Professor</option>
			<option value="2">Coordenador</option>
			<option value="3">Coor. Acadêmico</option>
			<option value="4">Diretor</option>
			<option value="5">Admin Saude</option>
			<option value="6">Admin Engenharia</option>
			<option value="7">Admin Informática</option>
		</select>
		<br/>
		<label for="hiddenCurso" id="labelCurso" class="w3-left-align" style="display:none"><p><b> De qual curso?</b></p></label>
		<select id="hiddenCurso" name="curso" class="w3-select" style="display:none" >
			<?php
			foreach ($cur_n as $mostra) {
				?>
				<option value="<?php echo $mostra[4];?>"><?php echo $mostra[4];?></option>
			<?php } ?>
		</select><br/>
		<label for="Logon" class="w3-left-align"><p><b>Login:</p></b></label><input onkeypress="if (!isNaN(String.fromCharCode(window.event.keyCode))) return false; else return true;" id="Logon" type="text" class="w3-input" name="Logon"/>
		<br/>
		<label for="Senha1" class="w3-left-align"><p><b>Senha:</p></b></label><input id="Senha1" type="password" class="w3-input" name="Senha1"/>
		<br/>
		<label for="Senha2" class="w3-left-align"><p><b>Confirmação de Senha:</p></b></label><input id="Senha2" type="password" class="w3-input" name="Senha2"/>
		<br/>
		<input type="submit" class="w3-btn w3-blue w3-margin w3-right" value="Cadastrar"/>
		
	</form>
	<!--acomodar-->


	<!--login-->
</div>
<script type="text/javascript">
	function optionCheck(){
		var option = document.getElementById("nivel").value;
		if(option == "2"){
			document.getElementById("hiddenCurso").style.display ="block";
			document.getElementById("labelCurso").style.display ="block";
		}
		else{
			document.getElementById("hiddenCurso").style.display ="none";
			document.getElementById("labelCurso").style.display ="none";
		}
	}
</script>
</body>
</html>